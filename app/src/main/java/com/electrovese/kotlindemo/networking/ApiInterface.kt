package com.electrovese.kotlindemo.networking

import code_setup.app_models.request_.*
import code_setup.app_models.response_.*
import code_setup.net_.NetworkConstant
import code_setup.ui_.home.models.*
import code_setup.ui_.settings.models.RejectedRidesResponseModel
import code_setup.ui_.settings.models.RequestSupportModel
import code_setup.ui_.settings.models.ReviewsResponseModel
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*


/**
 * Created by arischoice on 20/1/2019.
 */
interface ApiInterface {
    fun search(searchKey: String): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_LOGIN)
    fun loginRequest(@Body loginRequestModel: LoginRequestModel): Observable<LoginResponseModel>

    /* @GET(NetworkConstant.API_LIST)
      fun getList(): Observable<ListResponseModel>*/


    @GET(NetworkConstant.API_PROFILE)
    fun getProfileDetail(@HeaderMap headreToken: HashMap<String, String>): Observable<ProfileResponseModel>

    @POST(NetworkConstant.API_CHANGE_STATUS)
    fun changeDriverStatus(
        @HeaderMap headreToken: HashMap<String, String>,
        @Body changeDriverStatusRequest: ChangeDriverStatusRequest
    ): Observable<UpdateStatusResponseModel>

    @POST(NetworkConstant.API_CAPURE_INFO)
    fun captureInfo(
        @HeaderMap headreToken: HashMap<String, String>,
        @Body captureInfoModel: CaptureInfoModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_ACCEPT_BOOKINGS)
    fun acceptTourRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel1: UpdateBookingRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_REJECT_BOOKINGS)
    fun rejectTourRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel1: UpdateBookingRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_UPCOMING_BOOKINGS)
    fun getUpcomingToursRequest(@HeaderMap commonHeaders: HashMap<String, String>): Observable<TourListResponseModel>

    @GET(NetworkConstant.API_DRIVER_STATUS)
    abstract fun getStatusRequest(@HeaderMap commonHeaders: HashMap<String, String>): Observable<CurrentStatusResponseModel>


    @POST(NetworkConstant.API_START_MOVING)
    fun startMovingRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel: StartMovingRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_START_MOVING_TO_MEETING_POINT)
    fun startMovingToMeetingPointRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel: StartMovingRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_REACHED_MEETING_POINT)
    fun reachedMeetingPointRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel: StartMovingRequestModel
    ): Observable<BaseResponseModel>


    @POST(NetworkConstant.API_TOUR_DETAIL)
    fun getTourDetail(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel: UpdateBookingRequestModel
    ): Observable<TourDetailResponseModel>

    @POST(NetworkConstant.API_ARRIVE_AT_LOCATION)
    fun updateArriveStatus(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel: UpdateRequestModel
    ): Observable<BaseResponseModel>


    @POST(NetworkConstant.API_MARK_TRIP_COMPLETE)
    fun markTripComplete(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateRequestModel: UpdateRequestModel
    ): Observable<BaseResponseModel>


    @POST(NetworkConstant.API_ONBOARDING)
    fun onBoardingRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body onBoardingRequestModel: OnBoardingRequestModel
    ): Observable<OnBoardingResponseModel>

    @POST(NetworkConstant.API_GET_MESSAGES)
    fun getMessagesRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateBookingRequestModel: UpdateBookingRequestModel
    ): Observable<MessageListResponseModel>

    @POST(NetworkConstant.API_SEND_MESSAGES)
    fun sendMessagesRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body sendMessageRequestModel: SendMessageRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_BOOKING_HISTORY)
    fun getBookingHistory(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body bookingHistoryRequest: BookingHistoryRequest
    ): Observable<RideHistoryResponseModel>

    @POST(NetworkConstant.API_SUBMIT_RATING)
    fun submitTourRating(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body submitReviewRequest: SubmitReviewRequest
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_GET_DATEWISE_TOUR)
    fun requestTourList(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestDateTourModel: RequestDateTourModel
    ): Observable<TourListResponseModel>

    @POST(NetworkConstant.API_GET_SCHEDULED_TOUR)
    fun requestScheduledTourList(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestDateTourModel: RequestDateTourModel
    ): Observable<ScheduledTourListResponseModel>


    /*@POST(NetworkConstant.API_GET_MONTHWISE_TOUR)
    fun requestMonthlyTourList(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestDateTourModel: RequestDateTourModel
    ): Observable<MonthlyToursListModel>*/

    @POST(NetworkConstant.API_GET_MONTHWISE_SCHEDULED_TOUR)
    fun requestMonthlyTourList(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestDateTourModel: RequestDateTourModel
    ): Observable<MonthlySchedulesToursListModel>

    @POST(NetworkConstant.API_LOGOUT_USER)
    fun logoutUser(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body lgReqest: LogoutRequestData
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_LOGOUT_USER)
    fun logoutUser(@HeaderMap commonHeaders: HashMap<String, String>): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_SCHEDULE_TOUR)
    fun scheduleCalendarBooking(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestCalenderBookingModel: RequestCalenderBookingModel
    ): Observable<BaseResponseModel>

    @Multipart
    @POST(NetworkConstant.API_UPDATE_PROFILE)
    fun updateProfileImage(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Part requiredData: MultipartBody.Part
    ): Observable<ProfileUpdateResponseModel>

    @POST(NetworkConstant.API_SUPPORT)
    fun requestSupport(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requiredData: RequestSupportModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_REJECTED_TOURS)
    abstract fun getRejectedTours(
        @HeaderMap commonHeaders: HashMap<String, String>
    ): Observable<RejectedRidesResponseModel>

    @POST(NetworkConstant.API_EARNINGS_DATA)
    fun getEarningData(@HeaderMap commonHeaders: HashMap<String, String>):
            Observable<EarningResponseModel>

    @POST(NetworkConstant.API_TOUR_LIST)
    fun getTours(@HeaderMap commonHeaders: HashMap<String, String>, @Body s: RequestToursModel):
            Observable<ToursListResponseModel>

    @POST(NetworkConstant.API_RIDE_SUPPORT)
    fun requestRideSupport(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestRideSupport1: RequestRideSupport
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_REMOVE_SCHEDULE)
    fun requestRemoveScheduledTour(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestDateTourModel: RequestRemoveTourModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_LOCATION_UPDATES)
    fun updateLocationRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateOrderRequestModel: UpdateOrderRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_LOCATION_UPDATES_NO_RIDE)
    fun updateLocationNoRideRequest(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body updateOrderRequestModel: UpdateOrderRequestModel
    ): Observable<BaseResponseModel>


    @POST(NetworkConstant.API_DRIVER_REVIEWS)
    fun getDriverReviews(
        @HeaderMap commonHeaders: HashMap<String, String>
    ): Observable<ReviewsResponseModel>

    @POST(NetworkConstant.API_VERSION_CHECK)
    fun versionCheck(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body s: VersionCheckRequestModel
    ): Observable<BaseResponseModel>

    @POST(NetworkConstant.API_CANCEL_RIDE)
    fun requestCancelRide(
        @HeaderMap commonHeaders: HashMap<String, String>,
        @Body requestRideSupport1: RequestCancelRide
    ): Observable<BaseResponseModel>
}

