package code_setup.ui_.home.models

data class EarningResponseModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: ResponseObj
) {
    data class ResponseObj(
        var monthly_earning: List<Earning>,
        var monthly_total: Int,
        var today_earning: Earning,
        var weekly_earning: List<Earning>,
        var weekly_total: Int
    ) {
        data class Earning(
            var bookings: Int,
            var currency_symbol: String,
            var date: String,
            var total: Int
        )

        data class MonthlyEarning(
            var bookings: Int,
            var currency_symbol: String,
            var date: String,
            var total: Int
        )

        data class TodayEarning(
            var bookings: Int,
            var currency_symbol: String,
            var date: String,
            var total: Int
        )

        data class WeeklyEarning(
            var bookings: Int,
            var currency_symbol: String,
            var date: String,
            var total: Int
        )
    }
}