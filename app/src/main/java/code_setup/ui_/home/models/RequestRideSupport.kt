package code_setup.ui_.home.models

data class RequestRideSupport(
    var booking_id: String,
    var concern: String
)