package code_setup.ui_.home.models

data class ToursListResponseModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: List<ResponseObj>
) {
    data class ResponseObj(
        var _id: String,
        var id: String,
        var name: String
    )
}