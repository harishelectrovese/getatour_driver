package code_setup.ui_.home.models

data class RequestCancelRide(
    var booking_id: String,
    var lat: String,
    var lng:String,
    var reason:String
)