package code_setup.ui_.home.models

import code_setup.app_models.response_.TourDetailResponseModel
import java.io.Serializable

data class MonthlySchedulesToursListModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: ResponseObj
) {
    data class ResponseObj(
        var bookings: List<Booking>,
        var schedules: List<Schedule>
    ) {
        data class Booking(
            var _id: String,
            var booking_date: String,
            var category_name: String,
            var distance: String,
            var driver_rating: Float,
            var duration: String,
            var end_time: String,
            var id: String,
            var routes: List<ScheduledTourListResponseModel.Bookings.Route>,
            var seats: Int,
            var start_location: StartLocation,
            var start_time: String,
            var status: String,
            var tour_name: String,
            var is_ride: Boolean,
            var tour_link: String,
            var type: String,
            var userBookings: List<UserBooking>,
            var users: List<User>
        ) {
            /* data class Route(
                 var _id: String,
                 var driver_coordinates: List<Double>,
                 var is_arrived: Boolean,
                 var lat: Double,
                 var lng: Double,
                 var name: String,
                 var waiting: Int
             )*/

            data class StartLocation(
                var coordinates: List<Double>,
                var name: String,
                var type: String
            )

            data class UserBooking(
                var __v: Int,
                var _id: String,
//                var amount: Amount,
                var base_currency_price: Float,
                var board_type: String,
                var booking_date: String,
//                var chats: List<Any>,
                var city_id: String,
                var country_id: String,
                var coupon_id: String,
                var createdAt: String,
//                var currency: Currency,
//                var dates: Dates,
                var discount: String,
                var driver_id: String,
                var end_loc: EndLoc,
                var end_time: String,
                var expiration_date: String,
                var is_board: Boolean,
                var is_reviewed: Boolean,
//                var paid_amount: PaidAmount,
                var pickup_loc: PickupLoc,
                var pickup_time: String,
                var provider_id: String,
//                var review: Review,
//                var routes: List<Route>,
                var seats: Int,
                var start_loc: StartLoc,
                var start_time: String,
                var status: String,
                var sub_amount: Float,
                var tax: Float,
//                var time_zones: TimeZones,
                var total_amount: Float,
                var tour_booking_id: String,
                var tour_id: String,

                var tour_name: String,
                var type: String,
                var unique_code: String,
                var updatedAt: String,
                var user: TourDetailResponseModel.ResponseObj.User,
                var user_id: String
            ) {
                /*data class Amount(
                    var base_currency_price: Float,
                    var conversion_rate: Float,
                    var currency_shortcode: String,
                    var currency_symbol: String,
                    var discount: Float,
                    var rejected_fee: Float,
                    var subtotal: Float,
                    var tax: Float,
                    var tax_percentage: Float,
                    var total: Float
                )*/

                /* data class Currency(
                     var tour_currency_shortcode: String,
                     var tour_currency_symbol: String,
                     var user_currency_shortcode: String,
                     var user_currency_symbol: String
                 )*/

                /* data class Dates(
                     var local_timezone: Int,
                     var tour_timezone: Int
                 )*/

                data class EndLoc(
                    var coordinates: List<Double>,
                    var name: String,
                    var type: String
                )

                /*  data class PaidAmount(
                      var conversion_rate: Int,
                      var currency_shortcode: String,
                      var currency_symbol: String,
                      var discount: Float,
                      var subtotal: Float,
                      var tax: Float,
                      var tax_percentage: Float,
                      var total: Float
                  )*/

                data class PickupLoc(
                    var coordinates: List<Double>,
                    var id: String,
                    var name: String,
                    var type: String
                )

                /* data class Review(
                     var comment: String,
                     var rating: Float
                 )*/

                /* data class Route(
                     var _id: String,
                     var lat: Double,
                     var lng: Double,
                     var name: String,
                     var waiting: Int
                 )*/

                data class StartLoc(
                    var coordinates: List<Double>,
                    var name: String,
                    var type: String
                )

                /* data class TimeZones(
                     var tour: Double,
                     var user: Double
                 )*/

                /* data class User(
                     var __v: Int,
                     var _id: String,
                     var address: String,
                     var age: String,
                     var contact: Long,
                     var contact_verified: Boolean,
                     var country: String,
                     var country_code: String,
                     var createdAt: String,
                     var currency_shortcode: String,
                     var currency_symbol: String,
                     var dob: String,
                     var email: String,
                     var gender: String,
                     var is_email_verified: Boolean,
                     var name: String,
                     var password: String,
                     var status: String,
                     var updatedAt: String,
                     var user_image: String,
                     var verification_code: String
 //                    var wishlist: List<Any>
                 ):Serializable*/
            }

            data class User(
                var contact: String,
                var email: String,
                var is_board: Boolean,
                var location_name: String,
                var name: String,
                var unique_code: String,
                var user_booking_id: String,
                var user_id: String,
                var user_image: String
            )
        }

        data class Schedule(
            var _id: String,
            var end_time: String,
            var id: String,
            var name: String,
            var start_time: String,
            var tour_link: String
        )
    }
}