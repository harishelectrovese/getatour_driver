package code_setup.ui_.home.views.schedule

import android.graphics.Bitmap
import android.graphics.RectF
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.RequestDateTourModel
import code_setup.app_models.request_.UpdateBookingRequestModel
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_util.*
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.location_utils.log
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.DatesAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.MonthlySchedulesToursListModel
import code_setup.ui_.home.models.RequestRemoveTourModel
import com.alamkanak.weekview.DateTimeInterpreter
import com.alamkanak.weekview.MonthLoader.MonthChangeListener
import com.alamkanak.weekview.WeekView
import com.alamkanak.weekview.WeekViewEvent
import com.base.mvp.BasePresenter
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import kotlinx.android.synthetic.main.layout_schedule_activity.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.math.min


class ScheduleActivity : CoreActivity(), HomeView {
    companion object {
        lateinit var instance: ScheduleActivity
    }

    private var firstVisibleDate: String = ""
    private var selectedDate: String = ""
    var TAG: String = ScheduleActivity::class.java.simpleName
    lateinit var datesAdapter: DatesAdapter

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))

        when (int) {
            NetworkRequest.REQUEST_GET_MONYTHLY_TOURS -> {
                var responseData = list as MonthlySchedulesToursListModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    /* if (responseData.response_obj != null) {
                         addEventsToCalender(responseData.response_obj)
                     }*/
                    mNewEvents = ArrayList()
                    tourList = ArrayList()
                    if (responseData.response_obj != null) {
                        addToursToCalander(responseData.response_obj)
                    }

                } else if (responseData.response_code == NetworkCodes.SESSION.nCodes) {
                    AppUtils.showToast(getString(R.string.str_session_expired))
                    logoutUserNow()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }
            }

            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                } else {
                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
                    logoutUserNow()
                }

            }
            NetworkRequest.REQUEST_DELETE_SCHEDULED_TOURS -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showSnackBar(this, getString(R.string.removed_successfully))
                    getscheduledForMonth(selectedDate)
                }
            }
        }
    }

    val events: MutableList<WeekViewEvent> = ArrayList()
    var tourList = ArrayList<ScheduleDayActivity.TourData>()
    private fun addToursToCalander(responseObj: MonthlySchedulesToursListModel.ResponseObj) {
        var bookingList = responseObj.bookings
        var scheduleList = responseObj.schedules
        if (bookingList.isNotEmpty()) {
            for (i in 0 until bookingList.size) {
                mNewEvents!!.add(
                    WeekViewEvent(
                        bookingList.get(i).id,
                        bookingList.get(i).tour_name + '\n' + bookingList.get(i).type + '\n' + "From: " + getformatedTime(
                            bookingList.get(
                                i
                            ).start_time
                        ) + '\n' + "To: " + getformatedTime(bookingList.get(i).end_time),
                        getCalanderFromDate(bookingList.get(i).start_time),
                        getCalanderFromDate(bookingList.get(i).end_time)
                    )
                )
                // use for detail dialog
                tourList.add(
                    ScheduleDayActivity.TourData(
                        bookingList.get(i).tour_name,
                        bookingList.get(i).start_location.name,
                        bookingList.get(i).type,
                        bookingList.get(i).seats.toString(),
                        getformatedTime(bookingList.get(i).start_time),
                        getformatedTime(bookingList.get(i).end_time),
                        DateUtilizer.getHourValue(bookingList.get(i).start_time),
                        DateUtilizer.getMinutesValue(bookingList.get(i).start_time),
                        DateUtilizer.getTimeDiffereence(
                            bookingList.get(i).start_time,
                            bookingList.get(i).end_time
                        ).toInt(),
                        R.drawable.rectangle_bg_with_app_border_and_bg,
                        false,
                        bookingList.get(i).id, Gson().toJson(bookingList.get(i)),
                        bookingList.get(i).tour_link, bookingList.get(i).status,
                        bookingList.get(i).is_ride,
                        bookingList.get(i).routes.get(0).name,
                        bookingList.get(i).routes.get(bookingList.get(i).routes.size - 1).name
                    )
                )
            }
        }
        if (scheduleList.isNotEmpty()) {
            for (i in 0 until scheduleList.size) {
                var weekViewEvent = WeekViewEvent(
                    scheduleList.get(i).id,
                    scheduleList.get(i).name + '\n' + "From: " + getformatedTime(
                        scheduleList.get(
                            i
                        ).start_time
                    ) + '\n' + "To: " + getformatedTime(
                        scheduleList.get(i).end_time
                    ),
                    getCalanderFromDate(scheduleList.get(i).start_time),
                    getCalanderFromDate(scheduleList.get(i).end_time)
                )
                weekViewEvent.color = getResources().getColor(R.color.light_gray)
                mNewEvents!!.add(weekViewEvent)


                // use for detail dialog
                tourList.add(
                    ScheduleDayActivity.TourData(
                        scheduleList.get(i).name,
                        "No location",
                        "Manual Schedule",
                        "1",
                        getformatedTime(scheduleList.get(i).start_time),
                        getformatedTime(scheduleList.get(i).end_time),
                        DateUtilizer.getHourValue(scheduleList.get(i).start_time),
                        DateUtilizer.getMinutesValue(scheduleList.get(i).start_time),
                        DateUtilizer.getTimeDiffereence(
                            scheduleList.get(i).start_time,
                            scheduleList.get(i).end_time
                        ).toInt(),
                        R.drawable.rectangle_bg_with_app_border_and_bg,
                        true,
                        scheduleList.get(i).id,
                        Gson().toJson(scheduleList.get(i)),
                        "", "", false, "", ""
                    )
                )
            }
        }

        events.addAll(mNewEvents)
        weekView.notifyDataSetChanged()

    }

    private fun getformatedTime(strTimeTxt: String): String? {
        var timeStr: String = ""
        timeStr = DateUtilizer.getFormatedDate("dd-MM-YYYY, HH:mm", "hh:mm a", strTimeTxt)
        log("getformatedTime  " + timeStr)
        return timeStr
    }

    private fun getCalanderFromDate(sTime: String): Calendar {
        return DateUtilizer.getcalenderFromString(sTime, "dd-MM-yyyy, HH:mm")
    }

    private fun getCurrentStatus() {
        presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
    }

    /* private fun addEventsToCalender(responseObj: MonthlySchedulesToursListModel.ResponseObj) {
         var scheduleList = responseObj.schedules
         var bookingsList = responseObj.bookings
         val arrayList = ArrayList<EventDay>()
         val events: MutableList<EventDay> = arrayList
         if (scheduleList.isNotEmpty())
             for (i in 0 until responseObj.schedules.size) {
                 val calendar = getcalenderFromString(responseObj.schedules.get(i).month)
                 val drawableDim = getDrawableDim(R.mipmap.black_dot)
                 drawableDim.setColorFilter(Color.parseColor("#2196F3"), PorterDuff.Mode.SRC_IN)
                 events.add(EventDay(calendar!!, drawableDim))
                 events.add(EventDay(calendar!!, drawableDim))
                 events.add(EventDay(calendar!!, drawableDim))

             }
         if (bookingsList.isNotEmpty())
             for (i in 0 until responseObj.bookings.size) {
                 val calendar = getcalenderFromString(responseObj.bookings.get(i).month)
                 val drawableDim = getDrawableDim(R.mipmap.black_dot)
                 events.add(EventDay(calendar!!, drawableDim))
                 events.add(EventDay(calendar!!, drawableDim))
                 events.add(EventDay(calendar!!, drawableDim))

             }
         calendarView.setEvents(events)

     }*/

    private fun getDrawableDim(eventIcon: Int): Drawable {
// Read your drawable from somewhere
        // Read your drawable from somewhere
        val dr = resources.getDrawable(eventIcon)
        val bitmap = (dr as BitmapDrawable).bitmap
// Scale it to 50 x 50
        // Scale it to 50 x 50
        val d: Drawable = BitmapDrawable(resources, Bitmap.createScaledBitmap(bitmap, 20, 20, true))
// Set your new, scaled drawable "d"
        return d
    }


    private fun getcalenderFromString(month: String): Calendar {
        log("getcalenderFromString    " + month)
        val aTime = month
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val cal = Calendar.getInstance()
        try {
            cal.time = sdf.parse(aTime)
            Log.i(TAG, "time = " + cal.timeInMillis)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return cal
    }


    override fun showProgress() {
        loaderView.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        loaderView.visibility = View.GONE
    }

    override fun noResult() {

    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_schedule_activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
        AnimUtils.moveAnimationX(backToolbar, false)
        AnimUtils.moveAnimationX(backToolbar, true)
        txtTitletoolbar.gravity = Gravity.CENTER
        txtTitletoolbar.setText(R.string.strSchedule)
//        monthNameTxt.setText(DateUtilizer.getCurrentDate(DateUtilizer.TITLE_MONTH_FORMAT))
        backToolbar.setOnClickListener {
            onBackPressed()
        }
        logoutIco.visibility = View.VISIBLE
        logoutIco.setOnClickListener {
            AppDialogs.openCalandarViewDays(this, "", "", object : OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    if (t as Int == 0) {
                        weekView.goToToday()
                    } else {
                        weekView.numberOfVisibleDays = t as Int
                        Prefs.putInt(CommonValues.DEFAULT_CALANDAR_VIEW, t as Int)
                        if (t as Int == 7) {
                            weekView.headerWeekDayTitleTextSize = 18f
                        } else weekView.headerWeekDayTitleTextSize = 30f

                        if (!firstVisibleDate.isNullOrBlank())
                            weekView.goToDate(
                                DateUtilizer.getcalenderFromString(
                                    firstVisibleDate,
                                    "dd MMMM, yyyy"
                                )
                            )
                    }
                }
            })
        }

//        initWeekAdapter()
//        DateUtilizer.getLastDay()
//        DateUtilizer.getNextDays(
//            35, DateUtilizer.getLastDay() + "-" + DateUtilizer.getCurrentDate(YEAR_FORMAT)
//        )
//
//        previousMonthBtn.setOnClickListener {
//            datesAdapter.previousMonth(monthNameTxt.text.toString())
//        }
//        nextmonthBtn.setOnClickListener {
//            datesAdapter.nextMonth(monthNameTxt.text.toString())
//        }
        getCurrentStatus()
        initCalender()
        getscheduledForMonth(DateUtilizer.getCurrentDate("MM-dd-YYYY"))
        addScheduleBtn.setOnClickListener {
            Log.e("addSchedule ", " " + Gson().toJson(weekView.firstVisibleDay))
            Log.e(
                "addSchedule ",
                " " + DateUtilizer.getDateFromCalandar(weekView.firstVisibleDay!!, "dd MMMM, yyyy")
            )
            var bndl = Bundle()
            bndl.putString(
                CommonValues.DAY_DETAIL,
                DateUtilizer.getDateFromCalandar(weekView.firstVisibleDay!!, "dd MMMM, yyyy")
            )
            activitySwitcher(this, AddAvailabilityActivity::class.java, bndl)
        }
    }

    private fun getscheduledForMonth(currentDate: String) {
        /*  presenter.getAllToursForMonth(
              NetworkRequest.REQUEST_GET_MONYTHLY_TOURS,
              RequestDateTourModel(
                  currentDate
              )
          )*/
        selectedDate = currentDate
        Handler().postDelayed(Runnable {
            presenter.getAllToursForMonth(
                NetworkRequest.REQUEST_GET_MONYTHLY_TOURS,
                RequestDateTourModel(
                    currentDate
                )
            )
        }, 700)
    }

    private fun getCurrentMonth(): Calendar? {
        var calThis = calendarView.currentPageDate
        var date = calThis.get(Calendar.DATE)
        var month = calThis.get(Calendar.MONTH)
        var year = calThis.get(Calendar.YEAR)
        log("getCurrentMonth  " + date + "   " + month + " " + year)
        return calendarView.currentPageDate
    }

    private var mNewEvents = ArrayList<WeekViewEvent>()
    private fun initCalender() {
//        val min = Calendar.getInstance()
//        min.add(Calendar.DAY_OF_MONTH, -2)

//        val max = Calendar.getInstance()
//        max.add(Calendar.DAY_OF_MONTH, 2)

//        calendarView.setMinimumDate(min)
//        calendarView.setMaximumDate(max)

        calendarView.setOnDayClickListener { eventDay ->
//            Toast.makeText(
//                applicationContext,
//                eventDay.getCalendar().getTime().toString() + " "
//                        + eventDay.isEnabled(),
//                Toast.LENGTH_SHORT
//            ).show()
            Log.e("day  clicked ", "  " + eventDay.getCalendar().getTime().toString())
//            log("calendarView  " + getCurrentMonth())
            var bndl = Bundle()
            bndl.putString(CommonValues.DAY_DETAIL, eventDay.getCalendar().getTime().toString())
            activitySwitcher(this, ScheduleDayActivity::class.java, bndl)
        }
        calendarView.setOnPreviousPageChangeListener {
            var newCal = calendarView.currentPageDate
            val s = "" + newCal.get(Calendar.DATE) + " " + (newCal.get(Calendar.MONTH) + 1) + " "
            var newDate = s + newCal.get(Calendar.YEAR)
            Log.e("onPrevious click    ", " " + newDate);
            getscheduledForMonth(DateUtilizer.getFormatedDate("dd MM yyyy", "MM-dd-yyyy", newDate))
        }
        calendarView.setOnForwardPageChangeListener {
            var newCal = calendarView.currentPageDate
            var newDate =
                "" + newCal.get(Calendar.DATE) + " " + (newCal.get(Calendar.MONTH) + 1) + " " + newCal.get(
                    Calendar.YEAR
                )
            Log.e("onForward click    ", " " + newDate);
            getscheduledForMonth(DateUtilizer.getFormatedDate("dd MM yyyy", "MM-dd-yyyy", newDate))
        }


        // Set an action when any event is clicked.
        weekView.eventClickListener = object : WeekView.EventClickListener {
            override fun onEventClick(event: WeekViewEvent, eventRect: RectF) {
                Log.e("onEventClick    ", " " + event.name)
                if (tourList.isNotEmpty()) {
                    for (i in 0 until tourList.size) {
                        if (event.id.equals(tourList.get(i).id)) {
                            var bndl = Bundle()
                            bndl.putSerializable(CommonValues.TOUR_DATA, tourList.get(i))
                            activitySwitcher(this@ScheduleActivity, TourDetailAct::class.java, bndl)
                            /*AppDialogs.openDialogTourDetail(
                                this@ScheduleActivity,
                                "",
                                tourList.get(i),
                                object : OnBottomDialogItemListener<Any> {
                                    override fun onItemClick(
                                        view: View,
                                        position: Int,
                                        type: Int,
                                        t: Any
                                    ) {
                                        when (type) {
                                            2 -> {
                                                deleteManualSchedule(event.id)
                                            }

                                        }
                                    }
                                })*/
                        }
                    }
                }
            }
        }

        weekView.monthChangeListener = mMonthChangeListener
        weekView.numberOfVisibleDays = Prefs.getInt(CommonValues.DEFAULT_CALANDAR_VIEW, 3)
        weekView.scrollListener = scrollListner

        // Initially, there will be no events on the week view because the user has not tapped on
        // it yet.

        // Initially, there will be no events on the week view because the user has not tapped on
        // it yet.
        mNewEvents = ArrayList<WeekViewEvent>()
        weekView.numberOfVisibleDays = 3//Prefs.getInt(CommonValues.DEFAULT_CALANDAR_VIEW, 7)
        weekView.headerWeekDayTitleTextSize = 32f
        Prefs.putInt(CommonValues.DEFAULT_CALANDAR_VIEW, 3)
        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.

        // Set the new event with duration one hour.

        // Set the new event with duration one hour.
//        var time: Calendar = Calendar.getInstance()
//        val endTime: Calendar = time.clone() as Calendar
//        endTime.add(Calendar.HOUR, 4)

        // Create a new event.
//        val event = WeekViewEvent(20, "New event", time, endTime)
//        mNewEvents!!.add(WeekViewEvent("20", "New event", time, endTime))

//   weekView.dateTimeInterpreter=

        // change date and time  format in header
        setupDateTimeInterpreter()
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     *
     * @param shortDate True if the date values should be short.
     */
    fun setupDateTimeInterpreter() {
        weekView.dateTimeInterpreter = (object : DateTimeInterpreter {
            fun interpretDate(date: Calendar): String? {
                val weekdayNameFormat =
                    SimpleDateFormat("EE", Locale.US)
                var weekday = weekdayNameFormat.format(date.time)
                val format =
                    SimpleDateFormat(" d/M/y", Locale.US)

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
//                if (shortDate) weekday = weekday[0].toString()
                return weekday.toUpperCase() + format.format(date.time)
            }

            fun interpretTime(hour: Int): String? {
                return "" + hour
            }

            override fun getFormattedTimeOfDay(hour: Int, minutes: Int): String {
                var date: Date? = null
                val inputFormat = SimpleDateFormat("HH:mm", Locale.US)
                val outputFormat = SimpleDateFormat("hh:mm a", Locale.US)
                date = inputFormat.parse("" + hour + ":" + minutes)
                return outputFormat.format(date)
            }

            override fun getFormattedWeekDayTitle(date: Calendar): String {
                val weekdayNameFormat =
                    SimpleDateFormat("EE", Locale.US)
                var weekday = weekdayNameFormat.format(date.time)
                val format =
                    SimpleDateFormat(" dd/MM", Locale.US)
                return weekday.toUpperCase() + format.format(date.time)
            }
        })
    }


    private fun deleteManualSchedule(id: String?) {
        presenter.removeScheduledTour(
            NetworkRequest.REQUEST_DELETE_SCHEDULED_TOURS,
            RequestRemoveTourModel(
                id!!
            )
        )
    }

    var scrollListner = object : WeekView.ScrollListener {
        override fun onFirstVisibleDayChanged(
            newFirstVisibleDay: Calendar,
            oldFirstVisibleDay: Calendar?
        ) {
            try {
                Log.e(
                    "onFirstVisibleDayChanged   ",
                    " " + DateUtilizer.getDateFromCalandar(newFirstVisibleDay, "dd MMMM, yyyy")
                            + "    " + DateUtilizer.getDateFromCalandar(
                        oldFirstVisibleDay!!,
                        "dd MMMM, yyyy"
                    )
                )

                DateUtilizer.compairDates(
                    DateUtilizer.getDateFromCalandar(newFirstVisibleDay, "dd MMMM, yyyy"),
                    "dd MMMM, yyyy"
                )
                if (!DateUtilizer.compairDates(
                        DateUtilizer.getDateFromCalandar(newFirstVisibleDay, "dd MMMM, yyyy"),
                        "dd MMMM, yyyy"
                    )
                ) {
                    addScheduleBtn.visibility = View.GONE
                } else addScheduleBtn.visibility = View.VISIBLE
                firstVisibleDate =
                    DateUtilizer.getDateFromCalandar(newFirstVisibleDay, "dd MMMM, yyyy")
            } catch (e: Exception) {
            }
        }
    }


    //    private val events: List<WeekViewEvent> = ArrayList()
    var mMonthChangeListener: MonthChangeListener = object : MonthChangeListener {
        override fun onMonthChange(
            newYear: Int,
            newMonth: Int
        ): MutableList<out WeekViewEvent>? {
            Log.e("onMonthChange   ", " " + newYear + "   " + newMonth)
            Log.e(
                "onMonthChange   ",
                " " + DateUtilizer.getFormatedDate(
                    "yyyy-MM",
                    "MM-dd-yyyy",
                    "" + newYear + "-" + newMonth
                )
            )
            // Populate the week view with the events that was added by tapping on empty view.
//            getscheduledForMonth(DateUtilizer.getFormatedDate("yyyy-MM","MM-dd-yyyy",""+newYear+"-"+newMonth))
            val events: MutableList<WeekViewEvent> = ArrayList()
            val newEvents: ArrayList<WeekViewEvent> = getNewEvents(newYear, newMonth)
            events.addAll(newEvents)
            return events
        }
    }

    /**
     * Get events that were added by tapping on empty view.
     * @param year The year currently visible on the week view.
     * @param month The month currently visible on the week view.
     * @return The events of the given year and month.
     */
    private fun getNewEvents(year: Int, month: Int): ArrayList<WeekViewEvent> {

        // Get the starting point and ending point of the given month. We need this to find the
        // events of the given month.
        val startOfMonth = Calendar.getInstance()
        startOfMonth[Calendar.YEAR] = year
        startOfMonth[Calendar.MONTH] = month - 1
        startOfMonth[Calendar.DAY_OF_MONTH] = 1
        startOfMonth[Calendar.HOUR_OF_DAY] = 0
        startOfMonth[Calendar.MINUTE] = 0
        startOfMonth[Calendar.SECOND] = 0
        startOfMonth[Calendar.MILLISECOND] = 0
        val endOfMonth = startOfMonth.clone() as Calendar
        endOfMonth[Calendar.DAY_OF_MONTH] = endOfMonth.getMaximum(Calendar.DAY_OF_MONTH)
        endOfMonth[Calendar.HOUR_OF_DAY] = 23
        endOfMonth[Calendar.MINUTE] = 59
        endOfMonth[Calendar.SECOND] = 59

        // Find the events that were added by tapping on empty view and that occurs in the given
        // time frame.
        val events = ArrayList<WeekViewEvent>()
        for (event in this!!.mNewEvents!!) {
            if (event.endTime.timeInMillis > startOfMonth.timeInMillis &&
                event.startTime.timeInMillis < endOfMonth.timeInMillis
            ) {
                events.add(event)
            }
        }
        Log.e("onEventClick    ", " " + Gson().toJson(events))
        return events
    }


    private fun getWeekdaysList(): ArrayList<String> {
        var weekDays = ArrayList<String>()
        weekDays.add("S")
        weekDays.add("M")
        weekDays.add("T")
        weekDays.add("W")
        weekDays.add("T")
        weekDays.add("F")
        weekDays.add("S")
        return weekDays
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", "SCHEDULE DAY " + event.type)
        try {
            Log.d("onMessage", "SCHEDULE DAY " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {

            EVENTS.NETWORK_CONNECTION_CALLBACK -> {
                if (event.oj as Boolean) {
                    AppUtils.showSnackBarConnectiivity(
                        this,
                        getString(R.string.str_connected_to_internet),
                        true
                    )
                } else {
                    AppUtils.showSnackBarConnectiivity(
                        this,
                        getString(R.string.str_no_internet_conection),
                        false
                    )
                }

            }
            EVENTS.SCHEDULE_UPDATED -> {
                getscheduledForMonth(selectedDate)
            }

            EVENTS.REQUEST_NEW_TOUR_JOB -> {
                getCurrentStatus()
            }
        }
        removeStickyEvent()
    }

    private fun removeStickyEvent() {
        var stickyEvent = EventBus.getDefault().getStickyEvent(CustomEvent::class.java)
// Better check that an event was actually posted before
        if (stickyEvent != null) {
            // "Consume" the sticky event
            EventBus.getDefault().removeStickyEvent(stickyEvent);
            // Now do something with it
        }
    }
}