package code_setup.ui_.home.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.DisplayMetrics
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.CompoundButton
import android.widget.RelativeLayout
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import code_setup.app_core.BaseApplication
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.ADLocation
import code_setup.app_models.other_.NotificationDataBodyModel
import code_setup.app_models.other_.NotificationModel
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.ChangeDriverStatusRequest
import code_setup.app_models.request_.UpdateBookingRequestModel
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_models.response_.LoginResponseModel
import code_setup.app_models.response_.ProfileResponseModel
import code_setup.app_models.response_.UpdateStatusResponseModel
import code_setup.app_util.*
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.location_utils.MyTracker
import code_setup.app_util.socket_work.SocketService
import code_setup.db_.AppDatabase
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.LogoutRequestData
import code_setup.ui_.home.views.earning.EarningFragment
import code_setup.ui_.home.views.qr_fragment.QrCodeActivityForVehicle
import code_setup.ui_.settings.views.ridehistory.RideHistoryActivity
import code_setup.ui_.home.views.schedule.ScheduleActivity
import code_setup.ui_.settings.views.support.SupportActivity
import code_setup.ui_.settings.ProfileActivity
import code_setup.ui_.settings.views.about.AboutFragment
import code_setup.ui_.settings.views.profile.RateReviewActivity
import com.base.mvp.BasePresenter
import com.electrovese.setup.R
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.default_loading.*
import kotlinx.android.synthetic.main.layout_side_drawer.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.profile_top_view.*
import kotlinx.android.synthetic.main.status_badge_layout.*
import kotlinx.android.synthetic.main.trans_toolbar_lay.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject


class HomeActivity : CoreActivity(), NavigationView.OnNavigationItemSelectedListener, HomeView,
    OnMapReadyCallback, MyTracker.ADLocationListener {
    override fun whereIAM(loc: ADLocation?) {
        if (loc != null) {
            try {
                mDefaultLocation = LatLng(loc.lat, loc.longi)
                Prefs.putDouble(CommonValues.LATITUDE, loc.lat)
                Prefs.putDouble(CommonValues.LONGITUDE, loc.longi)
                Log.e(
                    TAG, "whereIAM " + loc.address
                )
                defaultCameraFocus()
            } catch (e: Exception) {
            }
        }
    }

    private var locationPermissionAlert: Dialog? = null
    private var isTourActive: Boolean = false
    private var isOtherFragment: Boolean = false
    private var newRequestAlert: Dialog? = null
    lateinit var mRunnable: Runnable
    lateinit var mHandler: Handler
    private var markersCurrentLication: Marker? = null
    lateinit var handler: Handler
    private lateinit var upcomingBottomSheet: UpcomingBottomSheet
    private lateinit var mapFragment: SupportMapFragment
    var isOpened: Boolean = false
    var TAG: String = HomeActivity::class.java.simpleName
    private var mDefaultLocation: LatLng? = null
    private lateinit var mMap: GoogleMap

    companion object {
        lateinit var homeInstance: HomeActivity
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onResume() {
        super.onResume()
        homeBottomArrow.bringToFront()
        if (!MyTracker.isAirplaneModeOn(BaseApplication.instance.getApplicationContext())) {
            requestLocationPermissions()
        }
        /* try {
             AppUtils.stopVibration(this)
         } catch (e: Exception) {
         }*/
        if (!isTourActive && !isOtherFragment) {
            getCurrentStatus()
        }
    }

    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private var dialogDisclosure: Dialog? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    //    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    var ifLocationPermissionDenied: Boolean = false

    //    var permissionListner: OnCallBackHandler? = null
    fun askLocationPermission() {

        if (!checkPermissions()) {
            if (!ifLocationPermissionDenied) {
                if (dialogDisclosure != null && dialogDisclosure!!.isShowing) {
                    return
                }
                dialogDisclosure = AppDialogs.openDisclosureAlertLocation(
                    this,
                    Any(),
                    Any(),
                    object : OnBottomDialogItemListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                            when (type) {
                                1 -> {
                                    requestPermissions()
//                                    param.onAccepted(Any())
                                }
                                0 -> {

                                }
                            }

                        }
                    })
            } else {
//                param.onCanceled(Any())
                AppUtils.showToast(
                    getString(R.string.str_location_permission_alert_message)
                )
            }

        } else {
            getLastLocation()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))

        when (int) {
            NetworkRequest.REQUEST_CHANGE_STATUS -> {
                var responseData = list as UpdateStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    statusUpdateBtn.isChecked = false
                    statusTextUpdate(statusUpdateBtn.isChecked)
                    Prefs.putString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "")
                    Prefs.putBoolean(CommonValues.IS_DRIVER_AVAILABLE, statusUpdateBtn.isChecked)
                    removeLocationUpdates()
                } else {
                    AppUtils.showToast(getString(R.string.error_session_expired))
                    logoutUser()
                }
            }
            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    if (responseData.response_obj.status.equals("INACTIVE")) {
                        statusUpdateBtn.isChecked = false
                        Prefs.putString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "")
                        Prefs.putBoolean(
                            CommonValues.IS_DRIVER_AVAILABLE,
                            statusUpdateBtn.isChecked
                        )
                        try {
                            Prefs.putString(CommonValues.ACTIVE_VEHICLE_NUMBER, "")
                        } catch (e: Exception) {
                        }
                        statusTextUpdate(statusUpdateBtn.isChecked)
                        removeLocationUpdates()

                    } else {
                        statusUpdateBtn.isChecked = true
                        Prefs.putBoolean(
                            CommonValues.IS_DRIVER_AVAILABLE,
                            statusUpdateBtn.isChecked
                        )
                        statusTextUpdate(statusUpdateBtn.isChecked)
                        Prefs.putString(
                            CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID,
                            responseData.response_obj.vehicle_id
                        )
                        try {
                            Prefs.putString(
                                CommonValues.ACTIVE_VEHICLE_NUMBER,
                                responseData.response_obj.vehicle_number
                            )
                            vehicleNumberSideBar.setText(responseData.response_obj.vehicle_number)
                        } catch (e: Exception) {
                        }

                        try {
                            if (Prefs.getBoolean(CommonValues.IS_GUIDE, false)) {
                                vehicleNameLableTxt.setText(R.string.guide_name)
                            } else vehicleNameLableTxt.setText(R.string.str_you_vehicle_number)
                        } catch (e: Exception) {
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            try {
                                stopService(Intent(this, SocketService::class.java))
                                Handler().postDelayed(Runnable {
                                    ContextCompat.startForegroundService(
                                        this,
                                        Intent(this, SocketService::class.java)
                                    )
                                }, 200)
                            } catch (e: Exception) {
                            }
                        } else {
                            startService(Intent(this, SocketService::class.java))
                        }
                        registerLocationUpdateEvents()
                    }
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                    // if ongoing tour is avaialble
                    if (responseData.response_obj != null && responseData.response_obj.current_booking != null) {
                        Prefs.putString(
                            CommonValues.CURRENT_BOOKING_ID,
                            responseData.response_obj.current_booking
                        )
                        openTourUpdatesFragment(responseData.response_obj.current_booking)

                    } else {
                        resetActiveTour()
                        Prefs.putString(CommonValues.CURRENT_BOOKING_ID, "")
//                        openUpComingTour()
                    }

                } else {
//                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
//                    logoutUser()
                }
            }
            NetworkRequest.REQUEST_LOGOUT_USER -> {

            }
            NetworkRequest.REQUEST_PROFILE_CODE -> {
                var responseData = list as ProfileResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    userNameVw.setText(responseData.response_obj.name)
                    userCountryNameVw.setText("Provider: " + responseData.response_obj.provider_name)
                    usermNumberVw.setText(responseData.response_obj.country_code + " " + responseData.response_obj.contact)
                    userEmailVw.setText(responseData.response_obj.email)
                    userImageView.setImageURI(responseData.response_obj.user_image)


                    try {
                        var userData = getUserData() as LoginResponseModel.ResponseObj
                        userData.contact =
                            responseData.response_obj.country_code + " " + responseData.response_obj.contact
                        userData.email = responseData.response_obj.email
                        userData.name = responseData.response_obj.name
                        userData.user_image = responseData.response_obj.user_image
                        Prefs.putString(CommonValues.USER_DATA, Gson().toJson(userData))
                    } catch (e: Exception) {
                    }
                } else if (responseData.response_code == NetworkCodes.SESSION.nCodes) {
                    AppUtils.showToast(getString(R.string.error_session_expired))
                    logoutUser()

                }
            }
        }


    }

    fun resetActiveTour() {
        isTourActive = false
    }

    private fun openTourUpdatesFragment(currentBooking: String) {
        isTourActive = true
        viewModes(VIEW_MODE.BOTTOM_FRAGMENT.nCodes, TourUpdatesFragment())
        Handler().postDelayed(Runnable {
            EventBus.getDefault().postSticky(
                CustomEvent<Any>(
                    EVENTS.REQUEST_TOUR_DETAIL,
                    currentBooking
                )
            )
        }, 100)
    }


    override fun showProgress() {
        homeLoaderView.visibility = View.VISIBLE
        avd!!.start()
    }

    override fun hideProgress() {
        homeLoaderView.visibility = View.GONE
        avd!!.stop()
    }

    override fun noResult() {

    }

    @Inject
    open lateinit var presenter: HomePresenter
    var avd: AnimatedVectorDrawable? = null

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun getScreenUi(): Int {
        return R.layout.activity_main
    }

    override fun onError() {

    }

    override fun setPresenter(presenter: BasePresenter<*>) {

    }

    lateinit var action: Runnable

    private lateinit var addDatabase: AppDatabase
    override fun onDestroy() {
        AppDatabase.destroyInstance()
        unregisterNetworkChanges();
        super.onDestroy()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeInstance = this
        toolbar_root.bringToFront()
//        userImageEdit.visibility = View.GONE
        setupsideMenuDetail()
        statusUpdateBtn.isChecked = checkCurrentStatus()
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        nav_view.setNavigationItemSelectedListener(this)

        clickCalls()

        presenter.captureInfo(NetworkRequest.REQUEST_CAPTURE_INFO, AppUtils.getCaptureInfoData())

        action = Runnable { repeatAnimation() }
        avd = iv_line.getBackground() as AnimatedVectorDrawable
        avd!!.registerAnimationCallback(object : Animatable2.AnimationCallback() {
            override fun onAnimationEnd(drawable: Drawable) {
                avd = iv_line.getBackground() as AnimatedVectorDrawable
                avd!!.start()
            }
        })
        avd!!.start()
        mapSetup()
        Handler().postDelayed(Runnable {
            progress_loading.visibility = View.GONE
        }, 3000)

        back_toolbar.setOnClickListener {

            if (BaseApplication.instance.CURRENT_VIEW_HOME == 1) {
                backToHomeView()
                toggleStatusUpdate()
            } else {
                if (isOpened) {
                    closeDrawer()
                } else
                    openDrawer()
            }

        }

        statusUpdateBtn.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
//                if (p1) {
//                    statusTextUpdate(true)
//                } else {
//                    statusTextUpdate(false)
//                }'
                Log.d("onCheckedChanged", " " + p1)
            }
        })
        statusUpdateBtn.setOnClickListener {
            if (!AppUtils.checkNetwork()) {
                AppUtils.showToast(getString(R.string.no_internet_connection))
                return@setOnClickListener
            }
            AnimUtils.rotateViewAnimatio(statusIconBadge)
            try {
                getProfileData() // used to check user session
            } catch (e: Exception) {
            }
            if (statusUpdateBtn.isChecked) {
                AppDialogs.openDialogScaneAlert(
                    this@HomeActivity,
                    Any(),
                    Any(),
                    object : OnBottomDialogItemListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
//                               statusUpdateBtn.isChecked = false
                            when (type) {
                                0 -> {
                                    checkCurrentStatus()
                                }
                                1 -> {
//                                    viewModes(VIEW_MODE.FRAGMENT.nCodes, QrCodeFragment())
                                    activitySwitcher(
                                        this@HomeActivity,
                                        QrCodeActivityForVehicle::class.java,
                                        null
                                    )
                                }
                            }

                        }
                    })
            } else {
                if (!isActiveBooking()) {
                    AppDialogs.openDialogGoOfflineAlert(
                        this@HomeActivity,
                        Any(),
                        Any(),
                        object : OnBottomDialogItemListener<Any> {
                            override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
//                               statusUpdateBtn.isChecked = false
                                when (type) {
                                    0 -> {
                                        checkCurrentStatus()
                                    }
                                    1 -> {
                                        changeDriverStatus()
                                    }
                                }

                            }
                        })
                } else {
                    statusUpdateBtn.isChecked = true
                    AppUtils.showToast(getString(R.string.str_complete_trip_first))
                }
            }
        }
        setFullViewDrawer()

//        getCurrentStatus()
        getIntentData()
        homeBottomArrow.setOnTouchListener { v, event ->
            when (event?.action) {
                MotionEvent.ACTION_DOWN -> {
                    openUpComingTour()
                }
            }
            v?.onTouchEvent(event) ?: true
        }

        intNetworkListner()
//      THIS ONLY FOR TESTING PURPOSE------------
//      showNewRequestDialog(CommonValues.notificationModel)

        getProfileData()
        checkGpsLocation()
    }

    private fun changeDriverStatus() {
        presenter.changeDriverStatus(
            NetworkRequest.REQUEST_CHANGE_STATUS,
            ChangeDriverStatusRequest(
                Prefs.getString(
                    CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID,
                    ""
                )!!
            )
        )
    }

    private fun getProfileData() {
        Handler().postDelayed(Runnable {
            presenter.getProfileData(NetworkRequest.REQUEST_PROFILE_CODE)
        }, 1000)

    }

    private fun getIntentData() {

        if (intent.getBooleanExtra(CommonValues.IS_FROM_MESSAGE_NOTIFICATION, false)) {
            var notificatiobId = intent.getIntExtra(CommonValues.NOTIFICATION_ID, 1)
            var notificationDAta = intent.getSerializableExtra(CommonValues.TOUR_DATA)
            var nData = notificationDAta as NotificationModel
//            Log.d("onMessage", " MSG " + Gson().toJson(nData))
            Handler().postDelayed(Runnable {
                var dataBody = Gson().fromJson<NotificationDataBodyModel>(
                    nData.data.toString(),
                    NotificationDataBodyModel::class.java
                )
                try {
                    dataBody.chat!!.sender_name = AppUtils.getNameFromTitle(notificationDAta.title)
                    Log.d("getIntentData", " MSG " + Gson().toJson(dataBody))
                    Log.d(
                        "getIntentData",
                        " MSG " + dataBody.user_name + '\n' + dataBody.user_image
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                }
//                Log.d("onMessage", " MSG " + Gson().toJson(dataBody))
//                   isFromNotification=true
//                   dataBody.notificatiobId = notificatiobId.toString()// notificatcion id added to model
//                   openTourUpdatesFragment(dataBody.booking_id)
                EventBus.getDefault().postSticky(
                    CustomEvent<Any>(
                        EVENTS.REQUEST_TOUR_CHAT_MESSAGE,
                        dataBody
                    )
                )
            }, 2000)
        }
    }

    private fun getCurrentStatus() {
        Handler().postDelayed(Runnable {
            presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
        }, 850)

    }

    private fun openUpComingTour() {
        homeLoaderView.visibility = View.GONE
        BaseApplication.instance.CURRENT_VIEW_HOME = VIEW_MODE.LIST_BOTTOM_FRAGMENT.nCodes
        Handler().postDelayed(Runnable {
            upcomingBottomSheet = UpcomingBottomSheet()
            try {
                upcomingBottomSheet.show(supportFragmentManager, upcomingBottomSheet.getTag())
            } catch (e: Exception) {
                upcomingBottomSheet.dismiss()
                upcomingBottomSheet.show(supportFragmentManager, upcomingBottomSheet.getTag())
            }

        }, 10)
    }

    fun statusTextUpdate(b: Boolean) {
        statusUpdateBtn.isChecked = b
        if (b) {
            statusIconBadge.setBackgroundResource(R.mipmap.ic_online_sign)
            statusText.setText(R.string.str_you_are_available)
            statusTextMessage.setText(R.string.str_your_tours_are_now_visible)
            txtToolbartitle.setText(R.string.str_available)
            vehiclePlateHolder.visibility = View.VISIBLE
        } else {
            statusIconBadge.setBackgroundResource(R.mipmap.ic_offline_sign)
            statusText.setText(R.string.str_you_are_offline_now)
            statusTextMessage.setText(R.string.str_your_services_are_unavaialble)
            txtToolbartitle.setText(R.string.str_unavailable)
            vehiclePlateHolder.visibility = View.GONE
        }
    }

    private fun checkCurrentStatus(): Boolean {
        statusUpdateBtn.isChecked =
            !Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "").equals(other = "")
        statusTextUpdate(
            !Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "").equals(
                other = ""
            )
        )
        showHideStatusBadge()

        return !Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "").equals(other = "")

    }

    private fun showHideStatusBadge() {
        badgeHolderView.visibility = View.VISIBLE

        // Initialize the handler instance
        mHandler = Handler()
        mHandler.postDelayed({
            // Do something here
            // AnimUtils.moveAnimationY_up_down(badgeHolderView, -200f, 1000)
//            if (!Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "").equals(""))
//                badgeHolderView.visibility = View.GONE
        }, 10000) // 10 seconds delay task execution

    }

    /**
     * Update user sidemenu Detail
     */
    fun setupsideMenuDetail() {
        try {
            if (getUserData() != null) {
                var userData = getUserData() as LoginResponseModel.ResponseObj
                userNameVw.setText(userData.name)
                usermNumberVw.setText("" + userData.contact)
                userEmailVw.setText(userData.email)
                userImageView.setImageURI(userData.user_image)
            }
        } catch (e: Exception) {
        }
    }

    private fun setFullViewDrawer() {
        var displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        var params: DrawerLayout.LayoutParams = nav_view.layoutParams as DrawerLayout.LayoutParams
        params.width = displayMetrics.widthPixels;
        nav_view.layoutParams = params
    }

    /**
     * Reset  view to home screen
     */
    fun backToHomeView() {
        BaseApplication.instance.CURRENT_VIEW_HOME = 0
        back_toolbar.setImageResource(R.mipmap.ic_menu)
//        AnimUtils.homeAnimationX(back_toolbar, false)
        back_toolbar.visibility = View.VISIBLE
        fragmentContainer.visibility = View.GONE
        mainView.visibility = View.VISIBLE
        statusUpdateBtn.visibility = View.VISIBLE
        txtToolbartitle.setTextColor(
            resources.getColor(R.color.colorBlack)
        )
        badgeHolderView.visibility = View.VISIBLE
        removeFragment(TourUpdatesFragment(), R.id.bottomSheetContainer)
//        checkCurrentStatus()
        getCurrentStatus()
        defaultCameraFocus()

//        loderView()
    }

    private fun toggleStatusUpdate() {
        statusUpdateBtn.visibility = View.VISIBLE
        if (Prefs.getBoolean(CommonValues.IS_DRIVER_AVAILABLE, false)) {
            txtToolbartitle.setText(R.string.str_available)
        } else {
            txtToolbartitle.setText(R.string.str_unavailable)
        }
    }

    /* private fun showNewRequestDialog(
         notificationModel: CurrentStatusResponseModel.ResponseObj.PendingJob,
         isFromNotificationClick: Boolean
     ) {
         badgeHolderView.visibility = View.GONE
         *//* if (newRequestAlert != null) {
             if (newRequestAlert!!.isShowing) {
                 newRequestAlert!!.dismiss()
             }
         }*//*
        AppDialogs.openNewRequestDialog(
            this,
            notificationModel,
            isFromNotificationClick,
            object : OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    when (type) {
                        EVENTS.ACCEPT_TOUR -> {
                            presenter.updateBookingRequest(
                                NetworkRequest.REQUEST_ACCEPT_BOOKING,
                                UpdateBookingRequestModel((t as CurrentStatusResponseModel.ResponseObj.PendingJob).id)
                            )
//                            openUpComingTour()
                            EventBus.getDefault()
                                .postSticky(CustomEvent<Any>(EVENTS.REFRESH_SCREEN, ""))
                        }
                        EVENTS.REJECT_TOUR -> {
                            Log.e(
                                "REJECT_TOUR ",
                                "  " + (t as CurrentStatusResponseModel.ResponseObj.PendingJob).id
                            )
                            presenter.updateBookingRequest(
                                NetworkRequest.REQUEST_REJECT_BOOKING,
                                UpdateBookingRequestModel((t as CurrentStatusResponseModel.ResponseObj.PendingJob).id)
                            )
                        }
                    }

                }

            })
    }*/


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun requestLocationPermissions() {
        askLocationPermission()
        /*  if (!checkPermissions()) {
              requestPermissions()
          } else {
              getLastLocation()
          }*/


        /* Locus.getCurrentLocation(this) { result ->
             result.location?.let {
                 *//* Received location update *//*
                val latLng = LatLng(result.location!!.latitude, result.location!!.longitude)
                mDefaultLocation = latLng
                goNext()
                Log.e("onSuccess", " LOCATION PERMISSION GRANTED  ")
            }
            result.error?.let {
                *//* Received error! *//*
                Log.e("onFailed", " LOCATION PERMISSION FAILED  ")
                *//*AppDialogs.openLocationPermissionAlert(
                    this,
                    Any(),
                    Any(),
                    object : OnBottomDialogItemListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                        }

                    })*//*
                getLastLocation()
            }
        }*/

    }


    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    //    private var ifLocationPermissionDenied: Boolean = false
    private fun requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            // Provide an additional rationale to the user. This would happen if the user denied the
            // request previously, but didn't check the "Don't ask again" checkbox.
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            showLocationRequiredAlert()

        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            Log.i(TAG, "Requesting permission")
            if (!ifLocationPermissionDenied)
                startLocationPermissionRequest()
            else {
                showLocationRequiredAlert()
            }
        }
    }

    private fun showLocationRequiredAlert() {
        if (locationPermissionAlert != null) {
            if (locationPermissionAlert!!.isShowing) {
                locationPermissionAlert!!.dismiss()
            }

        }
        locationPermissionAlert = AppDialogs.openLocationPermissionAlert(
            this,
            "",
            "",
            object : OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    when (type) {
                        1 -> {
//                                startLocationPermissionRequest()
                        }
                    }
                }
            })

    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult")

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                grantResults.isEmpty() -> Log.i(TAG, "User interaction was cancelled.")

                // Permission granted.
                (grantResults[0] == PackageManager.PERMISSION_GRANTED) -> getLastLocation()

                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                else -> {
                    ifLocationPermissionDenied = true
                    /*  showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                          View.OnClickListener {
                              // Build intent that displays the App settings screen.
                              val intent = Intent().apply {
                                  action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                  data = Uri.fromParts("package", APPLICATION_ID, null)
                                  flags = Intent.FLAG_ACTIVITY_NEW_TASK
                              }
                              startActivity(intent)
                          })*/
                }
            }
        } else if (requestCode == 100) {
            when {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                grantResults.isEmpty() -> Log.i(
                    TAG,
                    "User interaction was cancelled.  " + requestCode
                )

                // Permission granted.
                (grantResults[0] == PackageManager.PERMISSION_GRANTED) -> {
                    EventBus.getDefault()
                        .postSticky(CustomEvent<Any>(EVENTS.CALL_PERMISSION_GRANTED, ""))
                }
                else -> {
                    Log.i(TAG, "DENIED   User interaction was cancelled.  " + requestCode)
                    AppUtils.showToast(getString(R.string.call_permiss_requirted))
                }
            }
        }
    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     *
     * Note: this method should be called after location permission has been granted.
     */
//    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation
            .addOnCompleteListener { taskLocation ->
                if (taskLocation.isSuccessful && taskLocation.result != null) {

                    val location = taskLocation.result
                    location?.latitude?.let { Prefs.putDouble(CommonValues.LATITUDE, it) }
                    location?.longitude?.let { Prefs.putDouble(CommonValues.LONGITUDE, it) }
                    val latLng = LatLng(location!!.latitude, location!!.longitude)
                    mDefaultLocation = latLng
                    goNext()
//                  updateDriverCurrentLocation()
                    mDefaultLocation = LatLng(location!!.latitude, location!!.longitude)
                } else {
                    Log.w(TAG, "getLastLocation:exception", taskLocation.exception)
                    requestNewLocationData()
                    try {
                        if (AppUtils.getMyLocation()!!.latitude != 0.0) {
                            // Notify location
                            var location = Location("CurrentLocation")
                            location.setLatitude(AppUtils.getMyLocation()!!.latitude)
                            location.setLongitude(AppUtils.getMyLocation()!!.longitude)
//                            DbUtilsLocation.saveCurrentLocation(this, location, false)
                            EventBus.getDefault()
                                .postSticky(CustomEvent<Any>(EVENTS.CURRENT_LOCATION, true))
                        }

                    } catch (e: Exception) {
                    }
                }
            }
    }

    // FusedLocationProviderClient
    // object
    var mFusedLocationClient: FusedLocationProviderClient? = null

    @SuppressLint("MissingPermission")
    open fun requestNewLocationData() {
        // Initializing LocationRequest
        // object with appropriate methods
        val mLocationRequest = LocationRequest()
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        mLocationRequest.setInterval(3)
        mLocationRequest.setFastestInterval(0)
        mLocationRequest.setNumUpdates(2)

        // setting LocationRequest
        // on FusedLocationClient
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest,
            mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.getLastLocation()
            Log.w(TAG, "onLocationResult" + mLastLocation.latitude + "  " + mLastLocation.longitude)
            var location = Location("CurrentLocation")
            location.setLatitude(mLastLocation.latitude)
            location.setLongitude(mLastLocation.longitude)
//            permissionListner!!.onAccepted(location)
            Prefs.putDouble(CommonValues.LATITUDE, mLastLocation!!.latitude)
            Prefs.putDouble(CommonValues.LONGITUDE, mLastLocation!!.longitude)
//            DbUtilsLocation.saveCurrentLocation(this@CoreActivity, location, false)
            EventBus.getDefault()
                .postSticky(CustomEvent<Any>(EVENTS.CURRENT_LOCATION, true))
        }


    }

    @SuppressLint("MissingPermission")
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun goNext() {
        registerLocationUpdateEvents()
        MyTracker(getApplicationContext(), this).track()
        if (!isTourActive) {
            Handler().postDelayed({
                AnimUtils.startRevealAnimation(framLayoutMain, rootView)
            }, 100)
            defaultCameraFocus()
        }
    }

    private val DEFAULT_ZOOM: Float = 14f

    @SuppressLint("MissingPermission")
    private fun defaultCameraFocus() {
        try {
            markersCurrentLication!!.remove()
        } catch (e: Exception) {
        }
        if (mDefaultLocation == null) {
            val myLocation = AppUtils.getMyLocation()
            mDefaultLocation = myLocation
        }
        if (mDefaultLocation != null) {
            val position = CameraPosition.Builder()
                .target(mDefaultLocation!!)
                .zoom(DEFAULT_ZOOM)
                .build()
//              mMap.moveCamera(CameraUpdateFactory.newCameraPosition(position))
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position))
            markersCurrentLication = mMap.addMarker(
                MarkerOptions().position(mDefaultLocation!!).flat(true).icon(
                    BitmapDescriptorFactory.fromResource(R.mipmap.ic_navigation)
                ).anchor(0.5f, 0.5f)
            )
            try {
                if (checkPermissions())
                    mMap.setMyLocationEnabled(true)
            } catch (e: Exception) {
            }

            mMap.uiSettings.setRotateGesturesEnabled(false)
            mMap.uiSettings.isTiltGesturesEnabled = false
            mMap.uiSettings.setMyLocationButtonEnabled(true)

            setMapMargins()
        } else {
            mDefaultLocation = AppUtils.getMyLocation()
        }
        homeBottomArrow.bringToFront()
        homeBottomArrow.isClickable = true
    }

    fun setMapMargins() {
        if (mapFragment != null &&
            mapFragment.requireView()!!.findViewById<View>(Integer.parseInt("1")) != null
        ) {
            val locationButton =
                (mapFragment.requireView()!!
                    .findViewById<View>(Integer.parseInt("1")).parent as View).findViewById<View>(
                    Integer.parseInt(
                        "2"
                    )
                )
            val rlp = locationButton.layoutParams as (RelativeLayout.LayoutParams)
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            rlp.setMargins(0, 0, 30, 150)
        }
    }

    private fun mapSetup() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        presenter.customMap(this@HomeActivity, mMap)
        defaultCameraFocus()
//add location button click listener
        //add location button click listener
        mMap.setOnMyLocationButtonClickListener(OnMyLocationButtonClickListener { //TODO: Any custom actions
            checkGpsLocation()
            false
        })
    }

    private fun repeatAnimation() {
        avd!!.start()
        iv_line.postDelayed(action, 5000) // Will repeat animation in every 1 second
    }

    private fun clickCalls() {
        sideBackBtn.setOnClickListener {
            closeDrawer()
        }
        menuyourRideHistoryBtn.setSafeOnClickListener {
            closeDrawer()
            activitySwitcher(this, RideHistoryActivity::class.java, null)
        }
        /*------*/
        menuAbutBtn.setSafeOnClickListener {
            viewModes(VIEW_MODE.FRAGMENT.nCodes, AboutFragment())

        }
        menuEarningBtn.setSafeOnClickListener {
            viewModes(VIEW_MODE.FRAGMENT.nCodes, EarningFragment())
        }
        topViewdrawerHeaderProfile.setOnClickListener {
            closeDrawer()
            activitySwitcher(this, ProfileActivity::class.java, null)
        }
        menuSuptBtn.setOnClickListener {
            closeDrawer()
            activitySwitcher(this, SupportActivity::class.java, null)
        }
        menuScheduleBtn.setOnClickListener {
            closeDrawer()
            activitySwitcher(this, ScheduleActivity::class.java, null)
        }
        menuLogoutBtn.setOnClickListener {
            if (!isActiveBooking()) {
                logoutUser()
            } else {
                AppUtils.showToast(getString(R.string.str_complete_trip_first))
            }

        }
        menuRatingsBtn.setOnClickListener {
            closeDrawer()
            activitySwitcher(this, RateReviewActivity::class.java, null)
        }
    }

    private fun logoutUser() {
        try {
            if (!AppUtils.checkNetwork()) {
                AppUtils.showToast(getString(R.string.no_internet_connection))
                return
            }

            try {
                if (isVehicleAvailable())
                    changeDriverStatus()
                Handler().postDelayed(Runnable {
                    var driverId = ""
                    if (getUserData() != null)
                        driverId = (getUserData() as LoginResponseModel.ResponseObj).driver_id

                    logoutUserNow()
                    presenter.logoutUser(
                        NetworkRequest.REQUEST_LOGOUT_USER,
                        LogoutRequestData(driverId as String)
                    )

                }, 200)
            } catch (e: Exception) {
                logoutUserNow()
            }
        } catch (e: Exception) {
            logoutUserNow()
        }


    }

    private fun isVehicleAvailable(): Boolean {
        return !Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "").equals("")
    }

    fun viewModes(fMode: Int, aFragment: Fragment) {
        BaseApplication.instance.CURRENT_VIEW_HOME = fMode
        when (fMode) {
            VIEW_MODE.FRAGMENT.nCodes -> {
                Log.d("view mode ", " fragments")
                back_toolbar.setImageResource(R.mipmap.ic_arrow_right_black)
                AnimUtils.homeAnimationX(back_toolbar, false)
                back_toolbar.rotation = 180f
                mainView.visibility = View.GONE
                fragmentContainer.visibility = View.VISIBLE
                statusUpdateBtn.visibility = View.INVISIBLE
                bottomSheetContainer.visibility = View.GONE
                replaceContainer(aFragment, R.id.fragmentContainer)
                closeDrawer()
                progress_loading.visibility = View.GONE
            }
            VIEW_MODE.BOTTOM_FRAGMENT.nCodes -> {
                Log.d("view mode ", " bottom fragments")
                closeDrawer()
                fragmentContainer.visibility = View.GONE
                bottomSheetContainer.visibility = View.VISIBLE
                bottomSheetContainer.bringToFront()
                replaceContainer(aFragment, R.id.bottomSheetContainer)
                progress_loading.visibility = View.GONE
                mMap.setPadding(0, 0, 0, bottomSheetContainer.height)
            }

            VIEW_MODE.ACTIVITY.nCodes -> {
                backToHomeView()
                AnimUtils.homeAnimationX1(back_toolbar, true)
                removeFragment(TourUpdatesFragment(), R.id.bottomSheetContainer)
            }
        }
    }

    private fun closeDrawer() {
        isOpened = false
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    private fun openDrawer() {
        isOpened = true
        drawer_layout.openDrawer(GravityCompat.START)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
//        getIntentData()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            closeDrawer()
            back_toolbar.setImageResource(R.mipmap.ic_menu)
            fragmentContainer.visibility = View.GONE
            mainView.visibility = View.VISIBLE
            toggleStatusUpdate()
//
            isOtherFragment = false
            Log.e("onBackPressed", "=====  " + isOtherFragment)
            Log.e("onBackPressed", "=====  " + BaseApplication.instance.CURRENT_VIEW_HOME)

            when (BaseApplication.instance.CURRENT_VIEW_HOME) {
                VIEW_MODE.BOTTOM_FRAGMENT.nCodes -> {
                    viewModes(VIEW_MODE.ACTIVITY.nCodes, Fragment())
                }
                VIEW_MODE.FRAGMENT.nCodes -> {
                    if (isTourActive) {
                        getCurrentStatus()
                    }
                }

            }
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " HOME SCREEN " + event.type)
        try {
            Log.d("onMessage", " HOME SCREEN " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {
            EVENTS.REQUEST_NEW_TOUR_JOB -> {// In case of push notification
                /* var dataBody = Gson().fromJson<NotificationDataBodyModel>(
                     (event.oj as NotificationModel).data.toString(),
                     NotificationDataBodyModel::class.java
                 )
                 showNewRequestDialog(dataBody, false)*/
                getCurrentStatus()
            }
            EVENTS.SOCKET_REQUEST_NEW_TOUR_JOB -> {// In case of  socket notification
//                var dataBody = Gson().fromJson<NotificationDataBodyModel>(
//                    (event.oj as SocketUpdateModel).message.data.toString(),
//                    NotificationDataBodyModel::class.java
//                )
//                showNewRequestDialog(dataBody, false)
                getCurrentStatus()
            }
            EVENTS.NETWORK_CONNECTION_CALLBACK -> {
                if (event.oj as Boolean) {
                    AppUtils.showSnackBarConnectiivity(
                        this,
                        getString(R.string.str_connected_to_internet),
                        true
                    )
                    statusUpdateBtn.isEnabled = true
                } else {
                    statusUpdateBtn.isEnabled = false
                    AppUtils.showSnackBarConnectiivity(
                        this,
                        getString(R.string.str_no_internet_conection),
                        false
                    )
                }

            }
            EVENTS.PROFILE_UPDATED -> {
                getProfileData()
            }
            EVENTS.CURRENT_LOCATION -> {
                if (!isTourActive) {
                    goNext()
                }
//                setCurrentLocation()
            }
        }
        removeStickyEvent()
    }

    private fun removeStickyEvent() {
        var stickyEvent = EventBus.getDefault().getStickyEvent(CustomEvent::class.java)
// Better check that an event was actually posted before
        if (stickyEvent != null) {
            // "Consume" the sticky event
            EventBus.getDefault().removeStickyEvent(stickyEvent);
            // Now do something with it
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        // menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun startActivityForResult(intent: Intent?, requestCode: Int) {
        super.startActivityForResult(intent, requestCode)
        when (requestCode) {
            1001 -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED ||
                        ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_BACKGROUND_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
//                    gpsTracker = new GPSTracker(this);
//                    if (gpsTracker.canGetLocation()) {
//                        latitude = gpsTracker.getLatitude();
//                        longitude = gpsTracker.getLongitude();
//                    }
                        goNext()
                    } else {
                        requestPermissions(
                            arrayOf(
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION
                            ), 10
                        );
                    }
                }
            }

        }
    }

    fun refreshMapPadding() {
        if (mMap != null) {
            mMap.setPadding(0, 0, 0, 0);
            setMapMargins()
        }
    }

    fun isOtherFragmentOpened(b: Boolean) {
        isOtherFragment = b
    }

}
