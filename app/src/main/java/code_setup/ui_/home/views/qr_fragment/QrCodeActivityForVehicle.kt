package code_setup.ui_.home.views.qr_fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.ADLocation
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.ChangeDriverStatusRequest
import code_setup.app_models.request_.OnBoardingRequestModel
import code_setup.app_models.response_.OnBoardingResponseModel
import code_setup.app_models.response_.TourDetailResponseModel
import code_setup.app_models.response_.UpdateStatusResponseModel
import code_setup.app_util.AppDialogs
import code_setup.app_util.AppUtils
import code_setup.app_util.CommonValues
import code_setup.app_util.Prefs
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.app_util.location_utils.MyTracker
import code_setup.app_util.socket_work.SocketService
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.AllTourMembersAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.HomeActivity
import code_setup.ui_.home.views.chat_.ChatScreen
import com.budiyev.android.codescanner.*
import com.electrovese.setup.R
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.tbruyelle.rxpermissions3.RxPermissions
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_riders.*
import kotlinx.android.synthetic.main.layout_sacn_qr_activity.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


class QrCodeActivityForVehicle : CoreActivity(), /*ZXingScannerView.ResultHandler,*/ HomeView,
    MyTracker.ADLocationListener {
    lateinit var adapterMmber: AllTourMembersAdapter
    val TAG: String = QrCodeActivityForVehicle::class.java.simpleName
    private var mCurrenttLocation: LatLng? = null

    override fun whereIAM(loc: ADLocation?) {

    }


    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun getScreenUi(): Int {

        return R.layout.layout_sacn_vehicle_qr_activity
    }

    override fun onResume() {
        super.onResume()
        if (!checkPermissions()) {
            requestPermissions()
        } else {
            goScan()
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
//        mScannerView.stopCamera()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onResponse(list: Any, int: Int) {
        Log.e("onResponse", " " + Gson().toJson(list))
        when (int) {
            code_setup.net_.NetworkRequest.REQUEST_CHANGE_STATUS -> {
                var responseData = list as UpdateStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    AppDialogs.openDialogScaneSuccesssAlert(
                        this@QrCodeActivityForVehicle,
                        responseData,
                        Any(),
                        object : OnBottomDialogItemListener<Any> {
                            override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                                when (type) {
                                    0 -> {
                                        HomeActivity.homeInstance.statusTextUpdate(false)
                                    }
                                    1 -> {
                                        HomeActivity.homeInstance.statusTextUpdate(true)
                                        try {
                                            this@QrCodeActivityForVehicle.startService(
                                                Intent(
                                                    this@QrCodeActivityForVehicle,
                                                    SocketService::class.java
                                                )
                                            )
                                        } catch (e: Exception) {
                                        }
                                    }
                                }
//                                HomeActivity.homeInstance.backToHomeView()
                                onBackPressed()
                            }
                        })
                } else {
                    HomeActivity.homeInstance.statusTextUpdate(false)
                    AppUtils.showSnackBar(
                        this@QrCodeActivityForVehicle,
                        responseData.response_message
                    )
                    goScan()
                }
            }
        }
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        txtTitletoolbar.setText(R.string.str_scan_qr_code)
        txtTitletoolbar.setTextColor(resources.getColor(R.color.colorPrimary))
        txtTitletoolbar.gravity=Gravity.CENTER
        backToolbar.setOnClickListener {
            onBackPressed()
        }

        initScanner()
        MyTracker(getApplicationContext(), this).track()

    }

    private fun requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CAMERA
            )
        ) {
            // Provide an additional rationale to the user. This would happen if the user denied the
            // request previously, but didn't check the "Don't ask again" checkbox.
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            AppDialogs.openCameraPermissionAlert(
                this,
                Any(),
                Any(),
                object : OnBottomDialogItemListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                        //                         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
            /* showSnackbar(R.string.permission_rationale, android.R.string.ok, View.OnClickListener {
                 // Request permission
                 startLocationPermissionRequest()
             })*/

        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            Log.i(TAG, "Requesting permission")
            startPermissionRequest()
        }
    }

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 35
    private fun startPermissionRequest() {
        ActivityCompat.requestPermissions(
            this, arrayOf(Manifest.permission.CAMERA),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    lateinit var codeScanner: CodeScanner
    private fun initScanner() {
        codeScanner = CodeScanner(this, scanner_view)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            Handler(Looper.getMainLooper()).post(
                Runnable {
//                    Toast.makeText(activity!!, "Scan result: ${it.text}", Toast.LENGTH_LONG).show()
//                    AppUtils.showToast(it.text.toString())
                    proceed(it.text.toString())
                })
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            Handler(Looper.getMainLooper()).post(
                Runnable {
                    Toast.makeText(
                        this, "Camera initialization error: ${it.message}",
                        Toast.LENGTH_LONG
                    ).show()
                })
        }

        scanner_view.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    private fun goScan() {
//            mScannerViewAct.setResultHandler(this)
//            mScannerViewAct.startCamera()
        codeScanner.startPreview()
    }

    /*  override fun handleResult(p0: Result?) {
          Log.d("handleResult", "  " + p0.toString())
//        AppUtils.showToast(p0.toString())
          proceed(p0.toString())
      }*/

    private fun proceed(toString: String) {
        Prefs.putString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, toString)
        presenter.changeDriverStatus(
            code_setup.net_.NetworkRequest.REQUEST_CHANGE_STATUS,
            ChangeDriverStatusRequest(toString)
        )
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult")

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                grantResults.isEmpty() -> Log.i(TAG, "User interaction was cancelled.")

                // Permission granted.
                (grantResults[0] == PackageManager.PERMISSION_GRANTED) -> goScan()

                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                else -> {
                    /*  showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                          View.OnClickListener {
                              // Build intent that displays the App settings screen.
                              val intent = Intent().apply {
                                  action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                  data = Uri.fromParts("package", APPLICATION_ID, null)
                                  flags = Intent.FLAG_ACTIVITY_NEW_TASK
                              }
                              startActivity(intent)
                          })*/
                }
            }
        }
    }
}