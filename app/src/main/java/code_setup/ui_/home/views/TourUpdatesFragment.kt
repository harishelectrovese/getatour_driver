package code_setup.ui_.home.views

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreFragment
import code_setup.app_models.other_.ADLocation
import code_setup.app_models.other_.NotificationDataBodyModel
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.other_.event.RIDESTATUS
import code_setup.app_models.request_.StartMovingRequestModel
import code_setup.app_models.request_.UpdateBookingRequestModel
import code_setup.app_models.request_.UpdateRequestModel
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_models.response_.TourDetailResponseModel
import code_setup.app_util.*
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.app_util.location_utils.MyTracker
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.DestinationListAdapter
import code_setup.ui_.home.apapter_.TourMembersAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.chat_.ChatScreen
import code_setup.ui_.home.views.qr_fragment.QrCodeActivityForPickup
import com.base.mvp.BasePresenter
import com.electrovese.setup.R
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import kotlinx.android.synthetic.main.adapter_tour_people_view.*
import kotlinx.android.synthetic.main.fragment_tour_updates_layout.*
import kotlinx.android.synthetic.main.layout_moving_view_holder.*
import kotlinx.android.synthetic.main.layout_start_trip.*
import kotlinx.android.synthetic.main.layout_tour_view_new.*
import kotlinx.android.synthetic.main.layout_tour_view_new.view.*
import kotlinx.android.synthetic.main.layout_trip_destinations.*
import kotlinx.android.synthetic.main.layout_trip_pickup_riders_view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject


class TourUpdatesFragment : CoreFragment(), HomeView, MyTracker.ADLocationListener {

    private var contactString: String = ""
    lateinit var tourDataModel: TourDetailResponseModel
    lateinit var tourID: String
    lateinit var desAdapter: DestinationListAdapter
    var TAG: String = TourUpdatesFragment::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.MyBottomSheetDialogTheme)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)

    }

    /*
    *  E V E N T  BUS
    *  Update Viewe as per respected event
    *  @ Send custom object @CustomEvent
    * */
    @SuppressLint("RestrictedApi")
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " " + event.type)
        when (event.type) {
            EVENTS.REQUEST_TOUR_DETAIL -> {
                tourID = event.oj as String
                getTourDetail()
            }
            EVENTS.REQUEST_TOUR_CHAT_MESSAGE -> {
                Log.d("onMessage", " MSG " + Gson().toJson(event.oj))
                var data = event.oj as NotificationDataBodyModel
//                Log.d("onMessage", " MSG " + Gson().toJson(data))
                tourID = data.booking_id

                var userMdl = TourDetailResponseModel.ResponseObj.User()
                userMdl.user_booking_id = data.booking_id
                userMdl.name = data.chat!!.sender_name
                try {
                    userMdl.user_image = data.user_image
                } catch (e: Exception) {
                }
                val intent = android.content.Intent(
                    activity,
                    ChatScreen::class.java
                )
                intent.putExtra(CommonValues.TOUR_ID, tourID)
                intent.putExtra(
                    CommonValues.TOUR_DATA,
                    userMdl
                )
                /* val options =
                     androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation(
                         activity!!,
                         view!!,
                         view.transitionName
                     )*/
                startActivity(intent/*, options.toBundle()*/)

            }
            EVENTS.REQUEST_START_MOVING_TO_MEETING_POINT_VIEW -> {
                trip_start_moving_holder.visibility = View.VISIBLE
                trip_destination_holder.visibility = View.GONE
                trip_pickup_view_holder.visibility = View.GONE
                pickUpFloating.visibility = View.GONE
                setOnMovingData(event.oj as TourDetailResponseModel.ResponseObj)
                startMovingBtn.setText(R.string.str_start_moving_to_meeting_point)
                showMeetingPOint(event.oj as TourDetailResponseModel.ResponseObj)
            }

            EVENTS.REQUEST_REACHED_MEETING_POINT_VIEW -> {
                trip_start_moving_holder.visibility = View.VISIBLE
                trip_destination_holder.visibility = View.GONE
                trip_pickup_view_holder.visibility = View.GONE
                pickUpFloating.visibility = View.GONE
                setOnMovingData(event.oj as TourDetailResponseModel.ResponseObj)
                startMovingBtn.setText(R.string.str_reached_meeting_point)
                showMeetingPOint(event.oj as TourDetailResponseModel.ResponseObj)
            }

            EVENTS.REQUEST_START_MOVING_VIEW -> {
                trip_start_moving_holder.visibility = View.VISIBLE
//                AnimUtils.SlideToAbove(trip_start_moving_holder)
//                call_user_fBtn.visibility = View.VISIBLE
                trip_destination_holder.visibility = View.GONE
                trip_pickup_view_holder.visibility = View.GONE
                pickUpFloating.visibility = View.GONE
                setOnMovingData(event.oj as TourDetailResponseModel.ResponseObj)

            }
            EVENTS.REQUEST_TOUR_DESTINATIONS_VIEW -> {
                trip_start_moving_holder.visibility = View.GONE
                trip_destination_holder.visibility = View.VISIBLE
                pickUpFloating.visibility = View.GONE
                trip_pickup_view_holder.visibility = View.GONE
                pickUpFloating.bringToFront()
                AnimUtils.moveAnimationXinFromRight(pickUpFloating, -20f, 1000)
                AnimUtils.moveAnimationXinFromRight(supportBtn, -20f, 1000)

                setDestinationData(event.oj as TourDetailResponseModel.ResponseObj)
            }
            EVENTS.REQUEST_TOUR_PICKUP_RIDERS_VIEW -> {
                trip_start_moving_holder.visibility = View.GONE
                trip_destination_holder.visibility = View.GONE
                trip_pickup_view_holder.visibility = View.VISIBLE
                pickUpFloating.visibility = View.GONE
                pickUpFloating.bringToFront()

                setPickUpViewData(event.oj as TourDetailResponseModel.ResponseObj.Route)
            }
            EVENTS.REQUEST_TOUR_PICKUP_RIDER_CODE_SCANNED -> {

                var userBId = event.oj as String

                if (tourDataModel != null && tourDataModel.response_obj.users != null && tourDataModel.response_obj.users.isNotEmpty()) {

                    //   refresh user list manually
                    for (i in 0 until tourDataModel.response_obj.users.size) {
                        if (userBId.equals(tourDataModel.response_obj.users.get(i).user_booking_id)) {
                            tourDataModel.response_obj.users.get(i).is_board = true
                            disableScane()
                        }
                    }
                    //   refresh route bookings list manually
                    for (i in 0 until tourDataModel.response_obj.routes.size) {
                        if (tourDataModel.response_obj.routes.get(i).bookings.isNotEmpty()) {
                            for (j in 0 until tourDataModel.response_obj.routes.get(i).bookings.size)
                                if (userBId.equals(
                                        tourDataModel.response_obj.routes.get(i).bookings.get(
                                            j
                                        ).user_booking_id
                                    )
                                ) {
                                    tourDataModel.response_obj.routes.get(i).bookings.get(j).is_board =
                                        true
                                    setPickUpViewData(tourDataModel.response_obj.routes.get(i))
                                }
                        }
                    }
                    Log.d(
                        "REQUEST_TOUR_PICKUP_RIDER_CODE_SCANNED",
                        "  " + Gson().toJson(tourDataModel)
                    )
                    Handler().postDelayed(Runnable {
                        setDestinationData(tourDataModel.response_obj)
                    }, 1000)
                }

            }
            EVENTS.CALL_PERMISSION_GRANTED -> {
                if (!contactString.isNullOrEmpty()) {
                    InitCall(contactString)
                } else AppUtils.showToast("Contact number not found")
            }
            EVENTS.REFRESH_SCREEN -> {
                getTourDetail()
            }
        }
    }

    private fun showMeetingPOint(responseObj: TourDetailResponseModel.ResponseObj) {
        meetingPointHolder.visibility = View.VISIBLE
        tourMeetingLocation.setText(responseObj.routes.get(0).name)
        navigateMeetingLocation.setOnClickListener {
            navigationIntent(
                LatLng(responseObj!!.routes.get(0).lat, responseObj!!.routes.get(0).lng),
                responseObj!!.routes.get(0).name
            )
        }
    }

    private fun disableScane() {
        if (tourDataModel.response_obj.users.size == 1 && tourDataModel.response_obj.users.get(0).is_board) {
            scanQrBtn.isEnabled = false
            scanQrBtn.setBackgroundResource(R.drawable.rectangle_background_grey)
        }
    }

    private fun setPickUpViewData(responseObj: TourDetailResponseModel.ResponseObj.Route) {
        with(ridersRecyclar) {
            var memberAdapter = TourMembersAdapter(
                activity!!,
                getValidList(responseObj.bookings),
                true,
                object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                        when (type) {
                            CommonValues.MESSAGE_CLICK -> {
                                val intent = android.content.Intent(
                                    activity,
                                    ChatScreen::class.java
                                )
                                intent.putExtra(CommonValues.TOUR_ID, tourID)
                                intent.putExtra(
                                    CommonValues.TOUR_DATA,
                                    t as TourDetailResponseModel.ResponseObj.User
                                )
                                val options =
                                    androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        activity!!,
                                        view!!,
                                        view.transitionName
                                    )
                                startActivity(intent, options.toBundle())
//                                val bndl = Bundle()
//                                bndl.putString(CommonValues.TOUR_ID, tourID)
//                                bndl.putSerializable(CommonValues.TOUR_DATA, tourDataModel.response_obj)
//                                activitySwitcher(activity!!, ChatScreen::class.java, bndl)
                            }
                            CommonValues.CALL_CLICK -> {
                                contactString =
                                    (t as TourDetailResponseModel.ResponseObj.User).contact!!
                                InitCall((t as TourDetailResponseModel.ResponseObj.User).contact!!)
                            }
                        }
                    }
                })
            adapter = memberAdapter
            layoutManager = LinearLayoutManager(activity, VERTICAL, false)

        }

        backToBtn.setOnClickListener {
            EventBus.getDefault().postSticky(
                CustomEvent<Any>(
                    EVENTS.REQUEST_TOUR_DESTINATIONS_VIEW,
                    tourDataModel.response_obj
                )
            )
        }
        disableScane()
        scanQrBtn.setOnClickListener {
            val bndl = Bundle()
            bndl.putString(CommonValues.TOUR_ID, tourID)
            bndl.putSerializable(CommonValues.TOUR_DATA, tourDataModel.response_obj)
            activitySwitcher(activity!!, QrCodeActivityForPickup::class.java, bndl)
        }


    }

    private fun getValidList(bookings: List<TourDetailResponseModel.ResponseObj.Route.Booking>): ArrayList<TourDetailResponseModel.ResponseObj.User> {
        val updatedList = ArrayList<TourDetailResponseModel.ResponseObj.User>()

        for (i in 0 until bookings.size) {
            updatedList.add(
                TourDetailResponseModel.ResponseObj.User(
                    bookings.get(i).user_id,
                    bookings.get(i).contact,
                    bookings.get(i).email,
                    bookings.get(i).name,
                    bookings.get(i).unique_code,
                    "No location name",
                    bookings.get(i).is_board,
                    bookings.get(i).user_booking_id,
                    bookings.get(i).seats, false, "",
                    bookings.get(i).driver_unread_count,
                    bookings.get(i).user_image
                )
            )
        }
        return updatedList
    }

    private fun setDestinationData(rModel: TourDetailResponseModel.ResponseObj) {
        desAdapter.removeAll()
        if (!(rModel?.routes == null && !rModel.routes.isNotEmpty())) {
            desAdapter.tripstatus(rModel.status)
            desAdapter.updateAll(rModel.routes)
            checkCompletedRouts(rModel?.routes)
            if (rModel.users.size == 1)
                tripStatusDestinationViewTxt.setText(getString(R.string.str_trip_status) /*+ " (" + rModel.users.size + " Rider)"*/)
            else {
                tripStatusDestinationViewTxt.setText(getString(R.string.str_trip_status)/* + " (" + rModel.users.size + " Riders)"*/)
            }
        }
        if (rModel.status.equals(RIDESTATUS.RIDESTATUS_AT_MEETING_POINT)) {
            tripCompletedBtn.setText(R.string.str_start_moving)
            tripCompletedBtn.setBackgroundResource(R.drawable.rectangle_background)
            tripCompletedBtn.isEnabled = true
        } else {
            tripCompletedBtn.setText(R.string.str_complete_trip)
        }
    }

    private fun checkCompletedRouts(routes: List<TourDetailResponseModel.ResponseObj.Route>) {
        var isAllPicked = true
        for (i in 0 until routes.size) {
            if (!routes.get(i).is_arrived) {
                tripCompletedBtn.setBackgroundResource(R.drawable.rectangle_background_grey)
                tripCompletedBtn.isEnabled = false
                isAllPicked = false

            }
        }
        if (isAllPicked) {
            tripCompletedBtn.setBackgroundResource(R.drawable.rectangle_background)
            tripCompletedBtn.isEnabled = true
        }

    }

    private fun setOnMovingData(nData: TourDetailResponseModel.ResponseObj) {

        if (nData != null) {
            tourCategory.setText(nData.type.replace("_", " "))
            tourDestinationsTxt.setText(nData.tour_name)
            tourPickupLocation.setText(nData.start_location.name)
            tourPickupTime.setText(
                DateUtilizer.getFormatedDate(
                    "HH:mm",
                    "hh:mm a",
                    nData.booking_date.split(",")[1]
                ) + "( " + nData.duration + " Min Tour)"
            )

            try {
                dateTextTour.setText(
                    DateUtilizer.getPreText(
                        "dd-MM-yyyy",
                        nData.booking_date
                    ) + DateUtilizer.getFormatedDate(
                        "dd-MM-yyyy",
                        "dd MMMM yyyy",
                        nData.booking_date.split(",")[0]
                    )
                )
            } catch (e: Exception) {
                dateTextTour.setText(
                    DateUtilizer.getPreText(
                        "dd-MM-YYYY",
                        nData.booking_date
                    ) + DateUtilizer.getFormatedDate(
                        "dd-MM-YYYY",
                        "dd MMMM YYYY",
                        nData.booking_date.split(",")[0]
                    )
                )
            }


            tourPickupDistance.setText("" + nData.distance + " KM")
            tourPeopleTxt.setText("" + nData.users.size)
            tourPickupLocation.isSelected = true
            if (nData.seats > 1) {
                tourSeats.setText("" + nData.seats + " " + getString(R.string.seats))
            } else
                tourSeats.setText("" + nData.seats + " " + getString(R.string.seat))
            tourPeopleTxt.setOnClickListener {
                AppDialogs.openDialogPeopleAlert(
                    activity!!,
                    nData,
                    Any(),
                    object : OnBottomDialogItemListener<Any> {
                        override fun onItemClick(
                            view: View,
                            position: Int,
                            type: Int,
                            t: Any
                        ) {
                            contactString =
                                (t as TourDetailResponseModel.ResponseObj.User).contact!!
                            InitCall((t as TourDetailResponseModel.ResponseObj.User).contact!!)
                        }
                    })
            }
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var vw: View = inflater.inflate(R.layout.fragment_tour_updates_layout, container, false)
        // Inflate the layout for this fragment
//        changeGravityToBottom(vw)
        return vw

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestLocationPermissions()
        startTripBtn.setOnClickListener {
            start_trip_holder.visibility = View.GONE
            trip_destination_holder.visibility = View.VISIBLE
        }

        tripCompletedBtn.setOnClickListener {
            if (!AppUtils.checkNetwork()) {
                AppUtils.showToast(getString(R.string.no_internet_connection))
                return@setOnClickListener
            }
            mCurrentLocation = AppUtils.getMyLocation()
            if (tripCompletedBtn.text.toString().equals(getString(R.string.str_start_moving))) {
                if (checkRiderPickedup()) {
                    presenter.startMovingRequest(
                        NetworkRequest.REQUEST_START_MOVING_REQUEST,
                        StartMovingRequestModel(
                            tourID,
                            mCurrentLocation!!.latitude,
                            mCurrentLocation!!.longitude
                        )
                    )
                } else {
                    AppUtils.showToast(getString(R.string.str_cannot_start_without_rider))
                }
            } else
                if (checkRiderPickedup()) {
                    AppDialogs.openDialogTwoButtons(
                        activity!!,
                        getString(R.string.mark_trip_complete),
                        getString(R.string.do_uou_mark_trip_complete),
                        object : OnBottomDialogItemListener<Any> {
                            override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                                when (type) {
                                    0 -> {

                                    }
                                    1 -> {
                                        if (mCurrentLocation != null) {
                                            presenter.completeTripRequest(
                                                NetworkRequest.REQUEST_MARK_COMPLETE_TRIP,
                                                UpdateRequestModel(
                                                    tourID,
                                                    mCurrentLocation!!.latitude,
                                                    mCurrentLocation!!.longitude
                                                )
                                            )
                                        }

                                    }
                                }
                            }

                        })
                } else {
                    AppUtils.showToast(getString(R.string.str_cannot_complete_without_rider))
                }


        }

        startMovingBtn.setOnClickListener {
            //            start_trip_holder.visibility = View.VISIBLE
//            trip_start_moving_holder.visibility = View.GONE

            mCurrentLocation = AppUtils.getMyLocation()
            if (tourID != null && mCurrentLocation !== null) {
                if (startMovingBtn.text.toString()
                        .equals(getString(R.string.str_start_moving_to_meeting_point))
                ) {
                    if (!isActiveBooking()) {
                        presenter.startMovingToMeetingPontRequest(
                            NetworkRequest.REQUEST_START_MOVING_TO_MEETING_REQUEST,
                            StartMovingRequestModel(
                                tourID,
                                mCurrentLocation!!.latitude,
                                mCurrentLocation!!.longitude
                            )
                        )
                    } else {
                        AppUtils.showToast(getString(R.string.str_complete_trip_first))
                    }

                } else if (startMovingBtn.text.toString()
                        .equals(getString(R.string.str_reached_meeting_point))
                ) {

                    presenter.reachedMeetingPontRequest(
                        NetworkRequest.REQUEST_REACHED_MEETING_REQUEST,
                        StartMovingRequestModel(
                            tourID,
                            mCurrentLocation!!.latitude,
                            mCurrentLocation!!.longitude
                        )
                    )
                } else {
                    presenter.startMovingRequest(
                        NetworkRequest.REQUEST_START_MOVING_REQUEST,
                        StartMovingRequestModel(
                            tourID,
                            mCurrentLocation!!.latitude,
                            mCurrentLocation!!.longitude
                        )
                    )
                }
            }


        }
        pickUpFloating.setOnClickListener {
            val bndl = Bundle()
            bndl.putString(CommonValues.TOUR_ID, tourID)
            bndl.putSerializable(CommonValues.TOUR_DATA, tourDataModel.response_obj)
            activitySwitcher(activity!!, QrCodeActivityForPickup::class.java, bndl)
        }
        btnScaneQrCodes.setPaintFlags(btnScaneQrCodes.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        btnScaneQrCodes.setOnClickListener {
            val bndl = Bundle()
            bndl.putString(CommonValues.TOUR_ID, tourID)
            bndl.putSerializable(CommonValues.TOUR_DATA, tourDataModel.response_obj)
            activitySwitcher(activity!!, QrCodeActivityForPickup::class.java, bndl)
        }

        supportBtn.setOnClickListener {
            if (!Prefs.getString(
                    CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID,
                    ""
                ).equals(other = "")
            ) {
                val bndl = Bundle()
                bndl.putString(CommonValues.TOUR_ID, tourID)
                bndl.putSerializable(CommonValues.TOUR_DATA, tourDataModel.response_obj)
                activitySwitcher(activity!!, TourSupportActivity::class.java, bndl)
            }
        }

        initDestination()
    }

    private fun checkRiderPickedup(): Boolean {
        if (tourDataModel != null) {
            if (tourDataModel.response_obj.users.isNotEmpty()) {
                var userList = tourDataModel.response_obj.users
                for (i in 0 until userList.size) {
                    if (userList.get(i).is_board) {
                        return true
                    }
                }
            }
        }

        return false
    }

    private fun initDestination() {
        with(destinationsRecyclar) {
            desAdapter =
                DestinationListAdapter(activity!!, ArrayList(), object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                        if (!Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "").equals(
                                other = ""
                            )
                        )
                            when (type) {
                                CommonValues.TOUR_DESTINATION_ARRIVED -> {
                                    mCurrentLocation = AppUtils.getMyLocation()
                                    if (mCurrentLocation != null) {
                                        presenter.updateArriveStatus(
                                            NetworkRequest.REQUEST_ARRIVED_LOCATION,
                                            UpdateRequestModel(
                                                tourID,
                                                mCurrentLocation!!.latitude,
                                                mCurrentLocation!!.longitude,
                                                (t as TourDetailResponseModel.ResponseObj.Route)._id
                                            )
                                        )
                                    }

                                }
                                CommonValues.TOUR_PICKUP_RIDER_CLICK -> {
                                    var positionData =
                                        (t as TourDetailResponseModel.ResponseObj.Route)

                                    EventBus.getDefault().postSticky(
                                        CustomEvent<Any>(
                                            EVENTS.REQUEST_TOUR_PICKUP_RIDERS_VIEW,
                                            positionData
                                        )
                                    )
                                }
                            }
                        else {
                            AppUtils.showToast("You are offline")
                        }

                    }

                })
            adapter = desAdapter
            layoutManager = LinearLayoutManager(activity, VERTICAL, false)
        }
    }

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }


    override fun onResponse(list: Any, int: Int) {
        Log.d("onResponse", "  " + Gson().toJson(list))

        when (int) {
            NetworkRequest.REQUEST_START_MOVING_TO_MEETING_REQUEST,
            NetworkRequest.REQUEST_REACHED_MEETING_REQUEST,
            NetworkRequest.REQUEST_START_MOVING_REQUEST -> {
                var responseData = list as BaseResponseModel
//                if (responseData.re == NetworkCodes.SUCCEES.nCodes)
                getTourDetail()
            }
            NetworkRequest.REQUEST_TOUR_DETAIL -> {
                var rModel = list as TourDetailResponseModel
                tourDataModel = rModel
                if (rModel.response_code == NetworkCodes.SUCCEES.nCodes) {
/*
                    EventBus.getDefault().postSticky(
                        CustomEvent<Any>(
                            EVENTS.REQUEST_TOUR_DESTINATIONS_VIEW,
                            rModel.response_obj
                        )
                    )*/

                    when (rModel.response_obj.status) {
                        RIDESTATUS.RIDESTATUS_ACCEPTED -> {
                            EventBus.getDefault().postSticky(
                                CustomEvent<Any>(
                                    EVENTS.REQUEST_START_MOVING_TO_MEETING_POINT_VIEW,
                                    rModel.response_obj
                                )
                            )
                        }
                        RIDESTATUS.RIDESTATUS_STARTED -> {
                            EventBus.getDefault().postSticky(
                                CustomEvent<Any>(
                                    EVENTS.REQUEST_REACHED_MEETING_POINT_VIEW,
                                    rModel.response_obj
                                )
                            )
                        }
                        RIDESTATUS.RIDESTATUS_AT_MEETING_POINT -> {
                            /* EventBus.getDefault().postSticky(
                                 CustomEvent<Any>(
                                     EVENTS.REQUEST_START_MOVING_VIEW,
                                     rModel.response_obj
                                 )
                             )*/
                            Prefs.putString(CommonValues.CURRENT_BOOKING_ID, rModel.response_obj.id)
                            EventBus.getDefault().postSticky(
                                CustomEvent<Any>(
                                    EVENTS.REQUEST_TOUR_DESTINATIONS_VIEW,
                                    rModel.response_obj
                                )
                            )
                        }
                        RIDESTATUS.RIDESTATUS_IN_PROGRESS -> {
                            Prefs.putString(CommonValues.CURRENT_BOOKING_ID, rModel.response_obj.id)
                            EventBus.getDefault().postSticky(
                                CustomEvent<Any>(
                                    EVENTS.REQUEST_TOUR_DESTINATIONS_VIEW,
                                    rModel.response_obj
                                )
                            )
                        }
                    }
                } else {
                    AppUtils.showToast(rModel.response_message)
                }
            }
            NetworkRequest.REQUEST_ARRIVED_LOCATION -> {
                var rModel = list as BaseResponseModel
                if (rModel.response_code == NetworkCodes.SUCCEES.nCodes) {
                    getTourDetail()
                }
            }
            NetworkRequest.REQUEST_MARK_COMPLETE_TRIP -> {
                var rModel = list as BaseResponseModel
                if (rModel.response_code == NetworkCodes.SUCCEES.nCodes) {
                    Prefs.putString(CommonValues.RECENT_SUBSCRIBER_ID, "")
                    HomeActivity.homeInstance.backToHomeView()
                    HomeActivity.homeInstance.resetActiveTour()
                    HomeActivity.homeInstance.refreshMapPadding()
                    AppUtils.showToast(getString(R.string.str_completed_successfully))
//                    var bndl = Bundle()
//                    bndl.putString(CommonValues.TOUR_ID, tourID)
//                    bndl.putSerializable(CommonValues.TOUR_DATA, tourDataModel.response_obj)
//                    activitySwitcher(activity!!, RateTripActivity::class.java, bndl)
                } else {
                    AppUtils.showToast(rModel.response_message)
                }
            }


        }
    }

    private fun getTourDetail() {
        val updateBookingRequestModel = UpdateBookingRequestModel(tourID)
        presenter.getTourDeatail(
            NetworkRequest.REQUEST_TOUR_DETAIL,
            updateBookingRequestModel
        )
    }

    override fun showProgress() {
        try {
            loaderView.show()
            tripUpdateLoaderView.visibility=View.VISIBLE
            startMovingBtn.visibility=View.GONE
        } catch (e: Exception) {
        }
    }

    override fun hideProgress() {
        try {
            loaderView.hide()
            tripUpdateLoaderView.visibility=View.GONE
            startMovingBtn.visibility=View.VISIBLE
        } catch (e: Exception) {
        }
    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun setPresenter(presenter: BasePresenter<*>) {

    }

    private var mCurrentLocation: LatLng? = null
    override fun whereIAM(loc: ADLocation?) {
        if (loc != null) {
            try {
                mCurrentLocation = LatLng(loc.lat, loc.longi)
                Prefs.putDouble(CommonValues.LATITUDE, loc.lat)
                Prefs.putDouble(CommonValues.LONGITUDE, loc.longi)
                Log.e(
                    TAG, "whereIAM " + loc.address
                )

            } catch (e: Exception) {
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun requestLocationPermissions() {
        if (Build.VERSION.SDK_INT < 23) {
            //Do not need to check the permission
            goNext()
        } else {
            if (checkAndRequestLocationPermissions()) {
                //If you have already permitted the permission
                goNext()
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun goNext() {
        MyTracker(activity, this).track()
    }
}