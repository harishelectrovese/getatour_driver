package code_setup.ui_.home.views.qr_fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import code_setup.app_core.CoreFragment
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.ChangeDriverStatusRequest
import code_setup.app_models.response_.UpdateStatusResponseModel
import code_setup.app_util.AppDialogs
import code_setup.app_util.AppUtils
import code_setup.app_util.CommonValues
import code_setup.app_util.Prefs
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.socket_work.SocketService
import code_setup.net_.NetworkCodes
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.HomeActivity
import com.base.mvp.BasePresenter
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.DecodeCallback
import com.budiyev.android.codescanner.ErrorCallback
import com.budiyev.android.codescanner.ScanMode
import com.electrovese.setup.R
import com.google.gson.Gson
import com.google.zxing.Result
import com.tbruyelle.rxpermissions3.RxPermissions
import kotlinx.android.synthetic.main.qrcode_fragment_layout.*
import kotlinx.android.synthetic.main.trans_toolbar_lay.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class QrCodeFragment : CoreFragment()/*, ZXingScannerView.ResultHandler*/, HomeView {
    /*   override fun handleResult(p0: Result?) {
           Log.d("handleResult", "  " + p0.toString())
   //        AppUtils.showToast(p0.toString())
           proceed(p0.toString())

       }*/

    private fun proceed(toString: String) {
        Prefs.putString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, toString)
        presenter.changeDriverStatus(
            code_setup.net_.NetworkRequest.REQUEST_CHANGE_STATUS,
            ChangeDriverStatusRequest(toString)
        )

    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " " + event.type)
        when (event.type) {
            EVENTS.PERMISSION_GRANTED -> {
//                requestPermission()
                goScan()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.qrcode_fragment_layout, container, false)
    }

    //    lateinit var mScannerView: ZXingScannerView
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity!!.getWindow().setFormat(PixelFormat.TRANSLUCENT);
//        mScannerView = ZXingScannerView(activity);
//        qrCodeHolder.addView(mScannerView)
        HomeActivity.homeInstance.txtToolbartitle.setText(R.string.str_scan_qr_code)
        HomeActivity.homeInstance.txtToolbartitle.setTextColor(
            activity!!.resources.getColor(
                R.color.colorPrimary
            )
        )

        initScanner()

    }


    private fun requestPermission() {
        val rxPermissions = RxPermissions(this)
        rxPermissions.requestEach(Manifest.permission.CAMERA)
            .subscribe { permission ->  // will emit 2 Permission objects
                if (permission.granted) {
                    // `permission.name` is granted !
                    goScan()
                } else if (permission.shouldShowRequestPermissionRationale) {
                    // Denied permission without ask never again
//                    requestPermission()
                    AppUtils.showToast(getString(R.string.str_camera_permission_alert_message))
                    HomeActivity.homeInstance.backToHomeView()
                } else {
                    // Denied permission with ask never again
                    // Need to go to the settings
                    if (!permissionPopUpVisible) {
                        permissionPopUpVisible = true
                        AppDialogs.openCameraPermissionAlert(
                            activity!!,
                            Any(),
                            Any(),
                            object : OnBottomDialogItemListener<Any> {
                                override fun onItemClick(
                                    view: View,
                                    position: Int,
                                    type: Int,
                                    t: Any
                                ) {
                                    //                         TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                                permissionPopUpVisible == false
                                }

                            })
                    }
                }
            }

    }

    private var permissionPopUpVisible: Boolean = false
    lateinit var codeScanner: CodeScanner
    private fun initScanner() {
        codeScanner = CodeScanner(activity!!, scanner_view)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            Handler(Looper.getMainLooper()).post(
                Runnable {
//                    Toast.makeText(activity!!, "Scan result: ${it.text}", Toast.LENGTH_LONG).show()
//                    AppUtils.showToast(it.text.toString())
                    proceed(it.text.toString())
                })
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            Handler(Looper.getMainLooper()).post(
                Runnable {
                    Toast.makeText(
                        activity!!, "Camera initialization error: ${it.message}",
                        Toast.LENGTH_LONG
                    ).show()
                })
        }

        scanner_view.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    private fun goScan() {
//        mScannerView.setResultHandler(this)
//        mScannerView.startCamera()
        codeScanner.startPreview()
    }

    override fun onResume() {
        super.onResume()

        requestPermission()

    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
//        mScannerView.stopCamera()
    }

    /* fun checkAndRequestPermissions(): Boolean {
         var camPermission: Int =
             ActivityCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA)
         var readstoragePermission: Int =
             ActivityCompat.checkSelfPermission(
                 activity!!,
                 Manifest.permission.READ_EXTERNAL_STORAGE
             )
         var writestoragePermission: Int =
             ActivityCompat.checkSelfPermission(
                 activity!!,
                 Manifest.permission.WRITE_EXTERNAL_STORAGE
             )
         val listPermissionsNeeded = java.util.ArrayList<String>()
         if (readstoragePermission != PackageManager.PERMISSION_GRANTED) {
             listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE)
         }
         if (writestoragePermission != PackageManager.PERMISSION_GRANTED) {
             listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
         }
         if (camPermission != PackageManager.PERMISSION_GRANTED) {
             listPermissionsNeeded.add(Manifest.permission.CAMERA)

         }
         if (!listPermissionsNeeded.isEmpty()) {
             ActivityCompat.requestPermissions(
                 activity!!,
                 listPermissionsNeeded.toArray(arrayOfNulls(listPermissionsNeeded.size)),
                 CommonValues.REQUEST_CODE_PERMISSIONS_CAMERA
             )
             return false
         }
         return true
     }*/

    /*  override fun onRequestPermissionsResult(
          requestCode: Int,
          permissions: Array<out String>,
          grantResults: IntArray
      ) {
          when (requestCode) {
              CommonValues.REQUEST_CODE_PERMISSIONS_CAMERA -> {

                  Log.e(
                      "onRequestPermissionsResult :: ",
                      "User permissions granted" + "  " + Gson().toJson(grantResults)
                  )

                  if (grantResults.get(0) == 0)
                      goScan()
                  else AppUtils.showSnackBar(
                      activity!!,
                      getString(R.string.str_camers_permission_required)
                  )
              }
          }
      }*/

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onResponse(list: Any, int: Int) {
        when (int) {
            code_setup.net_.NetworkRequest.REQUEST_CHANGE_STATUS -> {

                var responseData = list as UpdateStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    AppDialogs.openDialogScaneSuccesssAlert(
                        activity!!,
                        responseData,
                        Any(),
                        object : OnBottomDialogItemListener<Any> {
                            override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                                when (type) {
                                    0 -> {
                                        HomeActivity.homeInstance.statusTextUpdate(false)
                                    }
                                    1 -> {
                                        HomeActivity.homeInstance.statusTextUpdate(true)
                                        activity!!.startService(
                                            Intent(
                                                activity,
                                                SocketService::class.java
                                            )
                                        )
                                    }
                                }
                                HomeActivity.homeInstance.backToHomeView()
                            }
                        })
                } else {
                    AppUtils.showSnackBar(activity!!, responseData.response_message)
                    goScan()
                }
            }
        }
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun noResult() {
    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {

    }

}