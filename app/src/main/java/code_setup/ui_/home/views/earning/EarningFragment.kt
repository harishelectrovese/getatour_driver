package code_setup.ui_.home.views.earning

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreFragment
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppUtils
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.GraphAdapter
import code_setup.ui_.home.apapter_.GraphYaxisAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.EarningData
import code_setup.ui_.home.models.EarningResponseModel
import code_setup.ui_.home.views.HomeActivity
import code_setup.ui_.settings.views.ridehistory.RejectedRidesActivity
import com.base.mvp.BasePresenter
import com.electrovese.setup.R
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.google.gson.Gson
import kotlinx.android.synthetic.main.graph_background_frame_layout.*
import kotlinx.android.synthetic.main.layout_earning_fragment.*
import kotlinx.android.synthetic.main.trans_toolbar_lay.*
import java.util.*
import javax.inject.Inject


class EarningFragment : CoreFragment(), HomeView {
    private var stepSize: Int = 100
    lateinit var graphAdater: GraphAdapter
    lateinit var graphYAdater: GraphYaxisAdapter
    private var earningData: EarningResponseModel.ResponseObj? = null

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_earning_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        HomeActivity.homeInstance.txtToolbartitle.setText(R.string.str_Earning)
        HomeActivity.homeInstance.isOtherFragmentOpened(true)
        onActivityInject()
        initRecyclar()
//        updateGraphView()
        getEarningData()


        weekBtn.setOnClickListener {
            weekBtn.setBackgroundResource(R.drawable.drawable_circle)
            weekBtn.setTextColor(resources.getColor(R.color.colorWhite))

            todayBtn.setBackgroundResource(R.color.colorWhite)
            todayBtn.setTextColor(resources.getColor(R.color.colorBlack))

            monthBtn.setBackgroundResource(R.color.colorWhite)
            monthBtn.setTextColor(resources.getColor(R.color.colorBlack))
            setWeeklyData()
            txtLable.setText(R.string.str_this_week)
        }
        todayBtn.setOnClickListener {
            todayBtn.setBackgroundResource(R.drawable.drawable_circle)
            todayBtn.setTextColor(resources.getColor(R.color.colorWhite))

            weekBtn.setBackgroundResource(R.color.colorWhite)
            weekBtn.setTextColor(resources.getColor(R.color.colorBlack))

            monthBtn.setBackgroundResource(R.color.colorWhite)
            monthBtn.setTextColor(resources.getColor(R.color.colorBlack))
            setTodaysData()
            txtLable.setText(R.string.str_today)
        }
        monthBtn.setOnClickListener {
            monthBtn.setBackgroundResource(R.drawable.drawable_circle)
            monthBtn.setTextColor(resources.getColor(R.color.colorWhite))

            todayBtn.setBackgroundResource(R.color.colorWhite)
            todayBtn.setTextColor(resources.getColor(R.color.colorBlack))

            weekBtn.setBackgroundResource(R.color.colorWhite)
            weekBtn.setTextColor(resources.getColor(R.color.colorBlack))
            setMonthData()
            txtLable.setText(R.string.str_this_month)
        }


        rejectedTripsBtn.setOnClickListener {

            activity?.let { it1 -> activitySwitcher(it1, RejectedRidesActivity::class.java, null) }
        }
        /*Handler().postDelayed(Runnable {
            initBarTable()
        },2000)*/
    }

    // variable for our bar chart
    var barChart: BarChart? = null
    // variable for our bar data.
    var barData: BarData? = null
    // variable for our bar data set.
    var barDataSet: BarDataSet? = null
    // array list for storing entries.
    var barEntriesArrayList= ArrayList<BarEntry>()

    private fun initBarTable() {
        // calling method to get bar entries.
        // calling method to get bar entries.
        barChart = (activity)!!.findViewById(R.id.idBarChart);
        getBarEntries()

        // creating a new bar data set.

        // creating a new bar data set.
        barDataSet = BarDataSet(barEntriesArrayList, "Geeks for Geeks")

        // creating a new bar data and
        // passing our bar data set.

        // creating a new bar data and
        // passing our bar data set.
        barData = BarData(barDataSet)

        // below line is to set data
        // to our bar chart.

        // below line is to set data
        // to our bar chart.
        barChart!!.setData(barData)
        // adding color to our bar data set.

        // adding color to our bar data set.
//       barDataSet!!.setColors(ColorTemplate.MATERIAL_COLORS)

        // setting text color.

        // setting text color.
        barDataSet!!.setValueTextColor(Color.BLACK)

        // setting text size

        // setting text size
        barDataSet!!.setValueTextSize(16f)
        barChart!!.getDescription().setEnabled(false)
    }
    private fun getBarEntries() {
        // creating a new array list
        barEntriesArrayList = ArrayList<BarEntry>()

        // adding new entry to our array list with bar
        // entry and passing x and y axis value to it.
        barEntriesArrayList!!.add(BarEntry(1f, 44f))
        barEntriesArrayList!!.add(BarEntry(2f, 65f))
        barEntriesArrayList!!.add(BarEntry(3f, 8f))
        barEntriesArrayList!!.add(BarEntry(4f, 20f))
        barEntriesArrayList!!.add(BarEntry(5f, 40f))
        barEntriesArrayList!!.add(BarEntry(6f, 10f))
    }
    private fun setMonthData() {
        txtLable.setText(R.string.str_this_month)
        if (earningData != null) {
            graphAdater.updateAll(earningData!!.monthly_earning)
            graphAdater.setViewTo(2)//0 for week, 1 for today,2 for month

            if (earningData != null) {
                bookingTxt.setText("" + getBookings(earningData!!.monthly_earning))

                earningTxt.setText(
                    earningData!!.today_earning.currency_symbol + "" + earningData!!.monthly_total
                )
            }
            bookingLableTxt.setText(R.string.str_monthly_booking_txt)
            earningLableTxt.setText(R.string.str_monthly_earning_txt)
            setYaxais(getEarnings(earningData!!.monthly_earning) as Int)
        }
    }

    private fun initRecyclar() {
        with(graphRecyclar) {
            graphAdater =
                GraphAdapter(activity!!, ArrayList(), object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                    }
                })
            adapter = graphAdater
            layoutManager = LinearLayoutManager(activity, HORIZONTAL, false)
        }

        with(yaxisRecyclarvw) {
            graphYAdater =
                GraphYaxisAdapter(
                    activity!!,
                    AppUtils.getDefaultyAxis(),
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                        }
                    })
            layoutManager = LinearLayoutManager(activity, VERTICAL, false)
            adapter = graphYAdater

        }
    }

    private fun setTodaysData() {
        txtLable.setText(R.string.str_today)
        if (earningData != null) {
            bookingTxt.setText("" + earningData!!.today_earning.bookings)
            earningTxt.setText(earningData!!.today_earning.currency_symbol + "" + earningData!!.today_earning.total)
            var todaysArray = ArrayList<EarningResponseModel.ResponseObj.Earning>()
            todaysArray.add(
                EarningResponseModel.ResponseObj.Earning(
                    earningData!!.today_earning.bookings,
                    earningData!!.today_earning.currency_symbol,
                    earningData!!.today_earning.date,
                    earningData!!.today_earning.total
                )
            )
            graphAdater.updateAll(todaysArray)
            graphAdater.setViewTo(0)//0 for week, 1 for today,2 for month

            bookingLableTxt.setText(R.string.str_today_booking_txt)
            earningLableTxt.setText(R.string.str_today_earning_txt)
        }
        updateGraphView()
        if (earningData != null)
            setYaxais(earningData!!.today_earning.total)
    }

    private fun getEarningData() {
        if (!AppUtils.checkNetwork()) {
            AppUtils.showToast(getString(R.string.no_internet_connection))
            return
        }
        presenter.getEarningData(NetworkRequest.REQUEST_EARNING_DATA)
    }


    private fun updateGraphView() {
        AnimUtils.resizeView(textView, 1000, 0, 0)
        AnimUtils.resizeView(textView1, 1000, 0, 0)
        AnimUtils.resizeView(textView2, 1000, 0, 0)
        AnimUtils.resizeView(textView3, 1000, 0, 0)
        AnimUtils.resizeView(textView4, 1000, 0, 0)
        AnimUtils.resizeView(textView5, 1000, 0, 0)
        AnimUtils.resizeView(textView6, 1000, 0, 0)
    }

    override fun onResponse(list: Any, int: Int) {
        Log.d("onResponse", "  " + Gson().toJson(list))
        when (int) {
            NetworkRequest.REQUEST_EARNING_DATA -> {
                var responseData = list as EarningResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    if (responseData.response_obj != null) {
                        earningData = responseData.response_obj
                        totalEarningTxt.setText(earningData!!.today_earning.currency_symbol + "" + responseData.response_obj.monthly_total)
//                        setWeeklyData()
                        setTodaysData()
                    }
                }

            }
        }
    }

    private fun setWeeklyData() {
        txtLable.setText(R.string.str_this_week)
        if (earningData != null) {
            bookingTxt.setText("" + getBookings(earningData!!.weekly_earning))

        }

        if (earningData != null) {
            earningTxt.setText(
                earningData!!.today_earning.currency_symbol + "" + earningData!!.weekly_total
            )
            graphAdater.updateAll(earningData!!.weekly_earning)
            graphAdater.setViewTo(1)//0 for week, 1 for today,2 for month
        }
        bookingLableTxt.setText(R.string.str_weekly_booking_txt)
        earningLableTxt.setText(R.string.str_weekly_earning_txt)
        if (earningData != null)
            setYaxais(getEarnings(earningData!!.weekly_earning) as Int)


    }

    private fun setYaxais(earnings: Int) {
        try {
            Log.e("setYaxais", "1  " + earnings)
            Log.e("setYaxais", "2  " + getStepSize(earnings))
            if (earnings > 700) {
                stepSize = getStepSize(earnings)
                graphYAdater.updateAll(
                    getList(
                        AppUtils.getArrayList(
                            0,
                            earnings,
                            getStepSize(earnings)
                        )
                    )
                )
                graphAdater.updateStep(stepSize)
            } else {
                stepSize = 100
                graphYAdater.updateAll(AppUtils.getDefaultyAxis())
                graphAdater.updateStep(stepSize)
            }
        } catch (e: Exception) {
            stepSize = 100
            graphYAdater.updateAll(AppUtils.getDefaultyAxis())
            graphAdater.updateStep(stepSize)
        }

        try {
            graphScroll.post(Runnable { graphScroll.fullScroll(ScrollView.FOCUS_DOWN) })
        } catch (e: Exception) {
        }
    }

    private fun getList(arrayList: ArrayList<String>): List<EarningData> {
        var newList = ArrayList<EarningData>()
        for (i in 0 until arrayList.size) {
            newList.add(EarningData(arrayList.get(i), ""))
        }
        return newList.reversed()
    }

    private fun getStepSize(earnings: Int): Int {
//        Log.e("getStepSize", "  " + earnings.toString().length)
//        if (earnings.toString().length < 5)
        try {
            if (earnings.toString().length == 4)
                if (earnings <= 7000)
                    return 1000
                else
                    return 2000
            if (earnings.toString().length == 5)
                if (earnings <= 50000)
                    return 5000
                else
                    return 10000
            if (earnings.toString().length == 6)
                if (earnings <= 500000)
                    return 50000
                else
                    return 100000
        } catch (e: Exception) {
        }
        return 100
    }

    private fun getEarnings(weeklyEarning: List<EarningResponseModel.ResponseObj.Earning>): Any? {
        var totalEarningss: Int = 0
        var earingValues = ArrayList<Int>()
        try {
            totalEarningss = 0
            for (i in 0 until weeklyEarning.size) {
                totalEarningss += weeklyEarning.get(i).total.toInt()
                earingValues.add(weeklyEarning.get(i).total)
            }
        } catch (e: Exception) {
            return 0
        }
        Log.e("earning amount", "  " + Collections.min(earingValues))
        Log.e("earning amount", "  " + Collections.max(earingValues))

        return Collections.max(earingValues)
    }

    private fun getBookings(weeklyEarning: List<EarningResponseModel.ResponseObj.Earning>): Any {
        var totalBookngs: Int = 0
        for (i in 0 until weeklyEarning.size) {

            totalBookngs += weeklyEarning.get(i).bookings.toInt()

/*



            try {
                day1TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(0).date
                    )
                )
                day2TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(1).date
                    )
                )
                day3TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(2).date
                    )
                )
                day4TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(3).date
                    )
                )
                day5TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(4).date
                    )
                )
                day6TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(5).date
                    )
                )
                day7TextView.setText(
                    DateUtilizer.getFormatedDate(
                        "yyyy-MM-dd",
                        "EEE",
                        weeklyEarning.get(6).date
                    )
                )
            } catch (e: Exception) {
            }*/

        }
        return totalBookngs
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun setPresenter(presenter: BasePresenter<*>) {

    }
}