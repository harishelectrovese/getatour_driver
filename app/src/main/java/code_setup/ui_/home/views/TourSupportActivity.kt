package code_setup.ui_.home.views

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.CheckBox
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreActivity
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_models.response_.TourDetailResponseModel
import code_setup.app_util.AppDialogs
import code_setup.app_util.AppUtils
import code_setup.app_util.CommonValues
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.TourMembersSupportAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.RequestCancelRide
import code_setup.ui_.home.models.RequestRideSupport
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.default_toolbar_lay.*
import kotlinx.android.synthetic.main.layout_support_activity.*
import kotlinx.android.synthetic.main.layout_tour_support_activity.*
import javax.inject.Inject


class TourSupportActivity : CoreActivity(), HomeView {
    private var selectedUser: TourDetailResponseModel.ResponseObj.User? = null
    lateinit var tourId: String
    lateinit var memberAdapter: TourMembersSupportAdapter

    var checkBoxList =
        arrayOf(R.id.checkBox_1, R.id.checkBox_2, R.id.checkBox_3, R.id.checkBox_4, R.id.checkBox_5)

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_tour_support_activity
    }

    override fun onResponse(list: Any, int: Int) {
        Log.d("onResponse", "  " + Gson().toJson(list))

        when (int) {
            NetworkRequest.REQUEST_RIDE_SUPPORT -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showToast("Request submitted successfully")
                    onBackPressed()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }
            }
            NetworkRequest.REQUEST_CANCEL_RIDE -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showToast("Request submitted successfully")
                    activitySwitcher(this,HomeActivity::class.java,null)
                    finishAffinity()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }

            }
        }
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        txt_title_toolbar.setText(R.string.str_contact_support)
        toolbarBack.setOnClickListener {
            onBackPressed()
        }
        initAdapter()
        getData(intent)
        iniChcekBoxSelction()
        saveBtnSupport.setOnClickListener {
            if (validated()) {
                presenter.requestRideSupport(
                    NetworkRequest.REQUEST_RIDE_SUPPORT,
                    RequestRideSupport(tourId, getSelectedReason())
                )
            }
        }
        cancelTripBtn.setPaintFlags(cancelTripBtn.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        cancelTripBtn.setSafeOnClickListener {
            AppDialogs.openCancelTrip(this, Any(), Any(), object : OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    when (type) {
                        1 -> {

                            presenter.requestCancelRide(
                                NetworkRequest.REQUEST_CANCEL_RIDE,
                                RequestCancelRide(
                                    tourId,
                                    AppUtils.getMyLocation()!!.latitude.toString(),
                                    AppUtils.getMyLocation()!!.longitude.toString(),
                                    t as String
                                )
                            )
                        }
                    }
                }

            })
        }
        initTextChangeListner()
    }

    private fun initTextChangeListner() {
        fieldOther.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                messageLengthText1.setText("" + fieldOther.text.length + "/" + "200")
                if (fieldOther.text.length == 200) {
                    messageLengthText1.setTextColor(resources.getColor(R.color.colorRed))
                } else
                    messageLengthText1.setTextColor(resources.getColor(R.color.colorTextGrey1))
            }
        })
    }

    private fun getSelectedReason(): String {
        if (checkBox_1.isChecked) {
            return checkBox_1.text.toString()
        } else if (checkBox_2.isChecked) {
            return selectedUser!!.name + " " + checkBox_2.text.toString()
        } else if (checkBox_3.isChecked) {
            return checkBox_3.text.toString()
        } else if (checkBox_4.isChecked) {
            return checkBox_4.text.toString()
        } else if (checkBox_5.isChecked) {
            return fieldOther.text.toString()
        }
        return ""
    }

    private fun validated(): Boolean {
        if (!checkBox_1.isChecked &&
            !checkBox_2.isChecked &&
            !checkBox_3.isChecked &&
            !checkBox_4.isChecked &&
            !checkBox_5.isChecked) {
            AppUtils.showToast(getString(R.string.str_select_a_option))
            return false
        } else if (checkBox_2.isChecked && selectedUser == null) {
            AppUtils.showToast(getString(R.string.select_rider))
            return false
        }
        return true
    }

    private fun iniChcekBoxSelction() {
        checkBox_1.setOnClickListener {
            updateSelection(checkBox_1)
        }
        checkBox_2.setOnClickListener {
            updateSelection(checkBox_2)

        }
        checkBox_3.setOnClickListener { updateSelection(checkBox_3) }
        checkBox_4.setOnClickListener { updateSelection(checkBox_4) }
        checkBox_5.setOnClickListener { updateSelection(checkBox_5) }
    }

    private fun updateSelection(checkbox: View) {
        for (i in 0 until checkBoxList.size) {
            if (checkbox.id == checkBoxList.get(i)) {
                (checkbox as CheckBox).isChecked = true
                if (i == 1) {
                    riderRecyclarSupport.visibility = View.VISIBLE
                } else {
                    riderRecyclarSupport.visibility = View.GONE
                }
                if (i == 4) {
                    fieldOther.visibility = View.VISIBLE
                    messageLengthText1.visibility = View.VISIBLE
                } else {
                    fieldOther.visibility = View.GONE
                    messageLengthText1.visibility = View.GONE
                }
            } else {
                (holderView.findViewById(checkBoxList.get(i)) as CheckBox).isChecked = false
            }
        }
    }

    private fun getData(intent: Intent?) {
        tourId = intent!!.getStringExtra(CommonValues.TOUR_ID)!!
        var tourData = intent!!.getSerializableExtra(CommonValues.TOUR_DATA)
        var toudDataModel = tourData as TourDetailResponseModel.ResponseObj

        if (toudDataModel != null) {

            memberAdapter.updateAll(toudDataModel.users)
        }
    }

    private fun initAdapter() {
        with(riderRecyclarSupport) {

            memberAdapter = TourMembersSupportAdapter(
                this@TourSupportActivity,
                ArrayList(),
                object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                        selectedUser = t as TourDetailResponseModel.ResponseObj.User

                    }
                })

            adapter = memberAdapter
            layoutManager = LinearLayoutManager(this@TourSupportActivity, VERTICAL, false)
        }
    }
}