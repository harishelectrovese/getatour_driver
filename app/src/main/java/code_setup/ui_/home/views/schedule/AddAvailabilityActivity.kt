package code_setup.ui_.home.views.schedule

import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.View
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.RequestCalenderBookingModel
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_util.*
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.RequestToursModel
import code_setup.ui_.home.models.ToursListResponseModel
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.add_availability_task_activitty.*
import kotlinx.android.synthetic.main.month_navigation_lay.*
import org.greenrobot.eventbus.EventBus
import java.text.SimpleDateFormat
import java.util.*
import java.util.Calendar.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.collections.ArrayList


class AddAvailabilityActivity : CoreActivity(), HomeView {
    private var selectedTourData: ToursListResponseModel.ResponseObj? = null
    private var tourList = ArrayList<ToursListResponseModel.ResponseObj>()
    val TAG: String = AddAvailabilityActivity::class.java.simpleName
    lateinit var startTimeString: String
    lateinit var endTimeString: String

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun getScreenUi(): Int {
        return R.layout.add_availability_task_activitty
    }

    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))
        when (int) {
            NetworkRequest.REQUEST_GET_MONYTHLY_TOURS -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showSnackBar(this, getString(R.string.str_booking_added_successfully))
                    startTimeTxt.setText("")
                    endTimeTxt.setText("")
                    startTimeString = ""
                    endTimeString = ""
                }
            }
            NetworkRequest.REQUEST_TOURS -> {
                var responseData = list as ToursListResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    if (responseData.response_obj != null && responseData.response_obj.isNotEmpty()) {
                        tourList.addAll(responseData.response_obj)
                        tourList.add(
                            (tourList.size),
                            ToursListResponseModel.ResponseObj("", "", "Other")
                        )
                    }
                }
            }
            NetworkRequest.REQUEST_SCHEDULE_CALENDAR_BOOKING -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showToast(getString(R.string.str_added_successfully))
                    EventBus.getDefault()
                        .postSticky(CustomEvent<Any>(EVENTS.SCHEDULE_UPDATED, true))
                    onBackPressed()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }
            }
        }
    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        closeBtn.setOnClickListener {
            onBackPressed()
        }
        dateText.setText(intent.getStringExtra(CommonValues.DAY_DETAIL))

        startTimeTxt.setOnClickListener {
            startTimeTxt.setText("")
            val cal = getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(HOUR_OF_DAY, hour)
                cal.set(MINUTE, minute)
                startTimeTxt.text = SimpleDateFormat("hh:mm a", Locale.US).format(cal.time)

                startTimeString = DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "MM-dd-yyyy",
                    intent.getStringExtra(CommonValues.DAY_DETAIL)!!
                ) + " " + SimpleDateFormat("HH:mm", Locale.US).format(cal.time)

            }
            TimePickerDialog(
                this,
                timeSetListener,
                cal.get(HOUR_OF_DAY),
                cal.get(MINUTE),
                false
            ).show()

        }
        endTimeTxt.setOnClickListener {
            endTimeTxt.setText("")
            val cal = getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(HOUR_OF_DAY, hour)
                cal.set(MINUTE, minute)
                endTimeTxt.text = SimpleDateFormat("hh:mm a", Locale.US).format(cal.time)


                endTimeString = DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "MM-dd-yyyy",
                    intent.getStringExtra(CommonValues.DAY_DETAIL)!!
                ) + " " + SimpleDateFormat("HH:mm", Locale.US).format(cal.time)
            }
            TimePickerDialog(
                this,
                timeSetListener,
                cal.get(HOUR_OF_DAY),
                cal.get(MINUTE),
                false
            ).show()

        }

        saveAvailabilityBtn.setOnClickListener {
            if (validated()) {
                presenter.scheduleCustomBooking(
                    NetworkRequest.REQUEST_SCHEDULE_CALENDAR_BOOKING,
                    RequestCalenderBookingModel(
                        endTimeString,
                        startTimeString,
                        selectedTourData!!.id
                    )
                )
            }
        }
        selectTourTxt.setSafeOnClickListener {
            AppDialogs.openDialogTourList(this@AddAvailabilityActivity, tourList, object :
                OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    try {
                        selectedTourData = t as ToursListResponseModel.ResponseObj
                        selectTourTxt.setText(selectedTourData!!.name)
                        selectTourTxt.setTextColor(resources.getColor(R.color.colorBlack))
                    } catch (e: Exception) {
                    }
                }
            })
        }
        switchListner()
        getTours()
    }

    private fun getTours() {
        presenter.getAllTours(
            NetworkRequest.REQUEST_TOURS,
            RequestToursModel(Prefs.getString(CommonValues.AVAIALBLE_SCANNED_VEHICLE_ID, "")!!)
        )
    }

    private fun switchListner() {
        allDaySwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                startTimeString = DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "MM-dd-yyyy",
                    intent.getStringExtra(CommonValues.DAY_DETAIL)!!
                ) + " " + "00:00"
                endTimeString = DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "MM-dd-yyyy",
                    intent.getStringExtra(CommonValues.DAY_DETAIL)!!
                ) + " " + "23:59"

                startTimeTxt.text = gettStartTime()
                endTimeTxt.text = "11:59 pm"
                startTimeTxt.isEnabled = false
                endTimeTxt.isEnabled = false
            } else {
                startTimeString = ""
                endTimeString = ""
                startTimeTxt.text = startTimeString
                endTimeTxt.text = endTimeString
                startTimeTxt.isEnabled = true
                endTimeTxt.isEnabled = true
            }
        }
    }

    private fun gettStartTime(): String {

        if (intent.getStringExtra(CommonValues.DAY_DETAIL)
                .equals(DateUtilizer.getCurrentDate("dd MMMM, yyyy"))
        ) {
            startTimeString = DateUtilizer.getFormatedDate(
                "dd MMMM, yyyy",
                "MM-dd-yyyy",
                intent.getStringExtra(CommonValues.DAY_DETAIL)!!
            ) + " " + DateUtilizer.incrementTimeByOne(DateUtilizer.getCurrentDate("hh:mm"))

            Log.e(
                "gettStartTime ",
                "------- " + DateUtilizer.incrementTimeByOne(DateUtilizer.getCurrentDate("hh:mm"))
            )
            Log.e("gettStartTime ", "------- " + DateUtilizer.getCurrentDate("hh:mm"))
            return DateUtilizer.incrementTimeByOne(DateUtilizer.getCurrentDate("hh:mm"))
        } else
            return "12:00 am"
    }

    private fun validated(): Boolean {
        if (selectTourTxt.text.toString().isNullOrEmpty() || selectTourTxt.text.toString()
                .equals(getString(R.string.str_select_your_tour))
        ) {
            AppUtils.showSnackBar(this, getString(R.string.str_select_tour))
            return false
        } else if (!startTimeTxt.text.toString().isNotEmpty()) {
            AppUtils.showSnackBar(this, getString(R.string.str_select_start_time))
            return false
        } else if (!endTimeTxt.text.toString().isNotEmpty()) {
            AppUtils.showSnackBar(this, getString(R.string.str_select_end_time))
            return false
        } else return isValidDifferenc()
    }

    private fun isValidDifferenc(): Boolean {
        Log.e("isValidDifferenc ", "------- " + endTimeString + "   " + startTimeString)
        var dateOne = DateUtilizer.getDateFromString("MM-dd-yyyy HH:mm", endTimeString)
        var dateTwo = DateUtilizer.getDateFromString("MM-dd-yyyy HH:mm", startTimeString)


        Log.e("isValidDifferenc ", "1 " + dateOne + "   " + dateTwo)
        Log.e("isValidDifferenc ", "2 " + dateOne?.time?.let {
            dateTwo?.time?.let { it1 ->
                val differenceBetweenDates = DateUtilizer.differenceBetweenDates(
                    it,
                    it1
                )
                differenceBetweenDates
            }
        })
        var secondaDifference = dateOne?.time?.let {
            dateTwo?.time?.let { it1 ->
                val differenceBetweenDates = DateUtilizer.differenceBetweenDates(
                    it,
                    it1
                )
                differenceBetweenDates
            }
        }
        val minutes = TimeUnit.SECONDS.toMinutes(secondaDifference!!);
        Log.e("isValidDifferenc ", " " + minutes)
        if (minutes >= 30) {
            return true
        } else if (minutes < 0) {
            AppUtils.showSnackBar(this, getString(R.string.str_invalid_time_selected))
            return false
        } else {
            AppUtils.showSnackBar(this, getString(R.string.str_invalid_time_duration))
            return false
        }
        return true
    }

}