package code_setup.ui_.home.views.schedule

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.ScrollView
import android.widget.TextView
import androidx.collection.LongSparseArray
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.RequestDateTourModel
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_util.*
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.app_util.location_utils.log
import code_setup.app_util.tachyon_day_view.DayView
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.DatesAdapter
import code_setup.ui_.home.apapter_.DayHoursAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.RequestRemoveTourModel
import code_setup.ui_.home.models.ScheduledTourListResponseModel
import com.base.mvp.BasePresenter
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import kotlinx.android.synthetic.main.event.*
import kotlinx.android.synthetic.main.layout_schedule_day_activity.*
import kotlinx.android.synthetic.main.month_navigation_lay.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.Serializable
import java.text.DateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList


class ScheduleDayActivity : CoreActivity(), HomeView {
    companion object {
        lateinit var instance: ScheduleDayActivity
    }

    private var selectedDate: String = ""
    lateinit var datHoursAdapter: DayHoursAdapter
    var TAG: String = ScheduleDayActivity::class.java.simpleName

    lateinit var datesAdapter: DatesAdapter

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))

        when (int) {
            /* NetworkRequest.REQUEST_GET_DATE_WISE_TOURS -> {
                 var responseData = list as TourListResponseModel
                 if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                     if (responseData.response_obj != null && responseData.response_obj.size > 0) {
                         addTourView(responseData.response_obj)
                     } else {
                         allEvents!!.clear()
                         onEventsChange()
                     }
                 } else if (responseData.response_code == NetworkCodes.SESSION.nCodes) {
                     AppUtils.showToast(getString(R.string.str_session_expired))
                     logoutUserNow()
                 } else {
                     AppUtils.showToast(responseData.response_message)
                 }
             }*/
            NetworkRequest.REQUEST_GET_DATE_WISE_SCHEDULED_TOURS -> {
                var responseData = list as ScheduledTourListResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    allEvents = LongSparseArray<List<TourData>>()
                    day = DateUtilizer.getcalenderFromString(
                        monthNameTxt.text.toString(),
                        "dd MMMM, YYYY"
                    )

                    /*  if (responseData.response_obj != null && responseData.response_obj.schedules.size > 0) {
                          addScheduledTourView(responseData.response_obj.schedules)
                      }*/
                    if (responseData.response_obj != null) {
                        addTourView(responseData.response_obj)
                    }

                    if (responseData.response_obj.bookings.size == 0 && responseData.response_obj.schedules.size == 0) {
                        allEvents!!.clear()

                    }
                    onEventsChange()
                    /* else {
                        allEvents!!.clear()
                        onEventsChange()
                    }*/
                } else if (responseData.response_code == NetworkCodes.SESSION.nCodes) {
                    AppUtils.showToast(getString(R.string.str_session_expired))
                    logoutUserNow()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }

            }
            NetworkRequest.REQUEST_DELETE_SCHEDULED_TOURS -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showSnackBar(this, getString(R.string.removed_successfully))
                    getTodaysTours(selectedDate)
                }
            }
        }
    }

    private fun addTourView(responseData: ScheduledTourListResponseModel.ResponseObj) {
        var tourList = ArrayList<TourData>()
        if (responseData.bookings.isNotEmpty())
            for (i in 0 until responseData.bookings.size) {
                tourList.add(
                    TourData(
                        responseData.bookings.get(i).tour_name,
                        responseData.bookings.get(i).start_location.name,
                        responseData.bookings.get(i).type,
                        responseData.bookings.get(i).seats.toString(),
                        getformatedTime(responseData.bookings.get(i).start_time),
                        getformatedTime(responseData.bookings.get(i).end_time),
                        DateUtilizer.getHourValue(responseData.bookings.get(i).start_time),
                        DateUtilizer.getMinutesValue(responseData.bookings.get(i).start_time),
                        DateUtilizer.getTimeDiffereence(
                            responseData.bookings.get(i).start_time,
                            responseData.bookings.get(i).end_time
                        ).toInt(),
                        R.drawable.rectangle_bg_with_app_border_and_bg,
                        false,
                        responseData.bookings.get(i).id,"",
                        "", responseData.bookings.get(i).status,responseData.bookings.get(i).is_ride,
                        "",
                        ""
                    )
                )
            }
        if (responseData.schedules.isNotEmpty())
            for (i in 0 until responseData.schedules.size) {
                tourList.add(
                    TourData(
                        responseData.schedules.get(i).name,
                        "No location",
                        "Manual Schedule",
                        "1",
                        getformatedTime(responseData.schedules.get(i).start_time),
                        getformatedTime(responseData.schedules.get(i).end_time),
                        DateUtilizer.getHourValue(responseData.schedules.get(i).start_time),
                        DateUtilizer.getMinutesValue(responseData.schedules.get(i).start_time),
                        DateUtilizer.getTimeDiffereence(
                            responseData.schedules.get(i).start_time,
                            responseData.schedules.get(i).end_time
                        ).toInt(),
                        R.drawable.rectangle_bg_with_app_border_and_bg,
                        true,
                        responseData.schedules.get(i).id,"",
                        "","",false,"",""
                    )
                )
            }



        log("addTourView  " + Gson().toJson(tourList))
        /* day = Calendar.getInstance()
         day.set(Calendar.HOUR_OF_DAY, 0)
         day.set(Calendar.MINUTE, 0)
         day.set(Calendar.SECOND, 0)
         day.set(Calendar.MILLISECOND, 0)*/

//        day = DateUtilizer.getcalenderFromString(monthNameTxt.text.toString(), "dd MMMM, YYYY")

        allEvents!!.put(day.getTimeInMillis(), tourList)
//        onEventsChange()
    }

    private fun addScheduledTourView(responseData: List<ScheduledTourListResponseModel.ResponseObj.Schedule>) {
        var tourList = ArrayList<TourData>()
        for (i in 0 until responseData.size) {
            tourList.add(
                TourData(
                    responseData.get(i).name,
                    "No location",
                    "Manual Schedule",
                    "1",
                    getformatedTime(responseData.get(i).start_time),
                    getformatedTime(responseData.get(i).end_time),
                    DateUtilizer.getHourValue(responseData.get(i).start_time),
                    DateUtilizer.getMinutesValue(responseData.get(i).start_time),
                    DateUtilizer.getTimeDiffereence(
                        responseData.get(i).start_time,
                        responseData.get(i).end_time
                    ).toInt(),
                    R.drawable.rectangle_bg_with_app_border_and_bg,
                    true,
                    responseData.get(i).id,
                    "",
                    "",
                    "",false,
                    "",
                    ""
                )
            )
        }

        log("addTourView  " + Gson().toJson(tourList))
        /* day = Calendar.getInstance()
         day.set(Calendar.HOUR_OF_DAY, 0)
         day.set(Calendar.MINUTE, 0)
         day.set(Calendar.SECOND, 0)
         day.set(Calendar.MILLISECOND, 0)*/



        allEvents!!.put(day.getTimeInMillis(), tourList)
//        onEventsChange()
    }




    private fun getformatedTime(strTimeTxt: String): String? {
        var timeStr: String = ""
        timeStr = DateUtilizer.getFormatedDate("dd-MM-YYYY, HH:mm", "hh:mm a", strTimeTxt)
        log("getformatedTime  " + timeStr)
        return timeStr
    }


    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_schedule_day_activity
    }

    /**
     * Some examples to demonstrate how the day view renders multiple events that are in close
     * proximity to each other.
     */
//    private val INITIAL_EVENTS: Array<TourData> = arrayOf<TourData>(
//        TourData(
//            "Per seat tour",
//            "Chandigarh",
//            "",
//            "",
//            "12:00 AM",
//            "12:30 AM",
//            0,
//            0,
//            30,
//            R.drawable.rectangle_bg_with_app_border_and_bg
//        ),
//        TourData(
//            "Per Person Tour",
//            "Panchkula",
//            "",
//            "",
//            "1:30 AM",
//            "3:00 AM",
//            1,
//            30,
//            90,
//            R.drawable.rectangle_bg_with_app_border_and_bg
//        ),
//        /* Event(
//             "Phone call",
//             "555-5555",
//             2,
//             0,
//             45,
//             android.R.color.holo_orange_dark
//         ),*/
//        /* Event(
//             "Lunch",
//             "Cafeteria",
//             2,
//             30,
//             30,
//             R.drawable.rectangle_bg_with_app_border_and_bg
//         ),*/
//        TourData(
//            "Dinner",
//            "Home",
//            "",
//            "",
//            "6:00 PM",
//            "6:30 PM",
//            18,
//            0,
//            30,
//            R.drawable.rectangle_bg_with_app_border_and_bg
//        )
//    )

    lateinit var day: Calendar
    private var allEvents: LongSparseArray<List<TourData>>? = null
    private var dateFormat: DateFormat? = null
    private var timeFormat: DateFormat? = null
    private var editEventDate: Calendar? = null
    lateinit var editEventStartTime: Calendar
    private var editEventEndTime: Calendar? = null
    lateinit var editEventDraft: TourData

    //    private var content: ViewGroup? = null
//    private var dateTextView: TextView? = null
    private var scrollView: ScrollView? = null
    lateinit var dayView: DayView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
        AnimUtils.moveAnimationX(backToolbar, false)
        AnimUtils.moveAnimationX(backToolbar, true)
        txtTitletoolbar.gravity = Gravity.CENTER
        txtTitletoolbar.setText(R.string.strSchedule)
        backToolbar.setOnClickListener {
            onBackPressed()
        }
        nextmonthBtn.setSafeOnClickListener {

            monthNameTxt.setText(getNextDate())
            getTodaysTours(
                DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "MM-dd-yyyy",
                    monthNameTxt.text.toString()
                )
            )
            dayNameTxt.setText(
                DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "EEEE",
                    monthNameTxt.text.toString()
                )
            )
            checkDate()
        }
        previousMonthBtn.setSafeOnClickListener {

            monthNameTxt.setText(getPrevDate())
            getTodaysTours(
                DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "MM-dd-yyyy",
                    monthNameTxt.text.toString()
                )
            )
            dayNameTxt.setText(
                DateUtilizer.getFormatedDate(
                    "dd MMMM, yyyy",
                    "EEEE",
                    monthNameTxt.text.toString()
                )
            )
            checkDate()
        }


        try {
            Log.d(" Intent data ", "1 " + intent.getStringExtra(CommonValues.DAY_DETAIL))
            Log.d(" Intent data ", "2 " + intent.getStringExtra(CommonValues.DAY_DETAIL))
            Log.d(
                " Intent data ",
                "3 " + DateUtilizer.getFormatedDate(
                    "EEE MMM dd HH:mm:ss zzzz yyyy",
                    "yyyy-MM-dd",
                    intent.getStringExtra(CommonValues.DAY_DETAIL)!!
                )
            )
            monthNameTxt.setText(
                DateUtilizer.getFormatedDate(
                    "EEE MMM dd HH:mm:ss zzzz yyyy",
                    "dd MMMM, yyyy",
                    intent.getStringExtra(CommonValues.DAY_DETAIL)!!
                )
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
        getTodaysTours(
            DateUtilizer.getFormatedDate(
                "EEE MMM dd HH:mm:ss zzzz yyyy",
                "MM-dd-YYYY",
                intent.getStringExtra(CommonValues.DAY_DETAIL)!!
            )
        )

        dayNameTxt.setText(
            DateUtilizer.getFormatedDate(
                "EEE MMM dd HH:mm:ss zzzz yyyy",
                "EEEE",
                intent.getStringExtra(CommonValues.DAY_DETAIL)!!
            )
        )

        addTaskBtn.setSafeOnClickListener {
            var bndl = Bundle()
            bndl.putString(CommonValues.DAY_DETAIL, monthNameTxt.text.toString())
            activitySwitcher(this, AddAvailabilityActivity::class.java, bndl)
        }
        initHoursAdapter()
        //or
        initDaySetup()
        checkDate()
    }

    private fun checkDate() {
        if (DateUtilizer.compairDates(monthNameTxt.text.toString(), "dd MMMM, yyyy")) {
            addTaskBtn.visibility = View.VISIBLE
        } else {
            addTaskBtn.visibility = View.GONE
        }
    }

    private fun getNextDate(): String {
        try {
            Log.e(
                "nextmonthBtn ",
                " ==>  " + DateUtilizer.incrementDateByOne(
                    DateUtilizer.getDateFromString(
                        "dd MMMM, yyyy",
                        monthNameTxt.text.toString()
                    )
                )
            )
        } catch (e: Exception) {
        }


        return DateUtilizer.getStringFromDate(
            DateUtilizer.incrementDateByOne(
                DateUtilizer.getDateFromString(
                    "dd MMMM, yyyy",
                    monthNameTxt.text.toString()
                )
            )!!
        )
    }

    private fun getPrevDate(): String {
        try {
            Log.e(
                "previousMonthBtn ",
                " ==>  " + DateUtilizer.decrementDateByOne(
                    DateUtilizer.getDateFromString(
                        "dd MMMM, yyyy",
                        monthNameTxt.text.toString()
                    )
                )
            )
        } catch (e: Exception) {
        }
        return DateUtilizer.getStringFromDate(
            DateUtilizer.decrementDateByOne(
                DateUtilizer.getDateFromString(
                    "dd MMMM, yyyy",
                    monthNameTxt.text.toString()
                )
            )!!
        )
    }

    private fun initDaySetup() {
        // Create a new calendar object set to the start of today
        // Create a new calendar object set to the start of today
        day = Calendar.getInstance()
        day.set(Calendar.HOUR_OF_DAY, 0)
        day.set(Calendar.MINUTE, 0)
        day.set(Calendar.SECOND, 0)
        day.set(Calendar.MILLISECOND, 0)

        // Populate today's entry in the map with a list of example events
        // Populate today's entry in the map with a list of example events
        /* allEvents =
             LongSparseArray<List<TourData>>()
         allEvents!!.put(
             day.getTimeInMillis(),
             java.util.ArrayList<TourData>(Arrays.asList(*INITIAL_EVENTS))
         )*/

        dateFormat = DateFormat.getDateInstance(
            DateFormat.MEDIUM,
            Locale.getDefault()
        )
        timeFormat = DateFormat.getTimeInstance(
            DateFormat.SHORT,
            Locale.getDefault()
        )

//        content = findViewById(R.id.sample_content)
//        dateTextView = findViewById(R.id.sample_date)
        scrollView = findViewById(R.id.sample_scroll)
        dayView = findViewById(R.id.sample_day)

        // Inflate a label view for each hour the day view will display
        // Inflate a label view for each hour the day view will display
        val hour = day.clone() as Calendar
        val hourLabelViews: MutableList<View> =
            java.util.ArrayList()
        for (i in dayView.getStartHour()..dayView.getEndHour()) {
            hour[Calendar.HOUR_OF_DAY] = i
            val hourLabelView =
                layoutInflater.inflate(R.layout.hour_label, dayView, false) as TextView
            hourLabelView.text = timeFormat!!.format(hour.time)
            hourLabelViews.add(hourLabelView)
        }
        dayView.setHourLabelViews(hourLabelViews)

        onDayChange()
    }

    private fun onDayChange() {
//        dateTextView!!.text = dateFormat!!.format(day.time)
//        onEventsChange()
    }

    private fun onEventsChange() { // The day view needs a list of event views and a corresponding list of event time ranges
        Log.e("onEventsChange  ", " " + Gson().toJson(allEvents))
        var eventViews: MutableList<View?>? = null
        var eventTimeRanges: MutableList<DayView.EventTimeRange?>? = null
        val events: List<TourData>? =
            allEvents!![day.timeInMillis]
        if (events != null) { // Sort the events by start time so the layout happens in correct order
            Collections.sort(
                events,
                Comparator<TourData> { o1, o2 -> if (o1.hour < o2.hour) -1 else if (o1.hour == o2.hour) if (o1.minute < o2.minute) -1 else if (o1.minute == o2.minute) 0 else 1 else 1 })
            eventViews = java.util.ArrayList()
            eventTimeRanges = java.util.ArrayList<DayView.EventTimeRange?>()
            // Reclaim all of the existing event views so we can reuse them if needed, this process
// can be useful if your day view is hosted in a recycler view for example
            val recycled = dayView!!.removeEventViews()
            var remaining = recycled?.size ?: 0
            for (event in events) { // Try to recycle an existing event view if there are enough left, otherwise inflate
// a new one
                val eventView =
                    if (remaining > 0) recycled!![--remaining] else layoutInflater.inflate(
                        R.layout.event,
                        dayView,
                        false
                    )
                (eventView.findViewById<View>(R.id.event_title) as TextView).setText(event.title)
                if (!event.location.isNullOrBlank())
                    (eventView.findViewById<View>(R.id.event_location) as TextView).setText(event.location)
                (eventView.findViewById<View>(R.id.tourTimeTxt) as TextView).setText(event.startTime + " " + event.endTime)
                if (!event.seats.isNullOrBlank())
                    (eventView.findViewById<View>(R.id.numSeatsTxt) as TextView).setText(event.seats)
                if (!event.tourType.isNullOrBlank())
                    (eventView.findViewById<View>(R.id.tourTypeTxt) as TextView).setText(
                        event.tourType!!.replace(
                            "_",
                            " "
                        )
                    )
                eventView.setBackgroundResource(event.color)
                if (event.isManual!!) {
                    (eventView.findViewById<View>(R.id.scheduleDeleteBtn) as ImageView).visibility =
                        View.VISIBLE
                } else (eventView.findViewById<View>(R.id.scheduleDeleteBtn) as ImageView).visibility =
                    View.GONE
                (eventView.findViewById<View>(R.id.scheduleDeleteBtn) as ImageView).setOnClickListener {
                    Log.e("code_setup click ", " " + event.id)
                    deleteManualSchedule(event.id)
                }
                // When an event is clicked, start a new draft event and show the edit event dialog
                eventView.setOnClickListener {
                    editEventDraft = event
                    editEventDate = day.clone() as Calendar
                    editEventStartTime = Calendar.getInstance()
                    editEventStartTime.set(Calendar.HOUR_OF_DAY, editEventDraft.hour)
                    editEventStartTime.set(Calendar.MINUTE, editEventDraft.minute)
                    editEventStartTime.set(Calendar.SECOND, 0)
                    editEventStartTime.set(Calendar.MILLISECOND, 0)
                    editEventEndTime = editEventStartTime.clone() as Calendar
                    editEventEndTime!!.add(Calendar.MINUTE, editEventDraft.duration)
                    AppDialogs.openDialogTourDetail(
                        this,
                        "",
                        event,
                        object : OnBottomDialogItemListener<Any> {
                            override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                                when (type) {
                                    2 -> {
                                         deleteManualSchedule(event.id)
                                    }

                                }
                            }
                        })


//                    showEditEventDialog(true, editEventDraft.title, editEventDraft.location, editEventDraft.color)
                }
                eventViews.add(eventView)

                // The day view needs the event time ranges in the start minute/end minute format,
// so calculate those here
                val startMinute: Int = 60 * event.hour + event.minute
                val endMinute: Int = startMinute + event.duration
                eventTimeRanges.add(DayView.EventTimeRange(startMinute, endMinute))
            }
        }
        // Update the day view with the new events
        dayView!!.setEventViews(eventViews, eventTimeRanges)
    }

    private fun deleteManualSchedule(id: String?) {
        presenter.removeScheduledTour(
            NetworkRequest.REQUEST_DELETE_SCHEDULED_TOURS,
            RequestRemoveTourModel(
                id!!
            )
        )
    }

    private fun getTodaysTours(formatedDate: String) {
        selectedDate = formatedDate
        /* presenter.getTourFromDate(
             NetworkRequest.REQUEST_GET_DATE_WISE_TOURS,
             RequestDateTourModel(
                 formatedDate
             )
         )*/
        Handler().postDelayed(Runnable {
            presenter.getScheduledTourFromDate(
                NetworkRequest.REQUEST_GET_DATE_WISE_SCHEDULED_TOURS,
                RequestDateTourModel(
                    formatedDate
                )
            )
        }, 900)
    }

    private fun initHoursAdapter() {
        with(dayhoursRecycler) {
            datHoursAdapter = DayHoursAdapter(
                this@ScheduleDayActivity,
                getDayHoursList(),
                object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                    }
                })
            adapter = datHoursAdapter
            layoutManager = LinearLayoutManager(this@ScheduleDayActivity, VERTICAL, false)

        }

    }

    private fun getDayHoursList(): ArrayList<String> {
        var weekDays = ArrayList<String>()
        for (i in 1 until 25) {
            if (i <= 12)
                weekDays.add("" + i + " am")
            else {
                weekDays.add("" + (i % 12) + " pm")
            }
        }

        return weekDays
    }


    override fun onResume() {
        super.onResume()

    }

    /**
     * A data class used to represent an event on the calendar.
     */
    public class TourData(
        val title: String?,
        val location: String?,
        val tourType: String?,
        val seats: String?,
        val startTime: String?,
        val endTime: String?,
        val hour: Int,
        val minute: Int,
        val duration: Int,
        val color: Int,
        val isManual: Boolean? = false,
        val id: String?,
        val tourData: String?,
        val tourLink: String?,
        val status: String?,
        val isRide: Boolean? = false,
        val startLocation: String?,
        val endLocation: String?
    ):Serializable

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", "SCHEDULE DAY " + event.type)
        try {
            Log.d("onMessage", "SCHEDULE DAY " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {

            EVENTS.NETWORK_CONNECTION_CALLBACK -> {
                if (event.oj as Boolean) {
                    AppUtils.showSnackBarConnectiivity(
                        this,
                        getString(R.string.str_connected_to_internet),
                        true
                    )
                } else {
                    AppUtils.showSnackBarConnectiivity(
                        this,
                        getString(R.string.str_no_internet_conection),
                        false
                    )
                }

            }
            EVENTS.SCHEDULE_UPDATED -> {
                getTodaysTours(selectedDate)
            }
        }
        removeStickyEvent()
    }

    private fun removeStickyEvent() {
        var stickyEvent = EventBus.getDefault().getStickyEvent(CustomEvent::class.java)
// Better check that an event was actually posted before
        if (stickyEvent != null) {
            // "Consume" the sticky event
            EventBus.getDefault().removeStickyEvent(stickyEvent);
            // Now do something with it
        }
    }
}