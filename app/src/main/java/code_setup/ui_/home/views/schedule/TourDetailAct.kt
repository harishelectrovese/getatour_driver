package code_setup.ui_.home.views.schedule

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppUtils
import code_setup.app_util.CommonValues
import code_setup.app_util.DateUtilizer
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.home.apapter_.TourPassengersAdapter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.models.MonthlySchedulesToursListModel
import code_setup.ui_.home.views.chat_.ChatScreen
import com.electrovese.setup.R
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import kotlinx.android.synthetic.main.layout_tour_details_activity.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TourDetailAct : CoreActivity(), HomeView {
    private var userList: ArrayList<MonthlySchedulesToursListModel.ResponseObj.Booking.User>? = null
    private var tourIdd: String? = ""
    private var passngrAdapter: TourPassengersAdapter? = null
    private var tourData: ScheduleDayActivity.TourData? = null
    var TAG: String = TourDetailAct::class.java.simpleName
    override fun onActivityInject() {

    }

    override fun getScreenUi(): Int {
        return R.layout.layout_tour_details_activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initToolbar()
        initPassengersAdapter()
        getIntentData()
        detailTxtBtnDetail.setPaintFlags(detailTxtBtnDetail.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        detailTxtBtnDetail.setOnClickListener {
            openBrowserDetail()
        }
        registerCallPermission()
    }
    private lateinit var requestCAllPermissionLauncher: ActivityResultLauncher<String>
    @RequiresApi(Build.VERSION_CODES.M)
    private fun registerCallPermission() {
        requestCAllPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
                if (granted) {
                    Log.d(TAG, "registercallPermission - CAll Permission Granted")
                    InitCall(contactString)
                } else {
                    Log.d(TAG, "registerCAllPermission - Call Permission NOT Granted")
                    requestCallPermission()
                }
            }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestCallPermission() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) == PackageManager.PERMISSION_GRANTED -> {

                Log.d(TAG, "requestStoragePermission - call Permission Granted")

                // The permission is granted
                // you can go with the flow that requires permission here
            }
            shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE) -> {
                // This case means user previously denied the permission
                // So here we can display an explanation to the user
                // That why exactly we need this permission
                Log.d(TAG, "requestStoragePermission - CAll Permission NOT Granted")
                val snackbar = Snackbar
                    .make(
                        mainViewDetail,
                        getString(R.string.call_permiss_requirted),
                        Snackbar.LENGTH_LONG
                    )
                    .setAction("Try Again") {
                        requestCAllPermissionLauncher.launch(Manifest.permission.CALL_PHONE)
                    }

                snackbar.show()

            }
            else -> {
                // Everything is fine you can simply request the permission
                showSnackbar()
            }
        }
    }

    private fun showSnackbar() {
        val snackbar = Snackbar
            .make(mainViewDetail, getString(R.string.call_permiss_requirted), Snackbar.LENGTH_LONG)
            .setAction("Settings") {
                startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    data = Uri.fromParts("package", packageName, null)
                })
            }

        snackbar.show()
    }

    private fun Status_checkCallPermission(): Boolean {
        val camera = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.CALL_PHONE
        )

        return camera == PackageManager.PERMISSION_GRANTED
    }
    private fun openBrowserDetail() {
        try {
            val url = tourData!!.tourLink
            val uri: Uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            // Verify that the intent will resolve to an activity
            // Verify that the intent will resolve to an activity
            if (intent.resolveActivity(packageManager) != null) {
                // Here we use an intent without a Chooser unlike the next example
                startActivity(intent)
            }
        } catch (e: Exception) {
        }
    }

    private var contactString: String = ""
    private fun initPassengersAdapter() {
        with(passengersRecyclar) {
            passngrAdapter = TourPassengersAdapter(this@TourDetailAct,
                ArrayList(), object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                        when (type) {
                            CommonValues.MESSAGE_CLICK -> {
                                val intentt = Intent(this@TourDetailAct, ChatScreen::class.java)
                                intentt.putExtra(CommonValues.TOUR_ID, tourIdd)
                                intentt.putExtra(CommonValues.TOUR_STATUS, tourData!!.status)
                                var positionData =
                                    (t as MonthlySchedulesToursListModel.ResponseObj.Booking.UserBooking).user
                                positionData.user_booking_id = getUserBookingId(positionData._id)
                                intentt.putExtra(CommonValues.TOUR_DATA, positionData)
                                val options =
                                    androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        this@TourDetailAct,
                                        view!!,
                                        view.transitionName
                                    )
                                startActivity(intentt, options.toBundle())
                            }
                            CommonValues.CALL_CLICK -> {
                                contactString =
                                    (t as MonthlySchedulesToursListModel.ResponseObj.Booking.UserBooking).user.contact.toString()!!
                                if (Status_checkCallPermission()) {
                                    InitCall(contactString)
                                } else {
                                    requestCAllPermissionLauncher.launch(Manifest.permission.CALL_PHONE)
                                }
                            }
                        }
                    }
                })
            adapter = passngrAdapter
            layoutManager = LinearLayoutManager(this@TourDetailAct, VERTICAL, false)

        }
    }

    private fun getUserBookingId(userId: String?): String? {
        for (i in 0 until userList!!.size) {
            if (userId.equals(userList!!.get(i).user_id)) {
                return userList!!.get(i).user_booking_id
            }
        }
        return ""
    }

    private fun getIntentData() {
        tourData =
            intent.getSerializableExtra(CommonValues.TOUR_DATA) as ScheduleDayActivity.TourData
        try {
            Log.e("Tour Data "," "+Gson().toJson(tourData))
        } catch (e: Exception) {
        }
        tourIdd = tourData!!.id
        if (tourData != null) {
            tourNameTxtDetail.setText(tourData!!.title)
            try {
                tourDurationTxtDetail.setText("" + DateUtilizer.getHoursFromMin(tourData!!.duration))
            } catch (e: Exception) {
                tourDurationTxtDetail.setText("" + tourData!!.duration + " Min")
            }
            try {
                tourStarttimeTxtDetail.setText(tourData!!.startTime)
                tourEndtimeTxtDetail.setText(tourData!!.endTime)
            } catch (e: Exception) {
            }


            if (tourData!!.isManual!!) {
                noPassengerTxt.visibility = View.VISIBLE
                passengersRecyclar.visibility = View.GONE
                detailTxtBtnDetail.visibility = View.GONE
            } else {
                noPassengerTxt.visibility = View.GONE
                passengersRecyclar.visibility = View.VISIBLE
                detailTxtBtnDetail.visibility = View.VISIBLE

                var tourDataJsonString = tourData!!.tourData
                var tourDataMdl = Gson().fromJson(
                    tourDataJsonString,
                    MonthlySchedulesToursListModel.ResponseObj.Booking::class.java
                )
                passngrAdapter!!.updateAll(tourDataMdl.userBookings as ArrayList<MonthlySchedulesToursListModel.ResponseObj.Booking.UserBooking>)
                userList =
                    tourDataMdl.users as ArrayList<MonthlySchedulesToursListModel.ResponseObj.Booking.User>
                passngrAdapter!!.setRideStatus(tourData!!.status)

                try {
                    Log.e("Tour Data 2 "," "+Gson().toJson(userList))
                } catch (e: Exception) {
                }
                if(tourData!!.isRide!!)
                {
                    txtTitletoolbar.setText(R.string.ride_detail)
                    detailTxtBtnDetail.visibility=View.INVISIBLE
                    bookingPassengerLabelTxt.setText(R.string.str_booking)
                    endLocationHolder.visibility=View.VISIBLE
                    lableTxt.setText(R.string.str_from)
                    tourNameTxtDetail.setText(tourData!!.startLocation)
                    endLocationTxtDetail.setText(tourData!!.endLocation)
                    passngrAdapter!!.setIsRide(true)
                }else{
                    txtTitletoolbar.setText(R.string.tour_detail)
                    detailTxtBtnDetail.visibility=View.VISIBLE
                    bookingPassengerLabelTxt.setText(R.string.str_booking_passenger)
                    endLocationHolder.visibility=View.GONE
                }
            }
        }
    }

    private fun initToolbar() {
        AnimUtils.moveAnimationX(backToolbar, false)
        AnimUtils.moveAnimationX(backToolbar, true)
        txtTitletoolbar.gravity = Gravity.CENTER
        txtTitletoolbar.setText(R.string.detail)
//      monthNameTxt.setText(DateUtilizer.getCurrentDate(DateUtilizer.TITLE_MONTH_FORMAT))
        backToolbar.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResponse(list: Any, int: Int) {

    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)

    }

    /*
   *  E V E N T  BUS
   *  Update Viewe as per respected event
   *  @ Send custom object @CustomEvent
   * */
    @SuppressLint("RestrictedApi")
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " " + event.type)
        when (event.type) {
            EVENTS.CALL_PERMISSION_GRANTED -> {
                if (!contactString.isNullOrEmpty()) {
                    InitCall(contactString)
                } else AppUtils.showToast("Contact number not found")
            }
        }
    }
}