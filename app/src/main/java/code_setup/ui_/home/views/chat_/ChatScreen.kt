package code_setup.ui_.home.views.chat_

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.BaseApplication
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.SendMessageRequestModel
import code_setup.app_models.request_.UpdateBookingRequestModel
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_models.response_.MessageListResponseModel
import code_setup.app_models.response_.TourDetailResponseModel
import code_setup.app_util.AppUtils
import code_setup.app_util.CommonValues
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.app_util.socket_work.SocketService
import code_setup.app_util.socket_work.model_.SocketUpdateModel
import code_setup.net_.NetworkCodes
import code_setup.ui_.home.apapter_.ChatConversationAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.schedule.TourDetailAct
import com.electrovese.setup.R
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.default_toolbar_lay.*
import kotlinx.android.synthetic.main.layout_chat_screen.*
import kotlinx.android.synthetic.main.layout_tour_details_activity.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class ChatScreen : CoreActivity(), HomeView {
    lateinit var adapterConvo: ChatConversationAdapter
    lateinit var riderDataMdl: TourDetailResponseModel.ResponseObj.User
    lateinit var tourId: String
    var TAG: String = ChatScreen::class.java.simpleName
    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_chat_screen
    }

    override fun onResponse(list: Any, int: Int) {
        Log.e("onResponse", " " + Gson().toJson(list))
        when (int) {
            code_setup.net_.NetworkRequest.REQUEST_SEND_MESSAGE -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    chatMessageField.setText("")
                    getConversation()
                }
            }
            code_setup.net_.NetworkRequest.REQUEST_FIND_CHAT_CONVERSATION -> {
                var responseData = list as MessageListResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    if (responseData.response_obj != null && responseData.response_obj.size > 0) {
                        adapterConvo.updateAll(responseData.response_obj)
                        chatRecyclar.smoothScrollToPosition((adapterConvo.itemCount - 1))
                        setScroller()
                    }
                }
            }
        }
    }

    private fun setScroller() {
        try {
            if (Build.VERSION.SDK_INT >= 11) {
                chatRecyclar.addOnLayoutChangeListener(View.OnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                    if (bottom < oldBottom) {
                        chatRecyclar.postDelayed(Runnable {
                            chatRecyclar.smoothScrollToPosition(
                                chatRecyclar.getAdapter()!!.getItemCount() - 1
                            )
                        }, 100)
                    }
                })
            }
        } catch (e: Exception) {
        }
    }

    override fun showProgress() {
        loaderViewChat.show()
    }

    override fun hideProgress() {
        loaderViewChat.hide()
    }


    override fun noResult() {

    }

    override fun onError() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        toolbarBack.setOnClickListener {
            onBackPressed()
        }
        chatImageHolder.visibility = View.VISIBLE
        callIco.visibility = View.VISIBLE
        getIntentData(intent)

        initAdapter()
        Handler().postDelayed(Runnable {
            showProgress()
            getConversation()
        }, 1000)
        val function: (View) -> Unit = {
            if (validated()) {
                presenter.sendMessageRequest(
                    code_setup.net_.NetworkRequest.REQUEST_SEND_MESSAGE,
                    SendMessageRequestModel(
                        riderDataMdl.user_booking_id!!,
                        chatMessageField.text.toString().replace("\\s+", " ")
                    )
                )
            }
        }

        sendMessageBtn.setSafeOnClickListener(function)
        callIco.bringToFront()
        callIco.setSafeOnClickListener {
            if (riderDataMdl != null && !riderDataMdl.contact.isNullOrBlank())

            if (Status_checkCallPermission()) {
                InitCall(riderDataMdl.contact!!)
            } else {
                requestCAllPermissionLauncher.launch(Manifest.permission.CALL_PHONE)
            }
        }
        registerCallPermission()
    }
    private lateinit var requestCAllPermissionLauncher: ActivityResultLauncher<String>
    @RequiresApi(Build.VERSION_CODES.M)
    private fun registerCallPermission() {
        requestCAllPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
                if (granted) {
                    Log.d(TAG, "registercallPermission - CAll Permission Granted")
                    InitCall(riderDataMdl.contact!!)
                } else {
                    Log.d(TAG, "registerCAllPermission - Call Permission NOT Granted")
                    requestCallPermission()
                }
            }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestCallPermission() {
        when {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CALL_PHONE
            ) == PackageManager.PERMISSION_GRANTED -> {

                Log.d(TAG, "requestStoragePermission - call Permission Granted")

                // The permission is granted
                // you can go with the flow that requires permission here
            }
            shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE) -> {
                // This case means user previously denied the permission
                // So here we can display an explanation to the user
                // That why exactly we need this permission
                Log.d(TAG, "requestStoragePermission - CAll Permission NOT Granted")
                val snackbar = Snackbar
                    .make(
                        mainViewChat,
                        getString(R.string.call_permiss_requirted),
                        Snackbar.LENGTH_LONG
                    )
                    .setAction("Try Again") {
                        requestCAllPermissionLauncher.launch(Manifest.permission.CALL_PHONE)
                    }

                snackbar.show()

            }
            else -> {
                // Everything is fine you can simply request the permission
                showSnackbar()
            }
        }
    }

    private fun showSnackbar() {
        val snackbar = Snackbar
            .make(mainViewChat, getString(R.string.call_permiss_requirted), Snackbar.LENGTH_LONG)
            .setAction("Settings") {
                startActivity(Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                    data = Uri.fromParts("package", packageName, null)
                })
            }

        snackbar.show()
    }

    private fun Status_checkCallPermission(): Boolean {
        val camera = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.CALL_PHONE
        )

        return camera == PackageManager.PERMISSION_GRANTED
    }
    override fun onResume() {
        super.onResume()
        subscribeForChat()
        BaseApplication.instance.CHAT_OPENED = true
    }

    override fun onPause() {
        super.onPause()
        BaseApplication.instance.CHAT_OPENED = false
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        intNetworkListner()
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " CHAT SCREEN " + event.type)
        Log.d("onMessage", " CHAT SCREEN " + event.oj.toString())
        when (event.type) {
            EVENTS.NEW_MESSAGE -> {// In case of push notification
                getConversation()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        unregisterNetworkChanges()
    }

    private fun subscribeForChat() {
        /*  try {
              if (SocketService.instance != null&&SocketService.instance.mSocket!!.connected()) {
                  Prefs.putString(CommonValues.RECENT_SUBSCRIBER_ID, riderDataMdl.user_booking_id)
                  SocketService.instance.mSocket!!.emit("subscribe", riderDataMdl.user_booking_id)
                  SocketService.instance.mSocket!!.on(EVENTS.MESSAGE_RECEIVED, getMessage)
              } else {
                  restartService()
              }
          } catch (e: Exception) {
              e.printStackTrace()
              restartService()
          }*/
    }

    private fun restartService() {
        stopService(Intent(this, SocketService::class.java))
        Handler().postDelayed(Runnable {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, Intent(this, SocketService::class.java))
            } else {
                startService(Intent(this, SocketService::class.java))
            }
            subscribeForChat()
        }, 1000)
    }

    override fun onDestroy() {
        super.onDestroy()
        /*  if (SocketService.instance != null) {
              SocketService.instance.mSocket!!.off(EVENTS.MESSAGE_RECEIVED)
          }*/
    }

    @RequiresApi(Build.VERSION_CODES.N)
    val getMessage = Emitter.Listener { args ->

        Log.d("GET_MESSAGE_Socket: ", " 1 chat screen " + args[0])
        Log.d("GET_MESSAGE_Socket: ", " 2  chat screen " + Gson().toJson(args))

        var updatesModel = try {
            Gson().fromJson("" + args[0], SocketUpdateModel::class.java)
        } catch (e: Exception) {
            getConversation()
        }
//        Log.d(" GET GET GET ", "   " + Gson().toJson(updatesModel.message.data))
//        when (updatesModel.message.action_code) {
//            EVENTS.MESSAGE_RECEIVED -> {
//                getConversation()
//            }


//        }
        return@Listener

    }

    private fun validated(): Boolean {
        if (!chatMessageField.text.toString().trim().isNotEmpty()
        ) {
            AppUtils.showToast(getString(R.string.str_enter_text_message))
            return false
        } else if (chatMessageField.text.toString().trim().isEmpty()) {
            AppUtils.showToast(getString(R.string.str_enter_text_message))
            return false
        }
        return true
    }

    private fun getConversation() {
        presenter.getTourChatConversation(
            code_setup.net_.NetworkRequest.REQUEST_FIND_CHAT_CONVERSATION,
            UpdateBookingRequestModel(riderDataMdl.user_booking_id!!)
        )
    }

    private fun getIntentData(intent: Intent) {
        tourId = intent.getStringExtra(CommonValues.TOUR_ID)!!
        Log.e("getIntentData ---", " " + tourId)
        var dataValue = intent.getSerializableExtra(CommonValues.TOUR_DATA)
        riderDataMdl = dataValue as TourDetailResponseModel.ResponseObj.User
        if (riderDataMdl != null) {
            Log.e("getIntentData -------------- ", " " + riderDataMdl.name)
            txt_title_toolbar.setText(riderDataMdl.name)
            try {
                if (!riderDataMdl.user_image.isNullOrBlank())
                    userImageChat.setImageURI(riderDataMdl.user_image)
            } catch (e: Exception) {
            }
        }
        if (intent.getStringExtra(CommonValues.TOUR_STATUS) != null) {
            var tourStatus = intent.getStringExtra(CommonValues.TOUR_STATUS)
            if (tourStatus.equals("COMPLETED", true)
                || tourStatus.equals("REJECTED", true)
                || tourStatus.equals("CANCELED", true)
            ) {
                messageFieldHolder.visibility = View.GONE
                callIco.visibility = View.INVISIBLE
            }
        }
    }

    private fun initAdapter() {
        with(chatRecyclar) {
            adapterConvo = ChatConversationAdapter(this@ChatScreen,
                ArrayList(), object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                    }
                })
            chatRecyclar.adapter = adapterConvo
            chatRecyclar.layoutManager = LinearLayoutManager(this@ChatScreen, VERTICAL, false)
        }


    }
}