package code_setup.ui_.home.views.qr_fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PixelFormat
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.ADLocation
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.OnBoardingRequestModel
import code_setup.app_models.response_.OnBoardingResponseModel
import code_setup.app_models.response_.TourDetailResponseModel
import code_setup.app_util.AppDialogs
import code_setup.app_util.AppUtils
import code_setup.app_util.CommonValues
import code_setup.app_util.Prefs
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.app_util.location_utils.MyTracker
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.AllTourMembersAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.chat_.ChatScreen
import com.budiyev.android.codescanner.*
import com.electrovese.setup.R
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import kotlinx.android.synthetic.main.layout_bottom_sheet_riders.*
import kotlinx.android.synthetic.main.layout_sacn_qr_activity.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


class QrCodeActivityForPickup : CoreActivity(), /*ZXingScannerView.ResultHandler,*/ HomeView,
    MyTracker.ADLocationListener {
    lateinit var adapterMmber: AllTourMembersAdapter
    val TAG: String = QrCodeActivityForPickup::class.java.simpleName
    private var mCurrenttLocation: LatLng? = null

    override fun whereIAM(loc: ADLocation?) {
        if (loc != null) {
            try {
                mCurrenttLocation = LatLng(loc.lat, loc.longi)
//                Prefs.putDouble(CommonValues.LATITUDE, loc.lat)
//                Prefs.putDouble(CommonValues.LONGITUDE, loc.longi)
                Log.e(
                    TAG, "whereIAM " + loc.address
                )
            } catch (e: Exception) {
            }
        }
    }

    lateinit var tourId: String
    lateinit var riderDataMdl: TourDetailResponseModel.ResponseObj

    @Inject
    lateinit var presenter: HomePresenter

    override fun onActivityInject() {
        DaggerHomeComponent.builder().appComponent(getAppcomponent())
            .homeModule(HomeModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun getScreenUi(): Int {

        return R.layout.layout_sacn_qr_activity
    }

    override fun onResume() {
        super.onResume()
        if (!checkPermissions()) {
            requestPermissions()
        } else {
            goScan()
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
//        mScannerView.stopCamera()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onResponse(list: Any, int: Int) {
        Log.e("onResponse", " " + Gson().toJson(list))
        when (int) {
            NetworkRequest.REQUEST_ONBOARDING -> {
                var responseData = list as OnBoardingResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    for (i in 0 until riderDataMdl.users.size) {
                        if (responseData.response_obj.user_booking_id.equals(riderDataMdl.users.get(i).user_booking_id)) {
                            codeField.setText("")
                            if (riderDataMdl.users.get(i).is_board) {
                                AppUtils.showToast(getString(R.string.str_already_scanned))
                            } else {
                                AppUtils.showToast(getString(R.string.str_picked_up_successfully))
                            }
                        }
                    }
                    refrshAdapter(responseData.response_obj.user_booking_id)
                    onBackPressed()
                   /* if (riderDataMdl.users.isNotEmpty()) {
                        if (riderDataMdl.users.size == 1) {
                            onBackPressed()
                        }
                    }*/
                } else {
                    AppUtils.showToast(responseData.response_message)
                    goScan()
                }

            }

        }
    }

    private fun refrshAdapter(userBId: String) {
        for (i in 0 until riderDataMdl.users.size) {
            if (userBId.equals(riderDataMdl.users.get(i).user_booking_id)) {
                /*  if (riderDataMdl.users.get(i).is_board) {
                      AppUtils.showToast(getString(R.string.str_already_scanned))
                  }*/
                riderDataMdl.users.get(i).is_board = true
            }
        }
        adapterMmber.updateAll(riderDataMdl.users)
        Handler().postDelayed(Runnable {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }, 10)
        EventBus.getDefault().postSticky(
            CustomEvent<Any>(
                EVENTS.REQUEST_TOUR_PICKUP_RIDER_CODE_SCANNED,
                userBId
            )
        )
        goScan()
    }


    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        txtTitletoolbar.setText(R.string.str_scan_qr_code)
        backToolbar.setOnClickListener {
            onBackPressed()
        }

        initScanner()
        initListeners()
        initAdapter()
        getIntentData(intent)
        MyTracker(getApplicationContext(), this).track()
        textViewHeader.setOnClickListener {
            slideUpDownBottomSheet()
        }
        submitBtn.setOnClickListener {
            if (validated()) {
                proceed(codeField.text.toString())
            }
        }

    }

    private fun validated(): Boolean {
        if (codeField.text.toString().isNullOrEmpty()) {
            AppUtils.showToast(getString(R.string.please_enter_code))
            return false
        }
        return true
    }


    private fun requestPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.CAMERA
            )
        ) {
            // Provide an additional rationale to the user. This would happen if the user denied the
            // request previously, but didn't check the "Don't ask again" checkbox.
            Log.i(TAG, "Displaying permission rationale to provide additional context.")
            AppDialogs.openCameraPermissionAlert(
                this,
                Any(),
                Any(),
                object : OnBottomDialogItemListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                })
            /* showSnackbar(R.string.permission_rationale, android.R.string.ok, View.OnClickListener {
                 // Request permission
                 startLocationPermissionRequest()
             })*/

        } else {
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            Log.i(TAG, "Requesting permission")
            startPermissionRequest()
        }
    }

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 35
    private fun startPermissionRequest() {
        ActivityCompat.requestPermissions(
            this, arrayOf(Manifest.permission.CAMERA),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    lateinit var codeScanner: CodeScanner
    private fun initScanner() {
        codeScanner = CodeScanner(this, scanner_view)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            Handler(Looper.getMainLooper()).post(
                Runnable {
//                    Toast.makeText(activity!!, "Scan result: ${it.text}", Toast.LENGTH_LONG).show()
//                    AppUtils.showToast(it.text.toString())
                    proceed(it.text.toString())
                })
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            Handler(Looper.getMainLooper()).post(
                Runnable {
                    Toast.makeText(
                        this, "Camera initialization error: ${it.message}",
                        Toast.LENGTH_LONG
                    ).show()
                })
        }

        scanner_view.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    private fun initAdapter() {
        with(allMembersRecyclar) {
            adapterMmber = AllTourMembersAdapter(
                this@QrCodeActivityForPickup,
                ArrayList(),
                object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                        when (type) {
                            CommonValues.CALL_CLICK -> {
                                InitCall((t as TourDetailResponseModel.ResponseObj.User).contact!!)
                            }
                            CommonValues.MESSAGE_CLICK -> {
                                val intent = android.content.Intent(
                                    this@QrCodeActivityForPickup,
                                    ChatScreen::class.java
                                )
                                intent.putExtra(CommonValues.TOUR_ID, tourId)
                                intent.putExtra(
                                    CommonValues.TOUR_DATA,
                                    t as TourDetailResponseModel.ResponseObj.User
                                )
                                val options =
                                    androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        this@QrCodeActivityForPickup,
                                        view!!,
                                        view.transitionName
                                    )
                                startActivity(intent, options.toBundle())
                            }
                        }
                    }

                })
            adapter = adapterMmber
            layoutManager = LinearLayoutManager(
                this@QrCodeActivityForPickup,
                VERTICAL,
                false
            )
        }
    }

    private fun getIntentData(intent: Intent?) {
        tourId = intent!!.getStringExtra(CommonValues.TOUR_ID)!!
        var dataValue = intent!!.getSerializableExtra(CommonValues.TOUR_DATA)

        riderDataMdl = dataValue as TourDetailResponseModel.ResponseObj
        if (riderDataMdl != null) {
            adapterMmber.updateAll(riderDataMdl.users)
        }
    }

    private fun goScan() {
//            mScannerViewAct.setResultHandler(this)
//            mScannerViewAct.startCamera()
        codeScanner.startPreview()
    }

    /*  override fun handleResult(p0: Result?) {
          Log.d("handleResult", "  " + p0.toString())
//        AppUtils.showToast(p0.toString())
          proceed(p0.toString())
      }*/

    private fun proceed(toString: String) {
        if (mCurrenttLocation != null) {
            presenter.updateOnBoarding(
                NetworkRequest.REQUEST_ONBOARDING,
                OnBoardingRequestModel(
                    toString,
                    tourId,
                    mCurrenttLocation!!.latitude.toString(),
                    mCurrenttLocation!!.longitude.toString()
                )
            )
        } else {
            mCurrenttLocation = LatLng(
                Prefs.getDouble(CommonValues.LATITUDE, 0.0),
                Prefs.getDouble(CommonValues.LONGITUDE, 0.0)
            )
            goScan()
        }
    }


    // BottomSheetBehavior variable
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    /**
     * method to initialize the listeners
     */
    private fun initListeners() { // register the listener for button click
        // Capturing the callbacks for bottom sheet
        bottomSheetBehavior = BottomSheetBehavior.from<ConstraintLayout>(bottomSheet)
        bottomSheetBehavior.isHideable = false
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {

            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {

                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {

                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }
                }
            }
        })
    }

    /***
     * Manually Slide up and Slide Down
     */
    private fun slideUpDownBottomSheet() {
        if (bottomSheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
//            buttonSlideUp.text = "Slide Down";
        } else {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED;
//            buttonSlideUp.text = "Slide Up"
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult")

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            when {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                grantResults.isEmpty() -> Log.i(TAG, "User interaction was cancelled.")

                // Permission granted.
                (grantResults[0] == PackageManager.PERMISSION_GRANTED) -> goScan()

                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                else -> {
                    /*  showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                          View.OnClickListener {
                              // Build intent that displays the App settings screen.
                              val intent = Intent().apply {
                                  action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                  data = Uri.fromParts("package", APPLICATION_ID, null)
                                  flags = Intent.FLAG_ACTIVITY_NEW_TASK
                              }
                              startActivity(intent)
                          })*/
                }
            }
        }
    }
}