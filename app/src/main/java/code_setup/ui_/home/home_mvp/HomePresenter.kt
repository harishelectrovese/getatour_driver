package code_setup.ui_.home.home_mvp

import android.app.Activity
import android.content.res.Resources
import android.util.Log
import code_setup.app_core.BaseApplication
import code_setup.app_models.request_.*
import code_setup.app_models.response_.CaptureInfoModel
import code_setup.app_util.AppUtils
import code_setup.app_util.location_utils.log
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.models.*
import com.base.mvp.BasePresenter
import com.base.util.SchedulerProvider
import com.electrovese.kotlindemo.networking.ApiInterface
import com.electrovese.setup.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MapStyleOptions
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class HomePresenter @Inject constructor(
    var api: ApiInterface,
    disposable: CompositeDisposable,
    scheduler: SchedulerProvider
) : BasePresenter<HomeView>(disposable, scheduler) {


    private val TAG: String = HomePresenter::class.java.simpleName


    /**
     * Add custom to map view
     */
    fun customMap(context: Activity, googleMap: GoogleMap) {
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            var success: Boolean = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                    context, R.raw.style_json1
                )
            )

            if (!success) {
                Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            Log.e(TAG, "Can't find style. Error: ", e)
        }
    }


    fun changeDriverStatus(
        requestChangeStatus: Int,
        changeDriverStatusRequest: ChangeDriverStatusRequest
    ) {
//        view?.showProgress()
        disposable.add(
            api.changeDriverStatus(
                BaseApplication.instance.getCommonHeaders(),
                changeDriverStatusRequest
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestChangeStatus)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }


    fun captureInfo(requestCode: Int, captureInfoData: CaptureInfoModel) {
//        view?.showProgress()
        disposable.add(
            api.captureInfo(BaseApplication.instance.getCommonHeaders(), captureInfoData)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        //                        view?.hideProgress()
                        view?.onResponse(result, requestCode)

                    },
                    { _ ->
                        //                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }


    fun getUpcominTours(requestCode: Int) {
        view?.showProgress()
        disposable.add(
            api.getUpcomingToursRequest(BaseApplication.instance.getCommonHeaders())
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }


    fun updateBookingRequest(
        reqCode: Int,
        updateBookingRequestModel1: UpdateBookingRequestModel
    ) {
        view?.showProgress()
        when (reqCode) {
            NetworkRequest.REQUEST_ACCEPT_BOOKING -> {
                disposable.add(
                    api.acceptTourRequest(
                        BaseApplication.instance.getCommonHeaders(),
                        updateBookingRequestModel1
                    )
                        .subscribeOn(scheduler.io())
                        .observeOn(scheduler.ui())
                        .subscribe(
                            { result ->
                                view?.hideProgress()
                                view?.onResponse(result, reqCode)

                            },
                            { _ ->
                                view?.hideProgress()
                                view?.onError()
                            })
                )
            }

            NetworkRequest.REQUEST_REJECT_BOOKING -> {
                disposable.add(
                    api.rejectTourRequest(
                        BaseApplication.instance.getCommonHeaders(),
                        updateBookingRequestModel1
                    )
                        .subscribeOn(scheduler.io())
                        .observeOn(scheduler.ui())
                        .subscribe(
                            { result ->
                                view?.hideProgress()
                                view?.onResponse(result, reqCode)

                            },
                            { _ ->
                                view?.hideProgress()
                                view?.onError()
                            })
                )
            }

        }
    }

    fun getCurrentStatus(requestCode: Int) {
        view?.showProgress()
        disposable.add(
            api.getStatusRequest(BaseApplication.instance.getCommonHeaders())
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)

                    },

                    {
                        log("getCurrentStatus " + it.localizedMessage)

                        view?.hideProgress()
                        view?.onError()
                        /*    try {
                                if (it is HttpException) {
                                    val exception: HttpException = it as HttpException
                                    when (exception.code()) {
                                        400 -> {
                                            log("getCurrentStatus   Error :  400" )
                                        }
                                        500 -> {
                                            log("getCurrentStatus   Error :  500" )
                                        }
                                        else -> {
                                            log("getCurrentStatus    Error :  else")
                                        }
                                    }
                                }
                            } catch (e: Exception) {
                                log("Exception     :  "+e.localizedMessage)
                            }*/


                    })
        )
    }

    fun startMovingToMeetingPontRequest(requestCode: Int, updateBookingRequestModel: StartMovingRequestModel) {
        view?.showProgress()
        disposable.add(
            api.startMovingToMeetingPointRequest(
                BaseApplication.instance.getCommonHeaders(),
                updateBookingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun reachedMeetingPontRequest(requestCode: Int, updateBookingRequestModel: StartMovingRequestModel) {
        view?.showProgress()
        disposable.add(
            api.reachedMeetingPointRequest(
                BaseApplication.instance.getCommonHeaders(),
                updateBookingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }


    fun startMovingRequest(requestCode: Int, updateBookingRequestModel: StartMovingRequestModel) {
        view?.showProgress()
        disposable.add(
            api.startMovingRequest(
                BaseApplication.instance.getCommonHeaders(),
                updateBookingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getTourDeatail(requestCode: Int, updateBookingRequestModel: UpdateBookingRequestModel) {
        view?.showProgress()
        disposable.add(
            api.getTourDetail(
                BaseApplication.instance.getCommonHeaders(),
                updateBookingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun updateArriveStatus(requestCode: Int, updateBookingRequestModel: UpdateRequestModel) {
        view?.showProgress()
        disposable.add(
            api.updateArriveStatus(
                BaseApplication.instance.getCommonHeaders(),
                updateBookingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun completeTripRequest(requestCode: Int, updateRequestModel: UpdateRequestModel) {
        view?.showProgress()
        disposable.add(
            api.markTripComplete(
                BaseApplication.instance.getCommonHeaders(),
                updateRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun updateOnBoarding(requestCode: Int, onBoardingRequestModel: OnBoardingRequestModel) {
        view?.showProgress()
        disposable.add(
            api.onBoardingRequest(
                BaseApplication.instance.getCommonHeaders(),
                onBoardingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getTourChatConversation(
        requestCode: Int,
        updateBookingRequestModel: UpdateBookingRequestModel
    ) {
        view?.showProgress()
        disposable.add(
            api.getMessagesRequest(
                BaseApplication.instance.getCommonHeaders(),
                updateBookingRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun sendMessageRequest(requestCode: Int, sendMessageRequestModel: SendMessageRequestModel) {
        if (!AppUtils.checkNetwork()) {
            AppUtils.showToast("No Internet Connection")
            return
        }
        view?.showProgress()
        disposable.add(
            api.sendMessagesRequest(
                BaseApplication.instance.getCommonHeaders(),
                sendMessageRequestModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getTourFromDate(requestCode: Int, requestDateTourModel: RequestDateTourModel) {
        view?.showProgress()
        disposable.add(
            api.requestTourList(BaseApplication.instance.getCommonHeaders(), requestDateTourModel)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getScheduledTourFromDate(requestCode: Int, requestDateTourModel: RequestDateTourModel) {
        view?.showProgress()
        disposable.add(
            api.requestScheduledTourList(
                BaseApplication.instance.getCommonHeaders(),
                requestDateTourModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }


    fun getAllToursForMonth(requestCode: Int, requestDateTourModel: RequestDateTourModel) {
        view?.showProgress()
        disposable.add(
            api.requestMonthlyTourList(
                BaseApplication.instance.getCommonHeaders(),
                requestDateTourModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun logoutUser(requestCode: Int) {
//        view?.showProgress()
        disposable.add(
            api.logoutUser(
                BaseApplication.instance.getCommonHeaders()
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        //                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        //                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun logoutUser(requestCode: Int, reqData: LogoutRequestData) {
//        view?.showProgress()
        disposable.add(
            api.logoutUser(
                BaseApplication.instance.getCommonHeaders(), reqData
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        //                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        //                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun scheduleCustomBooking(
        requestCode: Int,
        requestCalenderBookingModel: RequestCalenderBookingModel
    ) {
//        view?.showProgress()
        disposable.add(
            api.scheduleCalendarBooking(
                BaseApplication.instance.getCommonHeaders(), requestCalenderBookingModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        //                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        //                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getEarningData(requestCode: Int) {
        disposable.add(
            api.getEarningData(
                BaseApplication.instance.getCommonHeaders()
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        //                        view?.hideProgress()
                        view?.onResponse(result, requestCode)
                    },
                    { _ ->
                        //                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getProfileData(reqCode: Int) {
        view?.showProgress()
        disposable.add(
            api.getProfileDetail(BaseApplication.instance.getCommonHeaders())
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getAllTours(reqCode: Int, requestToursModel: RequestToursModel) {
        view?.showProgress()
        disposable.add(
            api.getTours(BaseApplication.instance.getCommonHeaders(), requestToursModel)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun requestRideSupport(reqCode: Int, requestRideSupport1: RequestRideSupport) {
        view?.showProgress()
        disposable.add(
            api.requestRideSupport(BaseApplication.instance.getCommonHeaders(), requestRideSupport1)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun requestCancelRide(reqCode: Int, requestRideSupport1: RequestCancelRide) {
        view?.showProgress()
        disposable.add(
            api.requestCancelRide(BaseApplication.instance.getCommonHeaders(), requestRideSupport1)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun removeScheduledTour(reqCode: Int, requestDateTourModel: RequestRemoveTourModel) {
        view?.showProgress()
        disposable.add(
            api.requestRemoveScheduledTour(
                BaseApplication.instance.getCommonHeaders(),
                requestDateTourModel
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun getRideHistory(reqCode: Int, bookingHistoryRequest: BookingHistoryRequest) {
        view?.showProgress()
        disposable.add(
            api.getBookingHistory(
                BaseApplication.instance.getCommonHeaders(),
                bookingHistoryRequest
            )
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->

                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }
}