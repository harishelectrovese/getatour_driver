package code_setup.ui_.home.apapter_

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import code_setup.app_models.response_.RideHistoryResponseModel
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.home.models.ToursListResponseModel
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_tour_list_view.view.*


class TourListAdapter(
    internal var activity: Activity,
    val dataList: ArrayList<ToursListResponseModel.ResponseObj>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<TourListAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_tour_list_view,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<ToursListResponseModel.ResponseObj>) {
        this.dataList.clear();
        this.dataList.addAll(posts);
        notifyDataSetChanged();
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: ToursListResponseModel.ResponseObj,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            //            tripsPriceTxt.setText("")

            try {
                tourNameTxt.setText(part.name)

            } catch (e: Exception) {
                e.printStackTrace()
            }
            itemView.setOnClickListener {
                listener.onItemClick(tourNameTxt, posit, 0, part)
            }


        }
    }

}
