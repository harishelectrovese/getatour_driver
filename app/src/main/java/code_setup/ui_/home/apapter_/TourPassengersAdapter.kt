package code_setup.ui_.home.apapter_

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import code_setup.app_util.CommonValues
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.home.models.MonthlySchedulesToursListModel
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_tour_people_view.view.*


class TourPassengersAdapter(
    internal var activity: FragmentActivity,
    val dataList: ArrayList<MonthlySchedulesToursListModel.ResponseObj.Booking.UserBooking>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<TourPassengersAdapter.OptionViewHolder>() {

    var rideStatuss: String = ""
    var isRide: Boolean = false
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_tour_people_view,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: ArrayList<MonthlySchedulesToursListModel.ResponseObj.Booking.UserBooking>) {
        this.dataList.clear();
        this.dataList.addAll(posts);
        notifyDataSetChanged();
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: MonthlySchedulesToursListModel.ResponseObj.Booking.UserBooking,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {

            try {

                if (part.seats!!.toInt() <= 1) {
//                    memberSeats.setText(part.seats + " seat ")
                    memberSeats.setText(activity.getString(R.string.str_seat_booked) + " " + part.seats)
                } else {
//                    memberSeats.setText(part.seats + " seats ")
                    memberSeats.setText(activity.getString(R.string.str_seats_booked) + " " + part.seats)
                }
                if (isRide) {
                    memberSeats.visibility = View.GONE
                } else
                    memberSeats.visibility = View.VISIBLE
            } catch (e: Exception) {

            }
            Log.e("Tour Status Adapter ", "   " + isRide)
            memberName.setText(part.user.name)
            if (!rideStatuss.isNullOrBlank()) {
                if (!part.status.isNullOrBlank() && part.status.equals("COMPLETED", true)
                    || part.status.equals("REJECTED", true)
                    || part.status.equals("CANCELED", true)
                ) {
                    memberCallImg.visibility = View.INVISIBLE
//                    memberMessageImg.visibility = View.INVISIBLE
                } else {
                    memberCallImg.visibility = View.VISIBLE
                    memberMessageImg.visibility = View.VISIBLE
                }

            }


            memberCallImg.setOnClickListener {
                listener.onItemClick(memberCallImg, 0, CommonValues.CALL_CLICK, dataList.get(posit))
            }
            memberMessageImg.setOnClickListener {
                listener.onItemClick(
                    memberMessageImg,
                    0,
                    CommonValues.MESSAGE_CLICK,
                    dataList.get(posit)
                )
            }
        }
    }

    fun setRideStatus(rideStatuss: String?) {
        this.rideStatuss = rideStatuss!!
        notifyDataSetChanged()
    }

    fun setIsRide(isRide: Boolean) {
        this.isRide = isRide
        notifyDataSetChanged()
    }
}
