package code_setup.ui_.home.apapter_

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.home.models.EarningData
import code_setup.ui_.home.models.EarningResponseModel
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_y_axis_view.view.*


class GraphYaxisAdapter(
    internal var activity: FragmentActivity,
    val dataList: ArrayList<EarningData>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<GraphYaxisAdapter.OptionViewHolder>() {

    private var viewType: Int = 1   //0 for week, 1 for today,2 for month

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.adapter_y_axis_view, p0, false)
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<EarningData>) {
        this.dataList.clear();
        this.dataList.addAll(posts);
        notifyDataSetChanged();
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }

    fun setViewTo(i: Int) {
        viewType = i
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: EarningData,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            if (posit == (dataList.size-1)) {
                yValueTxt.setText("")
            } else
                yValueTxt.setText(part.value)
        }
    }

}
