package code_setup.ui_.home.apapter_

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import code_setup.app_models.other_.DateModel
import code_setup.app_util.DateUtilizer
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.home.views.schedule.ScheduleActivity
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_date_view_layout.view.*
import kotlinx.android.synthetic.main.month_navigation_lay.*
import java.lang.Exception


class DatesAdapter(
    internal var activity: Activity,
    val dataList: ArrayList<DateModel>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<DatesAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_date_view_layout,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList.get(position), position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<DateModel>) {
        this.dataList.clear()
        this.dataList.addAll(posts)
        notifyDataSetChanged()
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }

    fun nextMonth(toString: String) {

        var newList = DateUtilizer.getNextDays(
            34,
            DateUtilizer.getFormatedDate(
                "MMMM/dd/yyyy",
                DateUtilizer.DEFAULT_DATE_FORMAT,
                dataList.get(dataList.size - 1).fullDate
            )
        )
        this.dataList.clear()
        notifyDataSetChanged()
        this.dataList.addAll(newList)
        try {
            ScheduleActivity.instance.monthNameTxt.setText(dataList.get(15).monthYear)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        notifyDataSetChanged()
    }

    fun previousMonth(toString: String) {
        var newList = DateUtilizer.getPastDays(
            -34,
            DateUtilizer.getFormatedDate(
                "MMMM/dd/yyyy",
                DateUtilizer.DEFAULT_DATE_FORMAT,
                dataList.get(0).fullDate
            )
        )
        this.dataList.clear()
        notifyDataSetChanged()
        this.dataList.addAll(newList)
        try {
            ScheduleActivity.instance.monthNameTxt.setText(dataList.get(15).monthYear)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        notifyDataSetChanged()
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: DateModel,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {

            dateTxt.text = "" + part.date
            Log.e(" = ", "  " + ScheduleActivity.instance.monthNameTxt.text.toString())
            Log.e(" = ", "  " + part.monthYear)

            if (part.isSelected) {
                dateTxt.setBackgroundResource(R.drawable.drawable_circle)
                dateTxt.setTextColor(activity.resources.getColor(R.color.colorWhite))
            } else {
                dateTxt.setBackgroundColor(activity.resources.getColor(R.color.colorWhite))
                dateTxt.setTextColor(activity.resources.getColor(R.color.colorBlack))
            }

            if (ScheduleActivity.instance.monthNameTxt.text.toString().equals(part.monthYear)
            ) {
                dateTxt.setTextColor(activity.resources.getColor(R.color.colorBlack))
            } else {
                dateTxt.setTextColor(activity.resources.getColor(R.color.colorTextGrey))

            }
        }
    }

}
