package code_setup.ui_.home.apapter_

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppUtils
import code_setup.app_util.DateUtilizer
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.home.models.EarningResponseModel
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_graph_view.view.*
import kotlinx.android.synthetic.main.layout_earning_fragment.*


class GraphAdapter(
    internal var activity: androidx.fragment.app.FragmentActivity,
    val dataList: ArrayList<EarningResponseModel.ResponseObj.Earning>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<GraphAdapter.OptionViewHolder>() {

    private var viewType: Int = 1   //0 for week, 1 for today,2 for month
    private var stepSize: Int = 100
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.adapter_graph_view, p0, false)
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<EarningResponseModel.ResponseObj.Earning>) {
        this.dataList.clear();
        this.dataList.addAll(posts);
        notifyDataSetChanged();
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }

    fun setViewTo(i: Int) {
        viewType = i
    }

    fun updateStep(stepSize: Int) {
Log.e("updateStep "," ")
        try {
            if(stepSize==1000)
            {
                this.stepSize = 10
            }else
                this.stepSize = stepSize/100
        } catch (e: Exception) {
            this.stepSize = 10
        }
        notifyDataSetChanged()
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: EarningResponseModel.ResponseObj.Earning,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            if (part.total > 0) {
                textViewPrice.setText("" + part.bookings + " Rides" + '\n' + part.currency_symbol + "" + part.total.toString())
                textViewPrice.setBackgroundResource(R.drawable.rectangle_background_white)
            } else {
                textViewPrice.setText("")
                textViewPrice.setBackgroundResource(android.R.color.transparent)
            }
            AnimUtils.resizeView(textViewPillerPrice, 1000, 0, part.total / stepSize)
            when (viewType) {
                0 -> {
                    dayLableView.setText(
                        DateUtilizer.getFormatedDate(
                            "yyyy-MM-dd",
                            "EEEE",
                            part.date
                        )
                    )
                }
                1 -> {
                    dayLableView.setText(
                        DateUtilizer.getFormatedDate(
                            "yyyy-MM-dd",
                            "EEE",
                            part.date
                        )
                    )
                }
                2 -> {
                    dayLableView.setText(
                        DateUtilizer.getFormatedDate(
                            "yyyy-MM-dd",
                            "dd/MMM",
                            part.date
                        )
                    )
                }
            }

        }
    }

}
