package code_setup.ui_.home.apapter_

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import code_setup.app_models.response_.RideHistoryResponseModel
import code_setup.app_util.DateUtilizer
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.settings.models.RejectedRidesResponseModel
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_rejectted_ride_view.view.*
import kotlinx.android.synthetic.main.adapter_ride_history_view.view.*


class RejectedRidesAdapter(
    internal var activity: FragmentActivity,
    val dataList: ArrayList<RejectedRidesResponseModel.ResponseObj>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<RejectedRidesAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_rejectted_ride_view,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<RejectedRidesResponseModel.ResponseObj>) {
        this.dataList.clear();
        this.dataList.addAll(posts);
        notifyDataSetChanged();
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: RejectedRidesResponseModel.ResponseObj,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            try {
                tourImageRejected.setImageURI(part.tour_image)
            } catch (e: Exception) {
            }
            try {
//              ratingView.rating = part.driver_rating.toFloat()
                userNameTxt.setText("" + part.user_name)
                tourNameTxt.setText("" + part.tour_name)
                priceTxt.setText(part.amount.currency_symbol + " " + part.amount.total)
                penaltyChargeTxt.setText(
                    activity.getString(R.string.str_penalty_charges) + " " + part.amount.currency_symbol + " " + part.amount.rejected_fee
                )
                dateTimeTxt.setText(
                    part.booking_date
                )

            } catch (e: Exception) {
                e.printStackTrace()
            }
//            startLocationTxt.setText(part.start_loc.name)
//            endLocationTxt.setText(part.end_loc.name)
            if (part.routes != null && part.routes.size > 0) {
                startLocationTxt.setText(part.routes.get(0).name)
                if (part.routes.size > 1) {
                    endLocationTxt.setText(part.routes.get(part.routes.size - 1).name)
                    endLocationTxt.visibility = View.VISIBLE
                } else {
                    endLocationTxt.visibility = View.GONE
                }
            }


            if (part.is_ride) {
                tourNameHolderViewRjected.visibility = View.GONE
            } else tourNameHolderViewRjected.visibility = View.VISIBLE

            /* if (part.routes != null && part.routes.size > 0) {
                 startLocationTxt.setText(part.routes.get(0).name)
                 if (part.routes.size > 1) {
                     endLocationTxt.setText(part.routes.get(part.routes.size - 1).name)
                 }

             }*/
        }
    }

}
