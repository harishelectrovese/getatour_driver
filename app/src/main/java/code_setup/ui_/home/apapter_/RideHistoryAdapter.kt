package code_setup.ui_.home.apapter_

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_models.response_.RideHistoryResponseModel
import code_setup.app_util.DateUtilizer
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.settings.adapter_.TourHistoryDestinationAdapter
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_ride_history_view.view.*


class RideHistoryAdapter(
    internal var activity: FragmentActivity,
    val dataList: ArrayList<RideHistoryResponseModel.ResponseObj>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<RideHistoryAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_ride_history_view,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<RideHistoryResponseModel.ResponseObj>) {
        this.dataList.clear();
        this.dataList.addAll(posts);
        notifyDataSetChanged();
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: RideHistoryResponseModel.ResponseObj,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            //            tripsPriceTxt.setText("")
            try {
                tourImage.setImageURI(part.tour_image)
            } catch (e: Exception) {
            }
            if (part.is_ride) {
                tourNameHolderView.visibility = View.GONE
            } else tourNameHolderView.visibility = View.VISIBLE

            try {
                tripsNameTxt.setText(part.tour_name)
                ratingView.rating = part.driver_rating.toFloat()
                tripsDateTxt.setText(
                    DateUtilizer.getFormatedDate(
                        "dd-MM-yyyy, HH:mm",
                        "EEE, MMM dd, hh:mm a",
                        part.booking_date
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (part.routes != null && part.routes.size > 0) {
                startDestinationsTxt.setText(part.routes.get(0).name)
                if (part.routes.size > 1) {
                    endDestinationsTxt.setText(part.routes.get(part.routes.size - 1).name)
                    endDestinationsTxt.visibility = View.VISIBLE
                } else {
                    endDestinationsTxt.visibility = View.GONE
                }


                /*with(destinationsRecyclarHistory) {
                    val destAdapter = TourHistoryDestinationAdapter(
                        activity,
                        part.routes as ArrayList<RideHistoryResponseModel.ResponseObj.Route>,
                        object : OnItemClickListener<Any> {
                            override fun onItemClick(
                                view: View,
                                position: Int,
                                type: Int,
                                t: Any?
                            ) {

                            }
                        })
                    adapter = destAdapter
                    layoutManager = LinearLayoutManager(activity, VERTICAL, false)
                }*/
            }
            if (!part.price.isNullOrEmpty()) {
                tripsPriceTxt.visibility = View.VISIBLE
                tripsPriceTxt.setText(part.currency_symbol+" "+part.price)
            } else tripsPriceTxt.visibility = View.GONE

        }
    }

}
