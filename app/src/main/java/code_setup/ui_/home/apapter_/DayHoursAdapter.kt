package code_setup.ui_.home.apapter_

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import code_setup.app_util.callback_iface.OnItemClickListener
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_view_calender_day.view.*
import kotlinx.android.synthetic.main.adapter_view_day_hours.view.*
import kotlinx.android.synthetic.main.layout_item_tour_detail_view.view.*


class DayHoursAdapter(
    internal var activity: Activity,
    val dataList: ArrayList<String>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<DayHoursAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_view_day_hours,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<String>) {
        this.dataList.clear()
        this.dataList.addAll(posts)
        notifyDataSetChanged()
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }

    fun clear() {
        dataList.clear()
        notifyDataSetChanged()
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: Any,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            hourText.setText(part as String)
//            if (posit % 2 == 0) {
//
//                itemViewtourHour.visibility = View.VISIBLE
//            } else {
//                itemViewtourHour.visibility = View.GONE
//            }
            itemViewtourHour.visibility = View.VISIBLE
        }
    }

}
