package code_setup.ui_.home.di_home

import code_setup.app_core.CoreActivity
import code_setup.app_util.di.ActivityScope
import code_setup.app_util.di.AppComponent
import code_setup.ui_.home.views.*
import code_setup.ui_.home.views.chat_.ChatScreen
import code_setup.ui_.home.views.earning.EarningFragment
import code_setup.ui_.home.views.qr_fragment.QrCodeActivityForPickup
import code_setup.ui_.home.views.qr_fragment.QrCodeActivityForVehicle
import code_setup.ui_.home.views.qr_fragment.QrCodeFragment
import code_setup.ui_.home.views.schedule.AddAvailabilityActivity
import code_setup.ui_.home.views.schedule.ScheduleActivity
import code_setup.ui_.home.views.schedule.ScheduleDayActivity
import code_setup.ui_.settings.views.ridehistory.RideHistoryActivity

import dagger.Component

@ActivityScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(HomeModule::class))
interface HomeComponent {

    fun inject(homeActivity: HomeActivity)

    fun inject(qrCodeFragment: QrCodeFragment)
    fun inject(upcomingToursFragment: UpcomingToursFragment)
    fun inject(upcomingBottomSheet: UpcomingBottomSheet)
    fun inject(tourUpdatesFragment: TourUpdatesFragment)
    fun inject(qrCodeActivityForPickup: QrCodeActivityForPickup)
    fun inject(chatScreen: ChatScreen)
    fun inject(scheduleDayActivity: ScheduleDayActivity)
    fun inject(scheduleActivity: ScheduleActivity)
    fun inject(addAvailabilityActivity: AddAvailabilityActivity)
    abstract fun inject(earningFragment: EarningFragment)
    abstract fun inject(qrCodeActivityForVehicle: QrCodeActivityForVehicle)
    abstract fun inject(tourSupportActivity: TourSupportActivity)

}
