package code_setup.ui_.auth.di_auth


import code_setup.app_util.di.ActivityScope
import code_setup.app_util.di.AppComponent
import code_setup.ui_.auth.views.ForgotActivity
import code_setup.ui_.auth.views.LoginActivity
import code_setup.ui_.auth.views.OTPActivity
import code_setup.ui_.auth.views.fragments.LandingActivity
import code_setup.ui_.onboard.di_onboard.OnboardActivityModule
import dagger.Component

@ActivityScope
@Component(
    dependencies = arrayOf(AppComponent::class),
    modules = arrayOf(AuthActivityModule::class)
)
interface AuthActivityComponent {

    fun inject(homeActivity: OTPActivity)
    fun inject(loginActivity: LoginActivity)

    fun inject(forgotActivity: ForgotActivity)
}
