package code_setup.ui_.auth.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import code_setup.app_core.CoreFragment
import code_setup.ui_.auth.views.LoginActivity
import code_setup.ui_.auth.views.SignupTypeFragment
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.lading_fragment_layout.*
import kotlinx.android.synthetic.main.landing_layout.*

class LandingFragment : CoreFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.lading_fragment_layout, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        loginBtn.setOnClickListener {
            activitySwitcher(
                activity!!,
                LoginActivity::class.java,
                null
            )
        }
        signUpBtn.setOnClickListener {
            LandingActivity.instance.layoutTextMain.visibility = View.INVISIBLE
            LandingActivity.instance.replaceContainer(
                SignupTypeFragment()
            )
        }


    }

    override fun onActivityInject() {

    }


}