package code_setup.ui_.auth.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import com.electrovese.setup.R

class LandingActivity : CoreActivity() {

    companion object {
        lateinit var instance: LandingActivity
    }


    override fun onActivityInject() {
    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.landing_layout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        instance = this
        replaceContainer(LandingFragment())

    }


    fun replaceContainer(pFragment: Fragment) {
        if (pFragment != null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.landingContainer, pFragment)
            fragmentTransaction.commitAllowingStateLoss()

            // set the toolbar title
        }
    }

}