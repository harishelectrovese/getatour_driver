package code_setup.ui_.auth.views

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import code_setup.app_core.BaseApplication
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import code_setup.app_models.request_.VersionCheckRequestModel
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_util.AppDialogs
import code_setup.app_util.CommonValues
import code_setup.app_util.Prefs
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.location_utils.log
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkConstant
import code_setup.ui_.home.views.HomeActivity
import com.beauty.board.networking.RestConfig
import com.electrovese.setup.BuildConfig
import com.electrovese.setup.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.splach_layout.*

class SplashScreen : CoreActivity() {
    override fun onActivityInject() {
    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.splach_layout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appVersionTxt.setText(getString(R.string.strversion) + ": " + BuildConfig.VERSION_NAME.toString())
        versionCheckRequest()
//        moveNext()
    }

    override fun onResume() {
        super.onResume()
        BaseApplication.instance.generateFirebaseToken(this)

    }

    //    var mCompositeDisposable: CompositeDisposable? = null
    private fun versionCheckRequest() {
//        val versionCode: Int = BuildConfig.VERSION_CODE
//        val versionName: String = BuildConfig.VERSION_NAME

        var mCompositeDisposable = CompositeDisposable()
        val apiService = RestConfig.create()
        val add = mCompositeDisposable?.add(
            apiService.versionCheck(
                BaseApplication.instance.getCommonHeaders(),
                VersionCheckRequestModel(
                    BuildConfig.VERSION_CODE.toString(),
                    CommonValues.DEVICE_OS.toUpperCase(),
                    Build.VERSION.RELEASE
                )
            )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleeSuccess, this::handleeFaliur)
        )


    }

    fun handleeSuccess(baseResponse: BaseResponseModel) {

        var bResponse = baseResponse
        if (bResponse.response_code == NetworkConstant.SUCCESS) {
            log("handleSuccess if " + " version check ")
            Handler().postDelayed({
//                Log.e("onSuccess ", " " + Gson().toJson((baseResponse as BaseResponseModel)))
                var responseData = baseResponse
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    moveNext()
                }
            }, 100)
        } else {
            log("handleSuccess else " + "    ")
            showUpdateAlert()
        }
    }

    fun handleeFaliur(error: Throwable) {
        showUpdateAlert()
    }

    private fun showUpdateAlert() {
        AppDialogs.openVersionAlert(
            this@SplashScreen,
            Any(),
            Any(),
            object : OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    when (type) {
                        2 -> {
                            goToPlaystore()
                        }
                        1 -> {
                            finish()
                        }
                    }
                }
            })
    }

    private fun goToPlaystore() {
        val appPackageName =
            packageName // getPackageName() from Context or Activity object

        try {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$appPackageName")
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                )
            )
        }
    }

    private fun moveNext() {
        Handler().postDelayed({
            if (Prefs.getBoolean(CommonValues.IS_LOGEDIN, false)) {
                activitySwitcher(
                    this,
                    HomeActivity::class.java,
                    null
                )
                finish()
            } else {
                activitySwitcher(
                    this,
                    LoginActivity::class.java,
                    null
                )
                finish()
            }

        }, 2000)
    }

}