package code_setup.ui_.auth.auth_mvp

import com.base.mvp.BasePresenter
import com.base.util.SchedulerProvider
import code_setup.app_models.request_.LoginRequestModel
import com.burakeregar.githubsearch.home.presenter.AuthView
import com.electrovese.kotlindemo.networking.ApiInterface
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AuthPresenter @Inject constructor(
    var api: ApiInterface,
    disposable: CompositeDisposable,
    scheduler: SchedulerProvider
) : BasePresenter<AuthView>(disposable, scheduler) {

    fun getRepos(searchKey: String) {

        view?.showProgress()
        disposable.add(
            api.search(searchKey)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, 1)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }

    fun loginUserCall(reqCode: Int, loginRequestModel: LoginRequestModel) {
        view?.showProgress()
        disposable.add(
            api.loginRequest(loginRequestModel)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.ui())
                .subscribe(
                    { result ->
                        view?.hideProgress()
                        view?.onResponse(result, reqCode)

                    },
                    { _ ->
                        view?.hideProgress()
                        view?.onError()
                    })
        )
    }
}