package code_setup.ui_.settings.adapter_

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import code_setup.app_models.response_.ProfileResponseModel
import code_setup.app_util.DateUtilizer
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.ui_.settings.models.ReviewsResponseModel
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.adapter_new_review_view.view.*
import kotlinx.android.synthetic.main.adapter_review_view.view.*
import kotlinx.android.synthetic.main.adapter_review_view.view.userReview


class NewReviewsAdapter(
    internal var activity: FragmentActivity,
    val dataList: ArrayList<ReviewsResponseModel.ResponseObj.Review>,
    internal var listener: OnItemClickListener<Any>
) : androidx.recyclerview.widget.RecyclerView.Adapter<NewReviewsAdapter.OptionViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): OptionViewHolder {
        return OptionViewHolder(
            LayoutInflater.from(activity).inflate(
                R.layout.adapter_new_review_view,
                p0,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: OptionViewHolder, position: Int) {
        (holder).bind(dataList[position], position, listener)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    fun updateAll(posts: List<ReviewsResponseModel.ResponseObj.Review>) {
        this.dataList.clear()
        this.dataList.addAll(posts)
        notifyDataSetChanged()
    }

    fun addItem(posts: Object) {
        //        this.slotsList.add(0, posts);
        //        notifyDataSetChanged();
    }

    fun removeAll() {
        dataList.clear()
        notifyDataSetChanged()
    }


    inner class OptionViewHolder
        (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(
            part: ReviewsResponseModel.ResponseObj.Review,
            posit: Int,
            listener: OnItemClickListener<Any>
        ) = with(itemView) {
            userNameReview.setText(part.user.name)
            userCommentReview.setText(part.review)
            userReviewDate.setText(DateUtilizer.getFormatedDate("dd-MM-yyyy", "dd,MMMM,yyyy", part.createdAt))
            userRatingReview.setText(""+part.rating)
            tourCategoryReview.setText(part.type_category)
            try {
                userImageReview.setImageURI(part.user.image)
            } catch (e: Exception) {
            }
        }
    }

}
