package code_setup.ui_.settings.views.support

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.response_.BaseResponseModel
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_models.response_.LoginResponseModel
import code_setup.app_models.response_.ProfileResponseModel
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppUtils
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.settings.ProfileActivity
import code_setup.ui_.settings.di_settings.DaggerSettingsComponent
import code_setup.ui_.settings.di_settings.SettingsModule
import code_setup.ui_.settings.models.RequestSupportModel
import code_setup.ui_.settings.settings_mvp.SettingsPresenter
import code_setup.ui_.settings.settings_mvp.SettingsView
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_with_appbar.*
import kotlinx.android.synthetic.main.default_loading.*
import kotlinx.android.synthetic.main.layout_support_activity.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class SupportActivity : CoreActivity(), SettingsView {
    @Inject
    lateinit var presenter: SettingsPresenter

    override fun onActivityInject() {
        DaggerSettingsComponent.builder().appComponent(getAppcomponent())
            .settingsModule(SettingsModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    var TAG: String = SupportActivity::class.java.simpleName
    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))

        when (int) {
            NetworkRequest.REQUEST_SUPPORT -> {
                var responseData = list as BaseResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    AppUtils.showToast(getString(R.string.str_support_request_sent))
                    finish()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }
            }
            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                } else {
                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
                    logoutUserNow()
                }

            }
        }

    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_support_activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AnimUtils.moveAnimationX(backBtntoolbar, false)
        AnimUtils.moveAnimationX(backBtntoolbar, true)
        titleToolbar.setText(R.string.str_support)
        backBtntoolbar.setOnClickListener {
            onBackPressed()
        }
        sendBtn.setSafeOnClickListener {
            AppUtils.hideKeyboard(sendBtn)
            if (validated()) {
                if (!AppUtils.checkNetwork()) {
                    AppUtils.showToast(getString(R.string.no_internet_connection))
                    return@setSafeOnClickListener
                }
                presenter.requestSupport(
                    NetworkRequest.REQUEST_SUPPORT,
                    RequestSupportModel(
                        "",
                        "Driver Support",
                        AppUtils.getMyLocation()!!.latitude,
                        AppUtils.getMyLocation()!!.longitude,
                        messagelFieldSupport.text.toString()
                    )
                )
            }
        }

        setUserData()
        initTextChangeListner()
    }

    private fun initTextChangeListner() {
        messagelFieldSupport.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                messageLengthText.setText("" + messagelFieldSupport.text.length + "/" + "500")
                if (messagelFieldSupport.text.length == 500) {
                    messageLengthText.setTextColor(resources.getColor(R.color.colorRed))
                } else
                    messageLengthText.setTextColor(resources.getColor(R.color.colorTextGrey1))
            }
        })
    }

    private fun setUserData() {
        if (getUserData() != null) {
            var userData = getUserData() as LoginResponseModel.ResponseObj
            nameFieldSupport.setText(userData.name)
            contactFieldSupport.setText("" + userData.contact)
            emailFieldSupport.setText(userData.email)

        }
    }

    private fun validated(): Boolean {
        if (nameFieldSupport.text.isNullOrEmpty()) {
            AppUtils.showSnackBar(this, getString(R.string.enter_your_name))
            return false
        } else if (emailFieldSupport.text.isNullOrEmpty()) {
            AppUtils.showSnackBar(this, getString(R.string.enter_your_email))
            return false
        } else if (contactFieldSupport.text.isNullOrEmpty()) {
            AppUtils.showSnackBar(this, getString(R.string.str_enter_your_mobile_number))
            return false
        } else if (messagelFieldSupport.text.isNullOrEmpty()) {
            AppUtils.showSnackBar(this, getString(R.string.enter_your_support_message))
            return false
        }

        return true
    }

    override fun onResume() {
        super.onResume()

    }
    override fun onStart() {
        super.onStart()
        intNetworkListner()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        unregisterNetworkChanges()
        EventBus.getDefault().unregister(this)
    }
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", "SUPPORT SCREEN " + event.type)
        try {
            Log.d("onMessage", "SUPPORT SCREEN " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {
            EVENTS.REQUEST_NEW_TOUR_JOB -> {
                getCurrentStatus()
            }
        }
    }
    private fun getCurrentStatus() {
        presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
    }
}