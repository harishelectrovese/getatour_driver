package code_setup.ui_.settings.views.about

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import code_setup.app_core.CoreFragment
import code_setup.app_util.AppUtils
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.HomeActivity
import com.base.mvp.BasePresenter
import com.electrovese.setup.BuildConfig
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.layout_about_view.*
import kotlinx.android.synthetic.main.trans_toolbar_lay.*

class AboutFragment : CoreFragment(), HomeView {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_about_view, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        HomeActivity.homeInstance.txtToolbartitle.setText(R.string.str_about_app)
        HomeActivity.homeInstance.isOtherFragmentOpened(true)
        aboutContentTxt.setText(Html.fromHtml(getString(R.string.str_about_content_html)))
        suportMailTxt.setOnClickListener {
            AppUtils.emailIntent(activity as AppCompatActivity, "")
        }
        appVersionAbout.setText(": "+BuildConfig.VERSION_NAME.toString())
    }

    override fun onActivityInject() {

    }

    override fun onResponse(list: Any, int: Int) {

    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    override fun onError() {

    }

    override fun setPresenter(presenter: BasePresenter<*>) {

    }
}