package code_setup.ui_.settings.views.ridehistory

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppUtils
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.RejectedRidesAdapter
import code_setup.ui_.settings.di_settings.DaggerSettingsComponent
import code_setup.ui_.settings.di_settings.SettingsModule
import code_setup.ui_.settings.models.RejectedRidesResponseModel
import code_setup.ui_.settings.settings_mvp.SettingsPresenter
import code_setup.ui_.settings.settings_mvp.SettingsView
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_with_appbar.*
import kotlinx.android.synthetic.main.layout_rejected_rides_activity.*
import kotlinx.android.synthetic.main.layout_ride_history_activity.rideHistoryAdapter
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class RejectedRidesActivity : CoreActivity(), SettingsView {
    private lateinit var rejectedRideAdapter: RejectedRidesAdapter

    @Inject
    lateinit var presenter: SettingsPresenter

    override fun onActivityInject() {
        DaggerSettingsComponent.builder().appComponent(getAppcomponent())
            .settingsModule(SettingsModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    var TAG: String = RejectedRidesActivity::class.java.simpleName
    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))

        when (int) {
            NetworkRequest.REQUEST_REJECTED_TOURS -> {
                var responseData = list as RejectedRidesResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    if (responseData.response_obj.isNotEmpty()) {
                        rejectedRideAdapter.updateAll(responseData.response_obj)
                        noDatTxt.visibility = View.GONE
                        rideRejectedRecyclar.visibility = View.VISIBLE
                    } else {
                        noDatTxt.visibility = View.VISIBLE
                        rideRejectedRecyclar.visibility = View.GONE
                    }
                } else {
                    AppUtils.showToast(responseData.response_message)
                }
            }
            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                } else {
                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
                    logoutUserNow()
                }

            }
        }
    }

    override fun showProgress() {
        try {
            rideRejectedRecyclar.showShimmerAdapter()
        } catch (e: Exception) {
        }
    }

    override fun hideProgress() {
        try {
            rideRejectedRecyclar.hideShimmerAdapter()
        } catch (e: Exception) {
        }
    }

    override fun noResult() {

    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_rejected_rides_activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AnimUtils.moveAnimationX(backBtntoolbar, false)
        AnimUtils.moveAnimationX(backBtntoolbar, true)
        titleToolbar.setText(R.string.str_rejected_trips)
        backBtntoolbar.setOnClickListener {
            onBackPressed()
        }

        initAdapter()
        getRejectedTrips()
    }

    private fun getRejectedTrips() {
        presenter.getRejectedTours(NetworkRequest.REQUEST_REJECTED_TOURS)
    }

    private fun initAdapter() {
        with(rideRejectedRecyclar) {
            layoutManager = LinearLayoutManager(this@RejectedRidesActivity)
            rejectedRideAdapter = RejectedRidesAdapter(this@RejectedRidesActivity,
                ArrayList(), object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                    }

                })
            adapter = rejectedRideAdapter
        }
    }

    override fun onResume() {
        super.onResume()

    }
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " HOME SCREEN " + event.type)
        try {
            Log.d("onMessage", " HOME SCREEN " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {
            EVENTS.REQUEST_NEW_TOUR_JOB -> {
                getCurrentStatus()
            }
        }
    }
    private fun getCurrentStatus() {
        presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
    }
}