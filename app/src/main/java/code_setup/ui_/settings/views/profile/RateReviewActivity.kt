package code_setup.ui_.settings.views.profile

import android.os.Bundle
import android.util.Log
import android.view.View
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_models.response_.LoginResponseModel
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppUtils
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.settings.adapter_.NewReviewsAdapter
import code_setup.ui_.settings.di_settings.DaggerSettingsComponent
import code_setup.ui_.settings.di_settings.SettingsModule
import code_setup.ui_.settings.models.ReviewsResponseModel
import code_setup.ui_.settings.settings_mvp.SettingsPresenter
import code_setup.ui_.settings.settings_mvp.SettingsView
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.layout_profile_fragment.profileBackBtn
import kotlinx.android.synthetic.main.layout_ratings_reviews_activity.*
import kotlinx.android.synthetic.main.profile_ratings_view.*
import kotlinx.android.synthetic.main.profile_top_view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class RateReviewActivity : CoreActivity(), SettingsView {
    lateinit var reviewsAdapter: NewReviewsAdapter
    lateinit var compAapter: ComplimentsAdapter
    lateinit var tourId: String

    @Inject
    lateinit var presenter: SettingsPresenter

    override fun onActivityInject() {
        DaggerSettingsComponent.builder().appComponent(getAppcomponent())
            .settingsModule(SettingsModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onResponse(list: Any, int: Int) {
        Log.e("onResponse", " " + Gson().toJson(list))
        when (int) {
            NetworkRequest.REQUEST_DRIVER_REVIEWS -> {
                var responseData = list as ReviewsResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    try {
                        totalRatingsTxt.setText(""+responseData.response_obj.rating)
                    } catch (e: Exception) {
                    }
                    try {
                        totalReviewsCountTxt.setText("" + responseData.response_obj.total_reviews)
                    } catch (e: Exception) {
                    }
                    try {
                        totalTripsCountTxt.setText("" + responseData.response_obj.total_trips)
                    } catch (e: Exception) {
                    }
                    if (responseData.response_obj.compliments != null && responseData.response_obj.compliments.isNotEmpty()) {
                        compAapter.updateAll(responseData.response_obj.compliments)
                        noComplementTxtReview.visibility = View.GONE
                        compementsRecyclarReview.visibility = View.VISIBLE
                    } else {
                        noComplementTxtReview.visibility = View.VISIBLE
                        compementsRecyclarReview.visibility = View.GONE
                    }


                    if (responseData.response_obj.review != null && responseData.response_obj.review.isNotEmpty()) {
                        reviewsAdapter.updateAll(responseData.response_obj.review)
                        noReviewTxtReview.visibility = View.GONE
                        newReviewRecyclar.visibility = View.VISIBLE
                    } else {
                        noReviewTxtReview.visibility = View.VISIBLE
                        newReviewRecyclar.visibility = View.GONE
                    }
                }
            }

            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                } else {
                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
                    logoutUserNow()
                }

            }
        }
    }

    override fun showProgress() {
        profileLoaderView.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        try {
            profileLoaderView.visibility = View.GONE
        } catch (e: Exception) {
        }
    }

    override fun noResult() {

    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_ratings_reviews_activity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        iniToolbar()
        setupbasicDetail(getUserData() as LoginResponseModel.ResponseObj)
        initAdaper()
        getReviews()
    }

    private fun getReviews() {
        presenter.getReviews(NetworkRequest.REQUEST_DRIVER_REVIEWS)
    }

    private fun initAdaper() {
        with(compementsRecyclarReview) {
            compAapter =
                code_setup.ui_.settings.views.profile.ComplimentsAdapter(
                    this@RateReviewActivity,
                    ArrayList(),
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                        }
                    })
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                this@RateReviewActivity,
                androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = compAapter
        }


        with(newReviewRecyclar) {
            reviewsAdapter =
                NewReviewsAdapter(
                    this@RateReviewActivity,
                    ArrayList(),
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                        }
                    })
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
                this@RateReviewActivity,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
            adapter = reviewsAdapter
        }

    }

    private fun iniToolbar() {
        AnimUtils.moveAnimationX(profileBackBtn, false)
        AnimUtils.moveAnimationX(profileBackBtn, true)
        titleTxtProfile.visibility = View.VISIBLE
        titleTxtProfile.setText(R.string.str_ratings)
        profileBackBtn.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupbasicDetail(userData: LoginResponseModel.ResponseObj) {
        if (userData != null) {
//            var userData = getUserData() as LoginResponseModel.ResponseObj
            userNameVw.setText(userData.name)
//            usermNumberVw.setText("" + userData.country_code + " " + userData.contact)
            userEmailVw.setText(userData.email)
            userImageView.setImageURI(userData.user_image)

        }
    }

    override fun onResume() {
        super.onResume()

    }
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " HOME SCREEN " + event.type)
        try {
            Log.d("onMessage", " HOME SCREEN " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {
            EVENTS.REQUEST_NEW_TOUR_JOB -> {
                getCurrentStatus()
            }
        }
    }
    private fun getCurrentStatus() {
        presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
    }
}