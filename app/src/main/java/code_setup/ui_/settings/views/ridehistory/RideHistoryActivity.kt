package code_setup.ui_.settings.views.ridehistory

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.request_.BookingHistoryRequest
import code_setup.app_models.request_.UpdateBookingRequestModel
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_models.response_.OnBoardingResponseModel
import code_setup.app_models.response_.RideHistoryResponseModel
import code_setup.app_util.AnimUtils
import code_setup.app_util.AppDialogs
import code_setup.app_util.AppUtils
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.apapter_.RideHistoryAdapter
import code_setup.ui_.home.di_home.DaggerHomeComponent
import code_setup.ui_.home.di_home.HomeModule
import code_setup.ui_.home.home_mvp.HomePresenter
import code_setup.ui_.home.home_mvp.HomeView
import code_setup.ui_.home.views.HomeActivity
import code_setup.ui_.settings.di_settings.DaggerSettingsComponent
import code_setup.ui_.settings.di_settings.SettingsModule
import code_setup.ui_.settings.settings_mvp.SettingsPresenter
import code_setup.ui_.settings.settings_mvp.SettingsView
import com.electrovese.setup.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.common_toolbar_with_appbar.*
import kotlinx.android.synthetic.main.layout_ride_history_activity.*
import kotlinx.android.synthetic.main.status_badge_layout.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

class RideHistoryActivity : CoreActivity(), SettingsView {
    lateinit var rideAdapter: RideHistoryAdapter

    @Inject
    lateinit var presenter: SettingsPresenter

    override fun onActivityInject() {
        DaggerSettingsComponent.builder().appComponent(getAppcomponent())
            .settingsModule(SettingsModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onResponse(list: Any, int: Int) {
        Log.e("onResponse", " " + Gson().toJson(list))
        when (int) {
            NetworkRequest.REQUEST_BOOKING_HISTORY -> {
                var responseData = list as RideHistoryResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {

                    if (responseData.response_obj != null && responseData.response_obj.isNotEmpty()) {
                        rideAdapter.updateAll(responseData.response_obj)
                    }
                    if (rideAdapter.itemCount > 0) {
                        noHistoryTxt.visibility = View.GONE
                        rideHistoryAdapter.visibility = View.VISIBLE
                    } else {
                        noHistoryTxt.visibility = View.VISIBLE
                        rideHistoryAdapter.visibility = View.GONE
                    }
                } else if (responseData.response_code == NetworkCodes.SESSION.nCodes) {
                    AppUtils.showToast(getString(R.string.str_session_expired))
                    logoutUserNow()
                } else {
                    AppUtils.showToast(responseData.response_message)
                }
            }
            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                } else {
                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
                    logoutUserNow()
                }

            }
        }

    }

    private fun getCurrentStatus() {
        presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
    }

    override fun showProgress() {
        rideHistoryAdapter.showShimmerAdapter()
        loaderViewHistory.show()
    }

    override fun hideProgress() {
        try {
            rideHistoryAdapter.hideShimmerAdapter()
            loaderViewHistory.hide()
        } catch (e: Exception) {
        }
    }

    override fun noResult() {

    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_ride_history_activity
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AnimUtils.moveAnimationX(backBtntoolbar, false)
        AnimUtils.moveAnimationX(backBtntoolbar, true)
        titleToolbar.setText(R.string.str_service_history)
        backBtntoolbar.setOnClickListener {
            onBackPressed()
        }
        getCurrentStatus()
        initAdapter()
        getHistory()
    }

    private fun getHistory() {
        presenter.getRideHistory(
            NetworkRequest.REQUEST_BOOKING_HISTORY,
            BookingHistoryRequest("COMPLETED")
        )
    }

    private fun initAdapter() {
        with(rideHistoryAdapter) {
            layoutManager = LinearLayoutManager(this@RideHistoryActivity)
            rideAdapter = RideHistoryAdapter(this@RideHistoryActivity,
                ArrayList<RideHistoryResponseModel.ResponseObj>(),
                object : OnItemClickListener<Any> {
                    override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                    }

                })
            adapter = rideAdapter
        }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " HOME SCREEN " + event.type)
        try {
            Log.d("onMessage", " HOME SCREEN " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {
            EVENTS.REQUEST_NEW_TOUR_JOB -> {
                getCurrentStatus()
            }
        }
    }
}