package code_setup.ui_.settings

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Animatable2
import android.graphics.drawable.AnimatedVectorDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import code_setup.app_core.CoreActivity
import code_setup.app_models.other_.event.CustomEvent
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_models.response_.LoginResponseModel
import code_setup.app_models.response_.ProfileResponseModel
import code_setup.app_models.response_.ProfileUpdateResponseModel
import code_setup.app_util.*
import code_setup.app_util.callback_iface.OnBottomDialogItemListener
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.net_.NetworkCodes
import code_setup.net_.NetworkRequest
import code_setup.ui_.home.views.HomeActivity
import code_setup.ui_.settings.adapter_.ReviewsAdapter
import code_setup.ui_.settings.di_settings.DaggerSettingsComponent
import code_setup.ui_.settings.di_settings.SettingsModule
import code_setup.ui_.settings.settings_mvp.SettingsPresenter
import code_setup.ui_.settings.settings_mvp.SettingsView
import code_setup.ui_.settings.views.profile.ComplimentsAdapter
import com.base.mvp.BasePresenter
import com.electrovese.setup.R
import com.github.drjacky.imagepicker.ImagePicker
import com.google.gson.Gson
import kotlinx.android.synthetic.main.default_loading.*
import kotlinx.android.synthetic.main.layout_profile_fragment.*
import kotlinx.android.synthetic.main.profile_ratings_view.*
import kotlinx.android.synthetic.main.profile_top_view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.File
import javax.inject.Inject
import kotlin.collections.ArrayList


class ProfileActivity : CoreActivity(), SettingsView {
    lateinit var reviewAapter: ReviewsAdapter
    lateinit var compAapter: ComplimentsAdapter
    var TAG: String = ProfileActivity::class.java.simpleName
    override fun onResponse(list: Any, int: Int) {
        Log.e(TAG, "" + Gson().toJson(list))
        try {
            avd!!.stop()
        } catch (e: Exception) {
        }
        progress_loading.visibility = View.GONE
        when (int) {
            NetworkRequest.REQUEST_PROFILE_CODE -> {
                var responseData = list as ProfileResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    setupsideMenuDetail(responseData.response_obj)
                    if (responseData.response_obj.compliments != null && responseData.response_obj.compliments.isNotEmpty()) {
                        compAapter.updateAll(responseData.response_obj.compliments)
                        noComplementTxt.visibility=View.GONE
                        compementsRecyclar.visibility=View.VISIBLE
                    }else{
                        noComplementTxt.visibility=View.VISIBLE
                        compementsRecyclar.visibility=View.GONE
                    }
                    if (responseData.response_obj.reviews.isNotEmpty()) {
                        reviewAapter.updateAll(responseData.response_obj.reviews)
                        reviewRecyclar.visibility=View.VISIBLE
                        noReviewTxt.visibility = View.GONE
                    } else {
                        reviewRecyclar.visibility=View.GONE
                        noReviewTxt.visibility = View.VISIBLE
                    }
                }
            }
            NetworkRequest.REQUEST_UPDATE_PROFILE -> {
                var responseData = list as ProfileUpdateResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    var userData = getUserData() as LoginResponseModel.ResponseObj
                    userData.user_image = responseData.response_obj.user_image
                    Prefs.putString(CommonValues.USER_DATA, Gson().toJson(userData))
                    setupbasicDetail(userData)
                    refreshHomeData()
                }
            }
            NetworkRequest.REQUEST_CURRENT_STATUS -> {
                var responseData = list as CurrentStatusResponseModel
                if (responseData.response_code == NetworkCodes.SUCCEES.nCodes) {
                    // if  new request received
                    if (responseData.response_obj != null && responseData.response_obj.pending_job != null) {
                        showNewRequestDialog(responseData.response_obj.pending_job, false)
                    }
                } else {
                    AppUtils.showSnackBar(this, getString(R.string.error_session_expired))
                    logoutUserNow()
                }

            }
        }
    }

    private fun refreshHomeData() {
        if (HomeActivity.homeInstance != null) {
            HomeActivity.homeInstance.setupsideMenuDetail()
        }
    }


    override fun showProgress() {
        profileLoaderView.bringToFront()
        profileLoaderView.show()
    }

    override fun hideProgress() {
        profileLoaderView.hide()
        avd!!.stop()
        progress_loading.visibility = View.GONE
    }

    override fun noResult() {
    }

//    lateinit var selectedImageUri: String
    var avd: AnimatedVectorDrawable? = null
    lateinit var action: Runnable

    @Inject
    lateinit var presenter: SettingsPresenter

    override fun onActivityInject() {
        DaggerSettingsComponent.builder().appComponent(getAppcomponent())
            .settingsModule(SettingsModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.layout_profile_fragment
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AnimUtils.moveAnimationX(profileBackBtn, false)
        AnimUtils.moveAnimationX(profileBackBtn, true)
        profileBackBtn.setOnClickListener {
            onBackPressed()
        }
        initAdapter()
        setupbasicDetail(getUserData() as LoginResponseModel.ResponseObj)
        Handler().post(Runnable {
            getUserProfileData()
        })
        userImageEdit.setOnClickListener {
            if (Build.VERSION.SDK_INT < 23) {
                //Do not need to check the permission
                openPickerAlert()
            } else {
                if (checkAndRequestPermissions()) {
                    //If you have already permitted the permission
                    openPickerAlert()
                }
            }
        }
    }

    private fun openPickerAlert() {
        if (!AppUtils.checkNetwork()) {
            AppUtils.showToast(getString(R.string.no_internet_connection))
            return
        }
        val name = arrayOf("TAKE A PHOTO", "CHOOSE FROM GALLERY")
        val icons =
            intArrayOf(R.drawable.ic_menu_camera, R.drawable.ic_menu_gallery)
        AppDialogs.openDialog(
            this,
            name,
            icons,
            object : OnBottomDialogItemListener<Any> {
                override fun onItemClick(view: View, position: Int, type: Int, t: Any) {
                    when (t as String) {

                        "TAKE A PHOTO" -> {
                            openCam()
                        }
                        "CHOOSE FROM GALLERY" -> {
                            pickFromGallry()
                        }
                    }
                }
            })
    }

    private fun pickFromGallry() {
       /* ImagePicker.with(this)
            // Crop Image(User can choose Aspect Ratio)
            .crop()
            // User can only select image from Gallery
            .galleryOnly()

            .galleryMimeTypes(  //no gif images at all
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            // Image resolution will be less than 1080 x 1920
            .maxResultSize(1080, 1920).compress(1024)
            .start(102)*/
        galleryLauncher.launch(
            ImagePicker.with(this)
                .crop()
                .galleryOnly()
                .galleryMimeTypes( // no gif images at all
                    mimeTypes = arrayOf(
                        "image/png",
                        "image/jpg",
                        "image/jpeg"
                    )
                )
                .createIntent()
        )
    }
    var selectedImageUri: String? = ""
    private var mCameraFile: File? = null
    private var mGalleryFile: File? = null
    private fun openCam() {
       /* ImagePicker.with(this@ProfileActivity)
            // User can only capture image from Camera
            .cameraOnly()
            // Image size will be less than 1024 KB
            .compress(1024)
            .saveDir(Environment.getExternalStorageDirectory())
//            .saveDir(Environment.getExternalStorageDirectory().absolutePath + File.separator + "ImagePicker")
//            .saveDir(getExternalFilesDir(null)!!)
            .start(103)*/

        cameraLauncher.launch(
            ImagePicker.with(this)
                .crop()
                .cameraOnly()
                .maxResultSize(1080, 1920, true)
                .createIntent()
        )
    }
    private val galleryLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val file = ImagePicker.getFile(it.data)!!
                mGalleryFile = file
                userImageView.setImageURI(file.absolutePath)
                selectedImageUri = file.absolutePath
                uploadImageUpdates()
            } else parseError(it)
        }
    private val cameraLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                val uri = it.data?.data!!
                val file = ImagePicker.getFile(it.data)!!
                mCameraFile = file
                userImageView.setImageURI(file.absolutePath)
                selectedImageUri = file.absolutePath
                uploadImageUpdates()
            } else parseError(it)
        }
    private fun parseError(activityResult: ActivityResult) {
        if (activityResult.resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(activityResult.data), Toast.LENGTH_SHORT)
                .show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }



    private fun uploadImageUpdates() {
        presenter.updtaeProfileImage(NetworkRequest.REQUEST_UPDATE_PROFILE, getRequiredData())
    }

    private fun getRequiredData(): MultipartBody.Part {
        var file = File(selectedImageUri)
        // Create a request body with file and image media type
        // Create a request body with file and image media type
        val fileReqBody: RequestBody =
            RequestBody.create(MediaType.parse("multipart/form-data"), file)
        // Create MultipartBody.Part using file request-body,file name and part name
        // Create MultipartBody.Part using file request-body,file name and part name
        val part = MultipartBody.Part.createFormData("user_image", file.getName(), fileReqBody)
        //Create request body with text description and text media type
        //Create request body with text description and text media type
        val description = RequestBody.create(MediaType.parse("text/plain"), "image-type")

        return part
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun getUserProfileData() {
        try {
            action = Runnable { repeatAnimation() }
            avd = iv_line.getBackground() as AnimatedVectorDrawable
            avd!!.registerAnimationCallback(object : Animatable2.AnimationCallback() {
                override fun onAnimationEnd(drawable: Drawable) {
                    avd = iv_line.getBackground() as AnimatedVectorDrawable
                    avd!!.start()
                }
            })
            avd!!.start()
        } catch (e: Exception) {
        }
        presenter.getProfileData(NetworkRequest.REQUEST_PROFILE_CODE)
    }

    private fun repeatAnimation() {
        avd!!.start()
        iv_line.postDelayed(action, 5000) // Will repeat animation in every 1 second
    }

    private fun setupbasicDetail(userData: LoginResponseModel.ResponseObj) {
        if (userData != null) {
//            var userData = getUserData() as LoginResponseModel.ResponseObj
            userNameVw.setText(userData.name)
            usermNumberVw.setText("" +userData.contact_string)
            userEmailVw.setText(userData.email)
            userImageView.setImageURI(userData.user_image)

            /*  try {
                  profileEducationText.setText(userData.education)
                  profileOverviewText.setText(userData.overview)
                  if (userData.languages != null && userData.languages.size > 0) {
                      if (userData.languages.size == 1)
                          profileLanguageText.setText(
                              getString(R.string.str_speaks) + " " + userData.languages.get(
                                  0
                              )
                          )
                      else if (userData.languages.size == 2) {
                          profileLanguageText.setText(
                              getString(R.string.str_speaks) + " " + userData.languages.get(0) + " " + getString(
                                  R.string.str_and
                              ) + " " + userData.languages.get(1)
                          )
                      } else {
                          var bldr = StringBuilder()
                          for (i in 0 until userData.languages.size) {
                              bldr.append(userData.languages.get(i) + ",")
                          }
                          profileLanguageText.setText(
                              getString(R.string.str_speaks) + bldr
                          )
                      }
                  }
              } catch (e: Exception) {
                  e.printStackTrace()
              }*/
        }
    }

    private fun setupsideMenuDetail(userData: ProfileResponseModel.ResponseObj) {
        if (userData != null) {
//            var userData = getUserData() as LoginResponseModel.ResponseObj
            userNameVw.setText(userData.name)
            usermNumberVw.setText("" + userData.country_code + " " + userData.contact)
            userEmailVw.setText(userData.email)
            userImageView.setImageURI(userData.user_image)
            userCountryNameVw.setText("Provider: " + userData.provider_name)
            try {
                totalRatingsTxt.setText(userData.rating)
            } catch (e: Exception) {
            }
            try {
                totalReviewsCountTxt.setText("" + userData.total_reviews)
            } catch (e: Exception) {
            }
            try {
                totalTripsCountTxt.setText("" + userData.total_trips)
            } catch (e: Exception) {
            }
            try {
                profileEducationText.setText(userData.education)
                profileOverviewText.setText(userData.overview)
                if (userData.languages != null && userData.languages.size > 0) {
                    if (userData.languages.size == 1)
                        profileLanguageText.setText(
                            getString(R.string.str_speaks) + " " + userData.languages.get(
                                0
                            )
                        )
                    else if (userData.languages.size == 2) {
                        profileLanguageText.setText(
                            getString(R.string.str_speaks) + " " + userData.languages.get(0) + " " + getString(
                                R.string.str_and
                            ) + " " + userData.languages.get(1)
                        )
                    } else {
                        var bldr = StringBuilder()
                        for (i in 0 until userData.languages.size) {
                            bldr.append(userData.languages.get(i) + ",")
                        }
                        try {
                            profileLanguageText.setText(
                                getString(R.string.str_speaks) + " " + AppUtils.removeLastCharOptional(
                                    bldr.toString()
                                )
                            )
                        } catch (e: Exception) {
                            profileLanguageText.setText(
                                getString(R.string.str_speaks) + " " + bldr.toString()
                            )

                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun initAdapter() {
        with(compementsRecyclar) {
            compAapter =
                ComplimentsAdapter(
                    this@ProfileActivity,
                    ArrayList(),
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                        }
                    })
            layoutManager = LinearLayoutManager(this@ProfileActivity, HORIZONTAL,false)
            adapter = compAapter
        }
        with(reviewRecyclar) {
            reviewAapter =
                ReviewsAdapter(
                    this@ProfileActivity,
                    ArrayList(),
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {

                        }
                    })
            layoutManager = LinearLayoutManager(this@ProfileActivity, VERTICAL, false)
            adapter = reviewAapter
        }
    }

    override fun onResume() {
        super.onResume()

    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: kotlin.IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            CommonValues.REQUEST_CODE_PERMISSIONS_CAMERA -> {
                Log.e(
                    "onRequestPermissionsResult :: ",
                    "User permissions granted" + "  " + Gson().toJson(grantResults)
                )
                if (grantResults.get(0) == 0)
                    openPickerAlert()
                else AppUtils.showSnackBar(this, getString(R.string.str_camers_permission_required))
            }
        }
    }


   /* override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (resultCode == Activity.RESULT_OK) {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                Log.e("TAG", "Path:${ImagePicker.getFilePath(data)}")
                // File object will not be null for RESULT_OK
                val file = ImagePicker.getFile(data)!!
                when (requestCode) {
                    102 -> {
                        selectedImageUri = file.absolutePath
                        uploadImageUpdates()
                        userImageView.setImageURI(fileUri)
                    }
                    103 -> {
                        selectedImageUri = file.absolutePath
                        uploadImageUpdates()
                        userImageView.setImageURI(fileUri)
                    }
                }

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            } else {
//                Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
//            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }
    }*/
    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    fun onMessage(event: CustomEvent<Any>) {
        Log.d("onMessage", " HOME SCREEN " + event.type)
        try {
            Log.d("onMessage", " HOME SCREEN " + event.oj.toString())
        } catch (e: Exception) {
        }
        when (event.type) {
            EVENTS.REQUEST_NEW_TOUR_JOB -> {// In case of push notification
                /* var dataBody = Gson().fromJson<NotificationDataBodyModel>(
                     (event.oj as NotificationModel).data.toString(),
                     NotificationDataBodyModel::class.java
                 )
                 showNewRequestDialog(dataBody, false)*/
                getCurrentStatus()
            }
        }
    }
    private fun getCurrentStatus() {
        presenter.getCurrentStatus(NetworkRequest.REQUEST_CURRENT_STATUS)
    }
}