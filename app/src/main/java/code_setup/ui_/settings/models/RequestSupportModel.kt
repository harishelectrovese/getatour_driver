package code_setup.ui_.settings.models

data class RequestSupportModel(
    var app_version: String,
    var concern: String,
    var lat: Double,
    var lng: Double,
    var message: String
)