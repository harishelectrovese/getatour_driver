package code_setup.ui_.settings.models

data class RejectedRidesResponseModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: List<ResponseObj>
) {
    data class ResponseObj(
        var _id: String,
        var amount: Amount,
        var booking: String,
        var booking_date: String,
        var end_loc: EndLoc,
        var is_ride:Boolean,
        var id: String,
        var routes: List<Route>,
        var seats: Int,
        var start_loc: StartLoc,
        var user_name: String,
        var tour_name: String,
        var tour_image: String
    ) {
        data class Amount(
            var conversion_rate: String,
            var currency_shortcode: String,
            var currency_symbol: String,
            var rejected_fee: String,
            var discount: String,
            var subtotal: String,
            var total: String
        )

        data class EndLoc(
            var coordinates: List<Double>,
            var name: String,
            var type: String
        )

        data class Route(
            var _id: String,
            var lat: Double,
            var lng: Double,
            var name: String,
            var waiting: String
        )

        data class StartLoc(
            var coordinates: List<Double>,
            var name: String,
            var type: String
        )
    }
}