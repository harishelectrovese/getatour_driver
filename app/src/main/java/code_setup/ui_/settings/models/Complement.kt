package code_setup.ui_.settings.models

public data class Compliment(
    var _id: String,
    var image: String,
    var title: String,
    var total: Int
)