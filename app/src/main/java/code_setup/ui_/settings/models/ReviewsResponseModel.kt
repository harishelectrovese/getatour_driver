package code_setup.ui_.settings.models


data class ReviewsResponseModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: ResponseObj
) {
    data class ResponseObj(
        var _id: String,
        var compliment_total: Int,
        var compliments: List<Compliment>,
        var driver_id: String,
        var rating: Double,
        var review: List<Review>,
        var total_reviews: Int,
        var total_trips: String
    ) {
        /*data class Compliment(
            var _id: String,
            var image: String,
            var title: String,
            var total: Int
        )*/

        data class Review(
            var _id: String,
            var createdAt: String,
            var image: String,
            var name: String,
            var rating: Float,
            var type_category: String,
            var review: String,
            var user: User
        ) {
            data class User(
                var image: String,
                var name: String
            )
        }
    }
}