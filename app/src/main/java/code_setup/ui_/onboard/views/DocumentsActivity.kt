package code_setup.ui_.onboard.views

import android.os.Bundle
import android.view.View
import com.base.mvp.BasePresenter
import code_setup.app_core.CoreActivity
import code_setup.ui_.home.views.HomeActivity
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.activity_document_layout.*
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import kotlinx.android.synthetic.main.user_documents_layout.*

class DocumentsActivity : CoreActivity() {
    override fun onActivityInject() {
    }

    override fun onError() {
    }

    override fun setPresenter(presenter: BasePresenter<*>) {
    }

    override fun getScreenUi(): Int {
        return R.layout.activity_document_layout
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        txtTitletoolbar.setText(R.string.str_upload_documents)
        val function: (View) -> Unit = {
            onBackPressed()
        }
        backToolbar.setOnClickListener(function)
        continueButton.setSafeOnClickListener {
            activitySwitcher(
                this,
                HomeActivity::class.java,
                null
            )
            finishAffinity()
        }

        drivingLicenseArrowBtn.setOnClickListener {
            activitySwitcher(
                this,
                UploadDocumentActivity::class.java,
                null
            )
        }
        policeVerifArrowBtn.setOnClickListener {
            activitySwitcher(
                this,
                UploadDocumentActivity::class.java,
                null
            )
        }
        vehicleRegiArrowBtn.setOnClickListener {
            activitySwitcher(
                this,
                UploadDocumentActivity::class.java,
                null
            )
        }
        vehiclePermitArrowBtn.setOnClickListener {
            activitySwitcher(
                this,
                UploadDocumentActivity::class.java,
                null
            )
        }
    }

    override fun onResume() {

        super.onResume()

    }

    private fun moveNext() {

    }

}