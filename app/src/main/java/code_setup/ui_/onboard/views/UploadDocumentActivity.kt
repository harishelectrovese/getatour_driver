package code_setup.ui_.onboard.views

import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageView
import androidx.annotation.RequiresApi
import code_setup.app_core.CoreActivity
import code_setup.ui_.auth.di_auth.DaggerOnboardActivityComponent
import code_setup.ui_.onboard.di_onboard.OnboardActivityModule
import code_setup.ui_.auth.views.fragments.LandingActivity
import code_setup.ui_.onboard.adapter_.TourTypeAdapter
import com.base.mvp.BasePresenter
import com.burakeregar.githubsearch.home.presenter.OnboardPresenter
import com.burakeregar.githubsearch.home.presenter.OnboardView
import com.electrovese.setup.R
import kotlinx.android.synthetic.main.activity_upload_document_layout.*
import kotlinx.android.synthetic.main.common_toolbar_lay.*
import javax.inject.Inject

class UploadDocumentActivity : CoreActivity(), OnboardView {
    override fun onResponse(list: Any, int: Int) {

    }

    override fun showProgress() {

    }

    override fun hideProgress() {

    }

    override fun noResult() {

    }

    private lateinit var tAdapter: TourTypeAdapter

    override fun onActivityInject() {
        DaggerOnboardActivityComponent.builder().appComponent(getAppcomponent())
            .onboardActivityModule(OnboardActivityModule())
            .build()
            .inject(this)
        presenter.attachView(this)
    }

    override fun onError() {
    }

    @Inject
    lateinit var presenter: OnboardPresenter

    override fun setPresenter(presenter: BasePresenter<*>) {

    }

    override fun getScreenUi(): Int {
        return R.layout.activity_upload_document_layout
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        txtTitletoolbar.setText(R.string.str_upload_documents)
        backToolbar.setOnClickListener {
            onBackPressed()
        }
        selectImage.setOnClickListener {
            openCamera()
        }
    }

    private fun openCamera() {

    }


    override fun onResume() {
        super.onResume()
//        moveNext()
    }

    private fun moveNext() {
        Handler().postDelayed({
            activitySwitcher(
                this,
                LandingActivity::class.java,
                null
            )

        }, 5000)
    }

}