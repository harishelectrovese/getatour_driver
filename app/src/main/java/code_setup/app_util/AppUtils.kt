package code_setup.app_util

import android.annotation.SuppressLint
import android.app.Activity
import android.app.NotificationManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Vibrator
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import code_setup.app_core.BaseApplication
import code_setup.app_models.response_.CaptureInfoModel
import code_setup.net_.NetworkConstant
import code_setup.ui_.home.models.EarningData
import com.electrovese.setup.BuildConfig
import com.electrovese.setup.R
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import java.security.AccessController.getContext
import java.util.*
import kotlin.collections.ArrayList


class AppUtils {
    companion object {


        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun showSnackBar(context: Activity, stringMessage: String) {
            val snack: Snackbar = Snackbar.make(
                context.findViewById(android.R.id.content),
                stringMessage,
                Snackbar.LENGTH_LONG
            )
//            SnackbarHelper.configSnackbar(this, snack);
            snack.config(context = context)// if you're using Kotlin
            snack.show()
        }

        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
        fun showSnackBarConnectiivity(context: Activity, stringMessage: String, staus: Boolean) {
            var snack: Snackbar
//            SnackbarHelper.configSnackbar(this, snack);
            if (!staus) {
                snack = Snackbar.make(
                    context.findViewById(android.R.id.content),
                    stringMessage,
                    Snackbar.LENGTH_INDEFINITE
                )
            } else {
                snack = Snackbar.make(
                    context.findViewById(android.R.id.content),
                    stringMessage,
                    Snackbar.LENGTH_SHORT
                )
            }
            snack.configForNetwork(context = context, status = staus)// if you're using Kotlin
            snack.show()
        }

        fun isValidEmail(email: String): Boolean {
            return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches()
        }


        fun showToast(stringMsg: String) {
            Toast.makeText(BaseApplication.instance, stringMsg, Toast.LENGTH_SHORT).show()
        }

        fun spannableText(viewT: TextView, text: String) {
//        val termConditionTxt = getClickableSpan(R.color.colorAccent, null, termConditionTxt)


            var sText: SpannableString =
                SpannableString("You Have " + text + " Seconds to Reject This ride. After timeout it will be Auto Accepted")
            var clickableSpan = getClickableSpan(sText)
            sText.setSpan(clickableSpan, 66, 79, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            viewT.setText(sText)
            viewT.setMovementMethod(LinkMovementMethod.getInstance())
            viewT.setHighlightColor(Color.BLACK)

        }

        fun getClickableSpan(sText: SpannableString): ClickableSpan {
            return object : ClickableSpan() {
                override fun onClick(view: View) {

                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.setUnderlineText(false) // set to false to remove underline
                    ds.color = Color.parseColor("#2196F3")
                }
            }
        }

        fun hideKeyboard(loginButton: View?) {
            val imm =
                BaseApplication.instance.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(loginButton!!.windowToken, 0)
        }

        fun getCaptureInfoData(): CaptureInfoModel {
            return CaptureInfoModel(
                AppUtils.getDeviceid(),
                CommonValues.DEVICE_OS,
                BuildConfig.VERSION_NAME,
                Build.VERSION.RELEASE,
                Prefs.getString(CommonValues.FCM_TOKEN, "")
            )
        }

        @SuppressLint("MissingPermission")
        fun getDeviceid(): String {
            var androidId = Prefs.getString(CommonValues.DEVICE_ID, "fgj")
            Log.d("ANDROID ID ", " == " + androidId)
            return androidId!!
        }

        fun getTimeZoneWithOffset(): String {

            val tz = TimeZone.getDefault()
            val now = Date();
//Import part : x.0 for double number
            val offsetFromUtc = tz.getOffset(now.getTime()) / 3600000.0
            val m2tTimeZoneIs = offsetFromUtc.toString()

            return m2tTimeZoneIs
        }

        fun getMyLocation(): LatLng? {
            return LatLng(
                Prefs.getDouble(CommonValues.LATITUDE, 0.0),
                Prefs.getDouble(CommonValues.LONGITUDE, 0.0)
            )
        }

        /*
        * Remove notification from id
        * */
        fun clearNotification(ctx: Activity, notificationId: Int) {
            var notificationManager =
                ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancel(notificationId);
        }

        fun removeLastCharOptional(str: String): String {
            return str.substring(0, str.length - 1);
        }

        fun getNameFromTitle(title: String?): String {
            try {
                return title!!.split("From")[1]
            } catch (e: Exception) {
                return ""
            }
        }


        fun emailIntent(context: AppCompatActivity, msg: String) {

            val selectorIntent = Intent(Intent.ACTION_SENDTO)
            selectorIntent.setData(Uri.parse("mailto:"))

            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.putExtra(
                Intent.EXTRA_EMAIL,
                arrayOf(context.getString(R.string.support_email))
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Need your Help ")
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            emailIntent.setSelector(selectorIntent)

            try {
                context.startActivity(
                    Intent.createChooser(
                        emailIntent,
                        context.getString(R.string.send_email_to_us)
                    )
                )
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(
                    context,
                    context.getString(R.string.share_no_intent_handler_found),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        fun getDefaultyAxis(): ArrayList<EarningData> {
            val arrayList = ArrayList<EarningData>()
            for (i in 0 until 8) {
                arrayList.add(EarningData("" + (i * 100), ""))
            }

            return arrayList.reversed() as ArrayList<EarningData>
        }

        fun getArrayList(earnings: Int, earnings1: Int, stepSize: Int): ArrayList<String> {
//            splitIntoParts(earnings, 8)

            return otherRange(earnings, earnings1, stepSize)
        }

        private fun splitIntoParts(whole: Int, parts: Int): IntArray? {
            val arr = IntArray(parts)
            var remain = whole
            var partsLeft = parts
            var i = 0
            while (partsLeft > 0) {
                val size = (remain + partsLeft - 1) / partsLeft // rounded up, aka ceiling
                arr[i] = size
                remain -= size
                partsLeft--
                i++
            }
            Log.e("splitIntoParts ", " " + Gson().toJson(arr))

            return arr
        }

        fun otherRange(start: Int, end: Int, step: Int): ArrayList<String> {
            Log.e("otherRange ", " " + start + " " + end + " " + step)
            var newEnd = try {
                end + step
            } catch (e: Exception) {
                end
            }
            var otherArray = ArrayList<String>()
            /*  if (step == undefined) {
                  step = 1;
              };*/
            if (step > 0) {
                run {
                    var i = start
                    while (i <= newEnd) {
                        otherArray.add("" + i)
                        i += step
                    }
                }
            } else {
                run {
                    var i = start
                    while (i >= newEnd) {
                        otherArray.add("" + i)
                        i += step
                    }
                }
            };
            Log.e("otherRange ", " " + Gson().toJson(otherArray))
            return otherArray;
        };

        fun checkNetwork(): Boolean {
            return BaseApplication.recentNetworkStatus
        }

        //        private var mediaPlayer: MediaPlayer? = null
        fun stopVibration(ctx: Activity) {
            try {
//                var vibrator = ctx.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
//                vibrator.cancel()
                try {
//                    MusicControl.getInstance(ctx)!!.stopMusic()
                    MyMediaPlayer.getInstance(BaseApplication.instance)!!.stopSound()
                } catch (e: Exception) {
                }
                /*try {
                    if (BaseApplication.mediaPlayer != null) {
                        if (BaseApplication.mediaPlayer!!.isPlaying()) BaseApplication.mediaPlayer!!.stop()
                        BaseApplication.mediaPlayer!!.release()
                        BaseApplication.mediaPlayer!!.stop()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }*/
            } catch (e: Exception) {
            }
        }


        fun pxToDp(px: Int): Int {
            val displayMetrics: DisplayMetrics = BaseApplication.instance.getResources().getDisplayMetrics()
            return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).toInt()
        }

        fun dpToPx(dp: Int): Int {
            val displayMetrics: DisplayMetrics = BaseApplication.instance.getResources().getDisplayMetrics()
            return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).toInt()
        }


        fun openWebIntent(homeActivity: Context) {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(NetworkConstant.BOOKING_LINK)
            homeActivity.startActivity(i)
        }

        fun openWebIntentGuide(homeActivity: Context) {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(NetworkConstant.GUIDE_LINK)
            homeActivity.startActivity(i)
        }
    }

}
