package code_setup.app_util

import android.app.Activity
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Build
import android.os.CountDownTimer
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import code_setup.app_models.other_.event.EVENTS
import code_setup.app_models.response_.CurrentStatusResponseModel
import code_setup.app_models.response_.TourDetailResponseModel
import code_setup.app_models.response_.UpdateStatusResponseModel
import code_setup.app_util.callback_iface.OnItemClickListener
import code_setup.app_util.views_.OpenBottonDialogAdapter
import code_setup.db_.AppDatabase
import code_setup.ui_.home.apapter_.TourListAdapter
import code_setup.ui_.home.apapter_.TourMembersAdapter
import code_setup.ui_.home.models.ToursListResponseModel
import code_setup.ui_.home.views.chat_.ChatScreen
import code_setup.ui_.home.views.schedule.ScheduleDayActivity
import com.electrovese.setup.BuildConfig
import com.electrovese.setup.R
import com.facebook.drawee.view.SimpleDraweeView
import com.rd.PageIndicatorView
import java.text.DecimalFormat
import kotlin.collections.ArrayList
import code_setup.app_util.callback_iface.OnBottomDialogItemListener as OnBottomDialogItemListener1


class AppDialogs {
    companion object {


        /*var mCompositeDisposable: CompositeDisposable? = null
        var mBottomSheetDialog: Dialog? = null
        var selectedDate: String? = null
        private fun getDateWiseCases(
            selectedDate: String,
            mBottomSheetDialog: Dialog
        ) {
            this.mBottomSheetDialog = mBottomSheetDialog
            this.selectedDate = selectedDate
            mCompositeDisposable = CompositeDisposable()
            val apiService = RestInitAPI.RedditApiWithHeader.create()
            mCompositeDisposable?.add(
                apiService.getActiveCases(
                    AppUtils.getCommonHeaders(), RequestCasesModel(selectedDate, "")
                )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleSuccessUpdate, this::handleFaliurUpdate)
            )

        }

        fun handleSuccessUpdate(baseResponse: CaseResponseModel) {
            var bResponse = baseResponse
            if (bResponse.response_code == NetworkConstant.SUCCESS) {
                if (bResponse.response_obj != null) {
                    if (mBottomSheetDialog != null) {
                        mBottomSheetDialog!!.dismiss()
                    }
                    bResponse.response_obj.selectedDate = this.selectedDate.toString()
                    EventBus.getDefault().postSticky(CustomEvent<Any>(EVENTS.DATE_WISE_CASE_RECEIVED, bResponse))
                } else {

                }
            } else {
                AppUtils.showToast(bResponse.response_message)
            }
        }
        fun handleFaliurUpdate(error: Throwable) {
        }
*/

        //        lateinit var dataBody: CurrentStatusResponseModel.ResponseObj.PendingJob
        fun openNewRequestDialog(
            activity: Activity,
            mData: Any,
            isFromNotificationClick: Boolean,
            listener: OnBottomDialogItemListener1<Any>
        ): Dialog {
            Log.e("openNewRequestDialog ", "-----------> ")
            val mBottomSheetDialog = Dialog(activity)
            val child =
                activity.layoutInflater.inflate(R.layout.layout_view_new_request_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            var dataBody = mData as CurrentStatusResponseModel.ResponseObj.PendingJob

            val spannableMessageText: TextView = child.findViewById(R.id.spannableMessageText)
            AppUtils.spannableText(spannableMessageText, "60" /*+ dataBody.time_left*/)

            val pickUpLocationNewDialog: TextView = child.findViewById(R.id.pickUpLocationNewDialog)
            val dropLocationNewDialog: TextView = child.findViewById(R.id.dropLocationNewDialog)
            val tourDateTimeNewDialog: TextView = child.findViewById(R.id.tourDateTimeNewDialog)
            val tourDestinationsNewDialog: TextView =
                child.findViewById(R.id.tourDestinationsNewDialog)
            val userNameNewDialog: TextView = child.findViewById(R.id.userNameNewDialog)
            val userImageNewDialog: SimpleDraweeView = child.findViewById(R.id.userImageNewDialog)
            val userRatingTxt: TextView = child.findViewById(R.id.userRatingTxt)
            val tourPriceNewDialog: TextView = child.findViewById(R.id.tourPriceNewDialog)
            val countDownTimerTxtDialog: TextView = child.findViewById(R.id.countDownTimerTxtDialog)
            val tourNameNewDialog: TextView = child.findViewById(R.id.tourNameNewDialog)
            val tourNameHolderview: LinearLayout = child.findViewById(R.id.tourNameHolderDialog)

            if (mData != null && !mData.equals("")) {
//
//                dataBody = Gson().fromJson<NotificationDataBodyModel>(
//                    nData.data.toString(),
//                    NotificationDataBodyModel::class.java
//                )
                if (dataBody != null) {
                    tourNameNewDialog.setText(dataBody.data.tour_name)
                    pickUpLocationNewDialog.setText(dataBody.data.pickup_loc)
                    dropLocationNewDialog.setText(dataBody.data.drop_off)
                    tourDateTimeNewDialog.setText(dataBody.data.booking_date)
                    tourDestinationsNewDialog.setText(dataBody.data.drop_off)
                    tourPriceNewDialog.setText(dataBody.data.currency_symbol + " " + dataBody.data.price)

                    userNameNewDialog.setText(dataBody.data.user_name)
//                  Log.e(" NEW REQUEST "," "+dataBody.data.user_image)
                    userImageNewDialog.setImageURI(dataBody.data.user_image)
                    userRatingTxt.setText(dataBody.data.rating)
                    if (dataBody.data.is_ride) {
                        tourNameHolderview.visibility = View.GONE
                    } else tourNameHolderview.visibility = View.VISIBLE
                }
                var remainingTime: Long = 60000
                try {
                    remainingTime = dataBody.time_left.toLong() * 1000
                } catch (e: Exception) {
                }

                if (isFromNotificationClick) {
                    try {
                        var date1 = AppDatabase.getAppDatabase(activity).notificationDao()
                            .findById(dataBody.data.notificatiobId!!).notifictaionTime
                        var date2 = DateUtilizer.getCurrentDate().time
                        remainingTime = DateUtilizer.differenceBetweenDates(date2, date1!!)
                        Log.e("TIME DIFFRENCE ", " difference " + remainingTime)
                        remainingTime *= 1000
                        Log.e("TIME DIFFRENCE ", " remaining " + remainingTime)
                        AppDatabase.getAppDatabase(activity).notificationDao()
                            .deleteById(dataBody.data.notificatiobId!!)
                        AppUtils.clearNotification(activity, dataBody.data.notificatiobId!!.toInt())
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                object : CountDownTimer(remainingTime/*dataBody.expiration_date*/, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        try {
                            val f = DecimalFormat("00")
                            val sec = millisUntilFinished / 1000 % 60
                            countDownTimerTxtDialog.setText("" + f.format(sec) /*+ " sec"*/)
                        } catch (e: Exception) {
                        }
//                        counter++
                    }

                    override fun onFinish() {
                        try {
                            countDownTimerTxtDialog.setText("00")
                            Handler().postDelayed(Runnable {
                                mBottomSheetDialog.dismiss()
                            }, 10)

                        } catch (e: Exception) {
                            mBottomSheetDialog.dismiss()
                        }
                    }
                }.start()
            }


            val acceptBtn: TextView = child.findViewById(R.id.acceptBtn)
            val rejectBtn: TextView = child.findViewById(R.id.rejectBtn)
            acceptBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(child, 0, EVENTS.ACCEPT_TOUR, dataBody)
            }
            rejectBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(child, 0, EVENTS.REJECT_TOUR, dataBody)
            }
            mBottomSheetDialog.setOnDismissListener {
                AppUtils.stopVibration(activity)
            }
            mBottomSheetDialog.show()

            return mBottomSheetDialog
        }

        fun openDialogGoOfflineAlert(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity)
            val child = activity.layoutInflater.inflate(R.layout.view_open_scan_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val dialogTitle: TextView = child.findViewById(R.id.dialogTitle)
            val dialogMessageText: TextView = child.findViewById(R.id.dialogMessageText)
            val dialogBtn: TextView = child.findViewById(R.id.dialogOpenBtn)

            dialogTitle.setText(R.string.str_goOffline)
            dialogMessageText.setText("If you go offline No ride will be assigned to you ")
            dialogBtn.setText(R.string.str_goOffline)
            dialogBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
//            dialogBtn.setText(asf as String)
//            dialogMessageText.setText(tText as String)
            dialogBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(dialogBtn, 0, 1, Any())
            }
            mBottomSheetDialog.setOnDismissListener(object : DialogInterface.OnDismissListener {
                override fun onDismiss(arg0: DialogInterface?) { // do something
                    listener.onItemClick(dialogBtn, 0, 0, Any())
                }
            })
            mBottomSheetDialog.show()
        }


        fun openDialogScaneAlert(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity)
            val child = activity.layoutInflater.inflate(R.layout.view_open_scan_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val dialogMessageText: TextView = child.findViewById(R.id.dialogMessageText)
            val dialogBtn: TextView = child.findViewById(R.id.dialogOpenBtn)
//            dialogBtn.setText(asf as String)
//            dialogMessageText.setText(tText as String)
            dialogBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(dialogBtn, 0, 1, Any())
            }
            mBottomSheetDialog.setOnDismissListener(object : DialogInterface.OnDismissListener {
                override fun onDismiss(arg0: DialogInterface?) { // do something
                    listener.onItemClick(dialogBtn, 0, 0, Any())
                }
            })
            mBottomSheetDialog.show()
        }

        fun openDialogScaneSuccesssAlert(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity)
            val child =
                activity.layoutInflater.inflate(R.layout.view_open_scan_success_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)
            var dataModel = asf as UpdateStatusResponseModel

            val dialogBtn: TextView = child.findViewById(R.id.dialogOpenBtn)

            val vehicleNumbr: TextView = child.findViewById(R.id.vehicleNumber)
            val vehicleCapacity: TextView = child.findViewById(R.id.vehicleCapicity)
            val vehicleDataHolder: LinearLayout = child.findViewById(R.id.vehicleDataHolder)
            val guideDataHolder: LinearLayout = child.findViewById(R.id.guideDataHolder)
            val guideName: TextView = child.findViewById(R.id.guideName)

            if (dataModel.response_obj != null) {

                try {
                    Prefs.putBoolean(CommonValues.IS_GUIDE,dataModel.response_obj.is_guide)
                } catch (e: Exception) {
                }

                if (dataModel.response_obj.is_guide) {
                    try {
                        guideDataHolder.visibility = View.VISIBLE
                        vehicleDataHolder.visibility = View.GONE
                        guideName.setText("" + dataModel.response_obj.vehicle_number)
                    } catch (e: Exception) {
                    }

                } else {
                    guideDataHolder.visibility = View.GONE
                    vehicleDataHolder.visibility = View.VISIBLE
                    vehicleNumbr.setText("" + dataModel.response_obj.vehicle_number)
                    vehicleCapacity.setText("" + dataModel.response_obj.capacity + " Person")
                    if (dataModel.response_obj.vehicle_number.equals("N/A")) {
                        dialogBtn.setText(R.string.str_vehicle_not_available)
                        dialogBtn.setBackgroundResource(R.drawable.rectangle_background_red)
                    }
                    try {
                        Prefs.putString(CommonValues.ACTIVE_VEHICLE_NUMBER,dataModel.response_obj.vehicle_number)
                    } catch (e: Exception) {
                    }
                }
            }

            dialogBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                if (dialogBtn.text.toString()
                        .equals(activity.getString(R.string.str_vehicle_not_available))
                ) {
                    listener.onItemClick(dialogBtn, 0, 0, Any())
                } else {
                    listener.onItemClick(dialogBtn, 0, 1, Any())
                }

            }
            mBottomSheetDialog.show()
        }


        fun openDialogPeopleAlert(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child =
                activity.layoutInflater.inflate(R.layout.view_open_tour_people_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)


            val dialogCloseBtn: ImageView = child.findViewById(R.id.closeDialog)
            val tourMembersRecyclar: RecyclerView = child.findViewById(R.id.tourMembers)
//            val memberMessageImg: ImageView = child.findViewById(R.id.memberMessageImg)
//            memberMessageImg.visibility = View.GONE
            var rData = asf as TourDetailResponseModel.ResponseObj
            val memberaAdapter =
                TourMembersAdapter(
                    activity as FragmentActivity,
                    ArrayList(), false,
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                            when (type) {
                                CommonValues.CALL_CLICK -> {
                                    listener.onItemClick(
                                        view,
                                        position,
                                        0,
                                        (t as TourDetailResponseModel.ResponseObj.User)
                                    )
                                }
                                CommonValues.MESSAGE_CLICK -> {
                                    /*     val intent = android.content.Intent(
                                             activity,
                                             ChatScreen::class.java
                                         )
                                         intent.putExtra(CommonValues.TOUR_ID, rData._id)
                                         intent.putExtra(
                                             CommonValues.TOUR_DATA,
                                             t as TourDetailResponseModel.ResponseObj.User
                                         )
                                         val options =
                                             androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation(
                                                 activity!!,
                                                 view!!,
                                                 view.transitionName
                                             )
                                         activity.startActivity(intent, options.toBundle())*/


                                    val bndl = Intent(activity, ChatScreen::class.java)
                                    bndl.putExtra(CommonValues.TOUR_ID, rData._id)
                                    bndl.putExtra(
                                        CommonValues.TOUR_DATA,
                                        t as TourDetailResponseModel.ResponseObj.User
                                    )
                                    activity.startActivity(bndl)
                                }
                            }
                        }
                    })

            tourMembersRecyclar.adapter = memberaAdapter
            tourMembersRecyclar.layoutManager = LinearLayoutManager(activity, VERTICAL, false)

            if (rData?.users != null && rData.users.isNotEmpty()) {
                memberaAdapter.updateAll(rData.users)
            }

            dialogCloseBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
            }
            mBottomSheetDialog.show()
        }

        fun openDialogArrivedMark(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child = activity.layoutInflater.inflate(R.layout.view_mark_arrived_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val arrivedBtnDialog: TextView = child.findViewById(R.id.arrivedBtnDialog)
            val dialogBtn: ImageView = child.findViewById(R.id.closeDialog)
            arrivedBtnDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(dialogBtn, 0, CommonValues.TOUR_DESTINATION_ARRIVED, Any())
            }

            dialogBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(dialogBtn, 0, 0, Any())
            }
            mBottomSheetDialog.show()
        }


        fun openDialogTwoButtons(
            activity: Activity,
            tText: Any,
            mText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child = activity.layoutInflater.inflate(R.layout.view_two_btns_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)
            val cancelBtnDialog: TextView = child.findViewById(R.id.cancelBtnDialog)
            val doneBtnDialog: TextView = child.findViewById(R.id.doneBtnDialog)

            val titleTxtDialog: TextView = child.findViewById(R.id.titleTxtDialog)
            val messageTextDialog: TextView = child.findViewById(R.id.messageTextDialog)
            titleTxtDialog.setText(tText as String)
            messageTextDialog.setText(mText as String)

            doneBtnDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(doneBtnDialog, 0, 1, Any())
            }

            cancelBtnDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(cancelBtnDialog, 0, 0, Any())
            }
            mBottomSheetDialog.show()
        }


        fun openDialogOneButton(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child = activity.layoutInflater.inflate(R.layout.view_one_button_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val dialogMessageText: TextView = child.findViewById(R.id.dialogMessageText)
            val dialogBtn: TextView = child.findViewById(R.id.dialogBtn)
            dialogBtn.setText(asf as String)
            dialogMessageText.setText(tText as String)
            dialogBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(dialogBtn, 0, 0, Any())
            }
            mBottomSheetDialog.show()
        }


        fun openDialogThanku(
            activity: Activity,
            aInt: Int,
            tTextList: ArrayList<String>,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, android.R.style.Theme_NoTitleBar_Fullscreen)
            val child = activity.layoutInflater.inflate(R.layout.view_full_thanku_screen, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val closeImage: ImageView = child.findViewById(R.id.closeBtnImage)
            val backLayout: LinearLayout = child.findViewById(R.id.backLayout)
            closeImage.bringToFront()
            backLayout.setOnClickListener {
                activity.finish()
                mBottomSheetDialog.dismiss()

//                listener.onItemClick(backLayout, 0, 0, Any())
            }
            closeImage.setOnClickListener {
                activity.onBackPressed()
                mBottomSheetDialog.dismiss()

//                listener.onItemClick(closeImage, 0, 0, Any())
            }
            mBottomSheetDialog.show()

        }


        fun openDialogFullScreenImage(
            activity: Activity,
            asf: Any,
            tText: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, android.R.style.Theme_NoTitleBar_Fullscreen)
            val child = activity.layoutInflater.inflate(R.layout.view_full_image_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val closeImage: ImageView = child.findViewById(R.id.closeImage)
            closeImage.bringToFront()
            val fullImageView: SimpleDraweeView = child.findViewById(R.id.fullImageView)
//            fullImageView.setImageURI(Uri.parse(tText as String))
//            Picasso.get().load(tText as String).into(fullImageView)
            closeImage.setOnClickListener {
                mBottomSheetDialog.dismiss()
//                listener.onItemClick(closeImage, 0, 0, Any())
            }
            mBottomSheetDialog.show()

        }

        fun openDialogFullScreenImages(
            activity: Activity,
            aInt: Int,
            tTextList: ArrayList<String>,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, android.R.style.Theme_NoTitleBar_Fullscreen)
            val child = activity.layoutInflater.inflate(R.layout.view_full_image_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)

            val closeImage: ImageView = child.findViewById(R.id.closeImage)
            closeImage.bringToFront()
            val fullImageView: SimpleDraweeView = child.findViewById(R.id.fullImageView)
            fullImageView.visibility = View.GONE
            val fullImageViewPager: ViewPager = child.findViewById(R.id.fullImageViewPager)

            val relativeLayout: RelativeLayout = child.findViewById(R.id.pagerHolderView)
            val pagerIndicator: PageIndicatorView = child.findViewById(R.id.pageIndicatorView)
            relativeLayout.visibility = View.VISIBLE

            /*  var mCustomPagerAdapter = CustomPagerAdapter(activity, tTextList)
              fullImageViewPager.bringToFront()
              pagerIndicator.bringToFront()

              fullImageViewPager.setAdapter(mCustomPagerAdapter)
              fullImageViewPager.setCurrentItem(aInt)*/

            closeImage.setOnClickListener {
                mBottomSheetDialog.dismiss()
            }
            mBottomSheetDialog.show()

        }


        /*
               * ----------------------------
               * Location permission dialog
               * -----------------------------
               */

        fun openLocationPermissionAlert(
            activity: Activity, any: Any, any1: Any,
            onBottomDialogItemListener: OnBottomDialogItemListener1<Any>
        ): Dialog {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child =
                activity.layoutInflater.inflate(
                    R.layout.layout_view_location_permission_dialog,
                    null
                )
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.BOTTOM)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
//            val resetBtn: TextView = child.findViewById(R.id.resetBtn)
            val settingButtonDialog: Button = child.findViewById(R.id.settingButtonDialog)

            settingButtonDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
                // Build intent that displays the App settings screen.
                var intent: Intent = Intent()
                intent.setAction(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                )
                var uri: Uri = Uri.fromParts(
                    "package",
                    BuildConfig.APPLICATION_ID, null
                )
                intent.setData(uri)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                activity.startActivity(intent)
            }


            mBottomSheetDialog.show()
            return mBottomSheetDialog
        }


        /*
              * ----------------------------
              * Location permission dialog
              * -----------------------------
              */

        fun openCameraPermissionAlert(
            activity: Activity, any: Any, any1: Any,
            onBottomDialogItemListener: OnBottomDialogItemListener1<Any>
        ): Dialog {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child =
                activity.layoutInflater.inflate(
                    R.layout.layout_view_camera_permission_dialog,
                    null
                )
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.BOTTOM)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
//            val resetBtn: TextView = child.findViewById(R.id.resetBtn)
            val settingButtonDialog: Button = child.findViewById(R.id.settingButtonDialog)

            settingButtonDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
//                activity.finish()
                // Build intent that displays the App settings screen.
                var intent: Intent = Intent()
                intent.setAction(
                    Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                )
                var uri: Uri = Uri.fromParts(
                    "package",
                    BuildConfig.APPLICATION_ID, null
                )
                intent.setData(uri)
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                activity.startActivity(intent)
                onBottomDialogItemListener.onItemClick(settingButtonDialog, 0, 0, Any())
            }


            mBottomSheetDialog.show()
            return mBottomSheetDialog
        }

        fun openDialog(
            activity: Activity,
            name: Array<String>,
            icons: IntArray,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialogSheet)
            val child = activity.layoutInflater.inflate(R.layout.view_image_pick_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()!!.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog.show()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
            val list_view: RecyclerView = child.findViewById(R.id.list_view)
            list_view.setLayoutManager(LinearLayoutManager(activity))
            list_view.setAdapter(
                OpenBottonDialogAdapter(
                    name,
                    icons,
                    object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                            when (type) {
                                CommonValues.APAPTER_BOTTOM_DIALOG_CLICK -> {
                                    val s = t as String
                                    Log.e("Data ", s)
                                    listener.onItemClick(view, position, type, s)
                                    mBottomSheetDialog.dismiss()
                                }
                            }
                        }
                    })
            )
        }

        fun openDialogTourList(
            activity: Activity,
            nameList: ArrayList<ToursListResponseModel.ResponseObj>,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialogSheet)
            val child = activity.layoutInflater.inflate(R.layout.view_tour_list_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()!!.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog.show()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
            val recyclarTour: RecyclerView = child.findViewById(R.id.recyclarTour)
            with(recyclarTour) {
                var tourDapter =
                    TourListAdapter(activity!!, nameList, object : OnItemClickListener<Any> {
                        override fun onItemClick(view: View, position: Int, type: Int, t: Any?) {
                            listener.onItemClick(
                                recyclarTour,
                                position,
                                0,
                                t as ToursListResponseModel.ResponseObj
                            )
                            mBottomSheetDialog.dismiss()
                        }
                    })
                adapter = tourDapter
                layoutManager = LinearLayoutManager(activity, VERTICAL, false)
            }
        }

        fun openDialogTourDetail(
            activity: Activity,
            tText: Any,
            mData: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child = activity.layoutInflater.inflate(R.layout.view_tour_detail_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)
            val cancelBtnDialog: TextView = child.findViewById(R.id.cancelBtnDialog)
            val doneBtnDialog: TextView = child.findViewById(R.id.doneBtnDialog)

            val titleTxtDialog: TextView = child.findViewById(R.id.event_titleDetail)
            val tourTimeTxtDetail: TextView = child.findViewById(R.id.tourTimeTxtDetail)
            val eventLocationDetail: TextView = child.findViewById(R.id.event_locationDetail)
            val tourTypeTxtDetail: TextView = child.findViewById(R.id.tourTypeTxtDetail)
            val numSeatsTxt: TextView = child.findViewById(R.id.numSeatsTxt)
            val removeScheduleTxt: TextView = child.findViewById(R.id.removeScheduleTxt)

            var tourData = mData as ScheduleDayActivity.TourData
            titleTxtDialog.setText(tourData.title)
            tourTimeTxtDetail.setText(tourData.startTime + " " + tourData.endTime)
            eventLocationDetail.setText(tourData.location)
            tourTypeTxtDetail.setText(
                tourData.tourType!!.replace(
                    "_",
                    " "
                )
            )
            numSeatsTxt.setText(tourData.seats)
            if (tourData.isManual!!) {
                removeScheduleTxt.visibility = View.VISIBLE
            } else removeScheduleTxt.visibility = View.GONE

            doneBtnDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(doneBtnDialog, 0, 1, Any())
            }
            removeScheduleTxt.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(doneBtnDialog, 0, 2, Any())
            }

            cancelBtnDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(cancelBtnDialog, 0, 0, Any())
            }
            mBottomSheetDialog.show()
        }

        fun openCalandarViewDays(
            activity: Activity,
            name: Any,
            icons: Any,
            listener: OnBottomDialogItemListener1<Any>
        ) {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialogSheet)
            val child = activity.layoutInflater.inflate(R.layout.view_calndar_type_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(true)
            mBottomSheetDialog.getWindow()!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()!!.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog.show()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
            val oneDayView: TextView = child.findViewById(R.id.oneDayView)
            val threeDayView: TextView = child.findViewById(R.id.threeDayView)
            val fiveDayView: TextView = child.findViewById(R.id.fiveDayView)
            val weekView: TextView = child.findViewById(R.id.weekView)
            val todayView: TextView = child.findViewById(R.id.todayView)

            if (Prefs.getInt(CommonValues.DEFAULT_CALANDAR_VIEW, 7) == 7) {
                weekView.setBackgroundColor(activity.resources.getColor(R.color.colorPrimarytrans))
            } else if (Prefs.getInt(CommonValues.DEFAULT_CALANDAR_VIEW, 7) == 3) {
                threeDayView.setBackgroundColor(activity.resources.getColor(R.color.colorPrimarytrans))
            } else if (Prefs.getInt(CommonValues.DEFAULT_CALANDAR_VIEW, 7) == 1) {
                oneDayView.setBackgroundColor(activity.resources.getColor(R.color.colorPrimarytrans))
            } else if (Prefs.getInt(CommonValues.DEFAULT_CALANDAR_VIEW, 7) == 0) {
                todayView.setBackgroundColor(activity.resources.getColor(R.color.colorPrimarytrans))
            }

            oneDayView.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(oneDayView, 0, 1, 1)
            }
            threeDayView.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(threeDayView, 0, 1, 3)
            }
            weekView.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(fiveDayView, 0, 1, 7)
            }
            todayView.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(fiveDayView, 0, 1, 0)
            }
        }

        /*   ----------------------------
                 *  Update app version
                 *  Confirmation dialog
                 * -----------------------------
               */
        fun openVersionAlert(
            activity: Activity, any: Any, any1: Any,
            listener: OnBottomDialogItemListener1<Any>
        ): Dialog {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child = activity.layoutInflater.inflate(R.layout.layout_view_version_dialog, null)
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.CENTER)
            val dialogMessagTitls: TextView = child.findViewById(R.id.dialogMessagTitls)
            val dialogMessageText: TextView = child.findViewById(R.id.dialogMessageText)
            val cancelBtn: TextView = child.findViewById(R.id.dialogCancelBtn)
            val okBtn: TextView = child.findViewById(R.id.dialogOkBtn)
//            okBtn.setText(R.string.str_delete)
//            dialogMessagTitls.setText(strTitle as String)
//            dialogMessageText.setText(steMsg as String)

            cancelBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(cancelBtn, 0, 1, Any())
            }

            okBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                listener.onItemClick(okBtn, 0, 2, Any())
            }
            mBottomSheetDialog.show()
            return mBottomSheetDialog
        }

        fun openDisclosureAlertLocation(
            activity: Activity, any: Any, any1: Any,
            onBottomDialogItemListener: OnBottomDialogItemListener1<Any>
        ): Dialog {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child =
                activity.layoutInflater.inflate(
                    R.layout.layout_view_disclosure_dialog,
                    null
                )
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.BOTTOM)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
            val acceptBtn: TextView = child.findViewById(R.id.acceptBtn)
            val cancelBtn: TextView = child.findViewById(R.id.cancelBtn)
            cancelBtn.setPaintFlags(cancelBtn.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

            cancelBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                onBottomDialogItemListener.onItemClick(acceptBtn, 0, 0, Any())
            }
            acceptBtn.setOnClickListener {
                mBottomSheetDialog.dismiss()
                onBottomDialogItemListener.onItemClick(acceptBtn, 0, 1, Any())
            }

            mBottomSheetDialog.show()
            return mBottomSheetDialog
        }


        fun openCancelTrip(
            activity: Activity, any: Any, any1: Any,
            onBottomDialogItemListener: OnBottomDialogItemListener1<Any>
        ): Dialog {
            val mBottomSheetDialog = Dialog(activity, R.style.MaterialDialog)
            val child =
                activity.layoutInflater.inflate(
                    R.layout.layout_view_cancel_tour,
                    null
                )
            mBottomSheetDialog.setContentView(child)
            mBottomSheetDialog.setCancelable(false)
            mBottomSheetDialog.getWindow()
                ?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )
            mBottomSheetDialog.getWindow()?.setGravity(Gravity.BOTTOM)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBottomSheetDialog.window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                mBottomSheetDialog.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                mBottomSheetDialog.window?.navigationBarColor =
                    ContextCompat.getColor(activity!!, R.color.colorWhite)
            }
            val cancelBtn: TextView = child.findViewById(R.id.cancelBtn)
//            cancelBtn.setPaintFlags(cancelBtn.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
            val closeImageBtnDialog: ImageView = child.findViewById(R.id.closeImageBtnDialog)
            val reason1ChkBox: CheckBox = child.findViewById(R.id.reason1ChkBox)
            val reason2ChkBox: CheckBox = child.findViewById(R.id.reason2ChkBox)
            val reason3ChkBox: CheckBox = child.findViewById(R.id.reason3ChkBox)
            reason1ChkBox.setOnClickListener {
                reason1ChkBox.isChecked = true
                reason2ChkBox.isChecked = false
                reason3ChkBox.isChecked = false
            }
            reason2ChkBox.setOnClickListener {
                reason1ChkBox.isChecked = false
                reason2ChkBox.isChecked = true
                reason3ChkBox.isChecked = false
            }
            reason3ChkBox.setOnClickListener {
                reason1ChkBox.isChecked = false
                reason2ChkBox.isChecked = false
                reason3ChkBox.isChecked = true
            }


            cancelBtn.setOnClickListener {

                if (!reason1ChkBox.isChecked && !reason2ChkBox.isChecked && !reason3ChkBox.isChecked) {
                    AppUtils.showToast(activity.getString(R.string.str_select_a_option))
                    return@setOnClickListener
                }
                if (reason1ChkBox.isChecked) {
                    mBottomSheetDialog.dismiss()
                    onBottomDialogItemListener.onItemClick(
                        cancelBtn,
                        0,
                        1,
                        reason1ChkBox.text.toString()
                    )
                } else if (reason2ChkBox.isChecked) {
                    mBottomSheetDialog.dismiss()
                    onBottomDialogItemListener.onItemClick(
                        cancelBtn,
                        0,
                        1,
                        reason2ChkBox.text.toString()
                    )
                } else if (reason3ChkBox.isChecked) {
                    mBottomSheetDialog.dismiss()
                    onBottomDialogItemListener.onItemClick(
                        cancelBtn,
                        0,
                        1,
                        reason3ChkBox.text.toString()
                    )
                }

            }
            closeImageBtnDialog.setOnClickListener {
                mBottomSheetDialog.dismiss()
            }

            mBottomSheetDialog.show()
            return mBottomSheetDialog
        }

    }
}