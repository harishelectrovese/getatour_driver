package code_setup.app_util

import android.util.Log
import code_setup.app_models.other_.DateModel
import code_setup.app_util.location_utils.log
import java.text.Format
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DateUtilizer() {


    companion object {
        val FULL_FORMAT: String = "dd MM yyyy, hh:mm:ss a"
        var DEFAULT_DATE_FORMAT: String = "dd-MM-yyyy"// "EEEE,MMMM YY";
        var MONTH_FORMAT: String = "MMMM"
        var YEAR_FORMAT: String = "yyyy"


        /**
         * get required current date format.
         */
        fun getCurrentDate(format: String): String {
            val dateFormat = SimpleDateFormat(format, Locale.US)
            val date = Date()
            Log.d("Date :: ", dateFormat.format(date))
            return dateFormat.format(date)
        }

        /**
         * get current date .
         */
        fun getCurrentDate(): Date {
            return Calendar.getInstance().getTime()
        }

        fun differenceBetweenDates(date1: Long, date2: Long): Long {
            Log.d("differenceBetweenDates ", "  " + date1 + "    " + date2)
            val diff: Long = date1 - date2
            val seconds = diff / 1000
            val minutes = seconds / 60
            val hours = minutes / 60
            val days = hours / 24
            Log.d("Difference ", "in seconds" + seconds)
            Log.d("Difference ", "in minutes" + minutes)
            return seconds
        }


        /**
         * get last sunday date
         */
        fun getLastDay(): String {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - 1))
            Log.d(" month :::::", "" + -(cal.get(Calendar.DAY_OF_WEEK) - 1))
            System.out.println(cal.get(Calendar.DATE))
            System.out.println(cal.get(Calendar.MONTH))
            var intMonth = cal.get(Calendar.MONTH)
            intMonth = (intMonth + 1)
            Log.d(" month :::::", "" + (intMonth + 1))
            return "" + cal.get(Calendar.DATE) + "-" + intMonth
        }

        /**
         * get last sunday date
         */
        fun getNextMonthDay(): String {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DAY_OF_WEEK, -(cal.get(Calendar.DAY_OF_WEEK) - 1))
            System.out.println(cal.get(Calendar.DATE))
            System.out.println(cal.get(Calendar.MONTH))
            var intMonth = cal.get(Calendar.MONTH)
            intMonth = (intMonth + 1)
            Log.d(" month :::::", "" + (intMonth + 1))
            return "" + cal.get(Calendar.DATE) + "-" + intMonth
        }


        fun getFormatedDate(inFormat: String, outFormat: String, dateText: String): String {

            var str: String? = ""
            try {
                Log.i(" getFormatedDate ", " indate ------------ " + dateText)
                val inputFormat = SimpleDateFormat(inFormat, Locale.US)
                val outputFormat = SimpleDateFormat(outFormat, Locale.US)

                var date: Date? = null
                str = null

                try {
                    date = inputFormat.parse(dateText)
                    str = outputFormat.format(date)
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
                Log.i(" getFormatedDate ", " outdate ----------- " + outFormat + "   " + str)
            } catch (e: Exception) {
                e.printStackTrace()
                str = dateText
            }
            return str.toString()
        }

        fun getNextDays(dCount: Int, iDate: String): ArrayList<DateModel> {
            Log.i(" getNextDays ", "1 --------------------------- $iDate")
            val modelList = ArrayList<DateModel>()
            var mDay = DateModel()
            val calendar = GregorianCalendar.getInstance();
            if (!iDate.equals("0")) {
                try {
//                val calendar = Calendar.getInstance()
                    calendar.set(Calendar.YEAR, iDate.split("-")[2].toInt())
                    calendar.set(Calendar.MONTH, (iDate.split("-")[1].toInt() - 1))
                    calendar.set(Calendar.DAY_OF_MONTH, iDate.split("-")[0].toInt())

                    Log.i("TAG", "1 ---------------------------d " + iDate.split("-")[0].toInt())
                    Log.i(
                        "TAG",
                        "1 ---------------------------m " + (iDate.split("-")[1].toInt() - 1)
                    )
                    Log.i("TAG", "1 ---------------------------y " + iDate.split("-")[2].toInt())
                    Log.i("TAG", "1 --------------------------- $iDate")
                } catch (e: ParseException) {
                    e.printStackTrace()
                }
            }

            val sdfFull = SimpleDateFormat("MMMM/dd/yyyy", Locale.US)
            val sdf = SimpleDateFormat("EE", Locale.US)
            val sdf1 = SimpleDateFormat("dd", Locale.US)
            if (!iDate.equals("0")) {
                val day = sdf.format(calendar.time)
                val date = sdf1.format(calendar.time)
                val fullDate = sdfFull.format(calendar.time)
                Log.i(" First Day", day + " " + date + "  " + fullDate)
                mDay = DateModel()
                mDay.date = Integer.parseInt(date)
                mDay.day = day.substring(0, 2)
                mDay.month = fullDate.split("/")[0]
                mDay.monthYear = fullDate.split("/")[0] + " " + fullDate.split("/")[2]
                mDay.fullDate = fullDate
                mDay.isSelected = false
                modelList.add(mDay)
            }




            for (i in 0 until dCount) {
                Log.i("TAG", "2--------------------------- $i")
                calendar.add(Calendar.DATE, 1)
                val day = sdf.format(calendar.time)
                val date = sdf1.format(calendar.time)
                val fullDate = sdfFull.format(calendar.time)
                Log.i("TAG", day + " " + date + "  " + fullDate)
                mDay = DateModel()
                mDay.date = Integer.parseInt(date)
                mDay.day = day.substring(0, 2)
                mDay.month = fullDate.split("/")[0]
                mDay.monthYear = fullDate.split("/")[0] + " " + fullDate.split("/")[2]
                mDay.fullDate = fullDate
                if (fullDate.equals(getCurrentDate("MMMM/dd/yyyy")))
                    mDay.isSelected = true
                modelList.add(mDay)
            }

            return modelList
        }

        fun getTimeDiffereence(startTime: String, endTime: String): Long {
            val startDate = startTime
            val stopDate = endTime

// Custom date format
            // Custom date format
            val format = SimpleDateFormat("dd-MM-yyyy, HH:mm", Locale.US)

            var d1: Date? = null
            var d2: Date? = null
            try {
                d1 = format.parse(startDate)
                d2 = format.parse(stopDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

// Get msec from each, and subtract.
            // Get msec from each, and subtract.
            val diff = d2!!.time - d1!!.time
            val diffSeconds = diff / 1000
            var diffMinutes = diff / (60 * 1000)
            val diffHours = diff / (60 * 60 * 1000)
            println("Time in seconds: $diffSeconds seconds.")
            println("Time in minutes: $diffMinutes minutes.")
            println("Time in hours: $diffHours hours.")

            // This is used to set proper data on caklander view
            /* if (diffMinutes < 15) {
                 diffMinutes = 15
             }*/
            return diffMinutes
        }

        /*
        * Get calendar from string
        * */
        fun getcalenderFromString(month: String, format: String): Calendar {
            log("getcalenderFromString    " + month)
            val aTime = month
            val sdf = SimpleDateFormat(format, Locale.US)
            val cal = Calendar.getInstance()
            try {
                cal.time = sdf.parse(aTime)
                Log.i("getcalenderFromString", "time = " + cal.timeInMillis)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return cal
        }

        /**
         * Get Previous Week
         */
        fun getPastDays(iDay: Int, iDate: String): ArrayList<DateModel> {
            Log.i("getPastWeek", "1 --------------------------- $iDate")
            val modelList = ArrayList<DateModel>()
            var mDay = DateModel()
            val calendar = GregorianCalendar.getInstance()

            try {
//                val calendar = Calendar.getInstance()
                calendar.set(Calendar.YEAR, iDate.split("-")[2].toInt())
                calendar.set(Calendar.MONTH, (iDate.split("-")[1].toInt() - 1))
                calendar.set(Calendar.DAY_OF_MONTH, iDate.split("-")[0].toInt())

                Log.i("TAG", "1 ---------------------------d " + iDate.split("-")[0].toInt())
                Log.i("TAG", "1 ---------------------------m " + (iDate.split("-")[1].toInt() - 1))
                Log.i("TAG", "1 ---------------------------y " + iDate.split("-")[2].toInt())
                Log.i("TAG", "1 --------------------------- $iDate")
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            val sdfFull = SimpleDateFormat("MMMM/dd/yyyy", Locale.US)
            val sdf = SimpleDateFormat("EE", Locale.US)
            val sdf1 = SimpleDateFormat("dd", Locale.US)
            for (i in 1 until iDay) {

                calendar.add(Calendar.DATE, -i)
                val day = sdf.format(calendar.time)
                val date = sdf1.format(calendar.time)
                val fullDate = sdfFull.format(calendar.time)
                Log.i("TAG", "$day $date")
                mDay = DateModel()
                mDay.date = Integer.parseInt(date)
                mDay.day = day.substring(0, 2)
                mDay.month = fullDate.split("/")[0]
                mDay.monthYear = fullDate.split("/")[0] + " " + fullDate.split("/")[2]
                mDay.fullDate = fullDate
                mDay.isSelected = false
                modelList.add(mDay)
                modelList.add(0, mDay)
            }
            //        specList.clear();
            modelList.addAll(0, modelList)

            return modelList
        }

        fun getPreText(sFormat: String, bookingDate: String): String {
            var strPreJoinTxt = ""
            try {
                val sdf =
                    SimpleDateFormat(sFormat, Locale.US)
                val date1 = sdf.parse(getCurrentDate(sFormat))
                val date2 = sdf.parse(bookingDate)


                if (date1.compareTo(date2) > 0) {
                    Log.i("app", "Date1 is after Date2")
                } else if (date1.compareTo(date2) < 0) {
                    Log.i("app", "Date1 is before Date2")
//                    if(date1.compareTo(date2) == -1){
//                        strPreJoinTxt = "Tomorrow, "
//                    }
                } else if (date1.compareTo(date2) == 0) {
                    Log.i("app", "Date1 is equal to Date2")
                    strPreJoinTxt = "Today, "
                }

                Log.d(
                    "TAG",
                    " " + date1 + "   " + date2 + "  " + date1.compareTo(date2) + " " + strPreJoinTxt + "  " + bookingDate
                )
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return strPreJoinTxt
        }

        fun getDateFromString(inFormat: String, strDate: String): Date? {
            var dtStart = strDate
            val format = SimpleDateFormat(inFormat, Locale.US)
            var date = try {
                format.parse(dtStart);

            } catch (e: ParseException) {
                e.printStackTrace();
            }
            try {
                System.out.println(date);
            } catch (e: Exception) {
            }
            return date as Date?
        }

        fun getStringFromDate(dat: Date): String {
            val formatter: Format = SimpleDateFormat("dd MMMM, yyyy", Locale.US)
            val s: String = formatter.format(dat)
            return s
        }

        /**
         * Get next date from current selected date
         *
         * @param date date
         */
        fun incrementTimeByOne(timeStr: String): String {
            val myTime = timeStr
            val df = SimpleDateFormat("hh:mm")
            val d = df.parse(myTime)
            val cal = Calendar.getInstance()
            cal.time = d
            cal.add(Calendar.MINUTE, 1)
            val newTime = df.format(cal.time)
            return newTime
        }


        /**
         * Get +1 time from current selected tme
         *
         * @param date date
         */
        fun incrementDateByOne(date: Date?): Date? {
            val c = Calendar.getInstance()
            c.time = date
            c.add(Calendar.DATE, 1)
            return c.time
        }

        /**
         * Get previous date from current selected date
         *
         * @param date date
         */
        fun decrementDateByOne(date: Date?): Date? {
            val c = Calendar.getInstance()
            c.time = date
            c.add(Calendar.DATE, -1)
            return c.time
        }

        fun compairDates(bDate: String, sFormat: String): Boolean {
            try {
                val sdf = SimpleDateFormat(sFormat, Locale.US)
                val date1 = sdf.parse(getCurrentDate(sFormat))
                val date2 = sdf.parse(bDate)


                if (date1.compareTo(date2) > 0) {
                    Log.i("app", "Date1 is after Date2")
                    return false
                } else if (date1.compareTo(date2) < 0) {
                    Log.i("app", "Date1 is before Date2")
//                    if(date1.compareTo(date2) == -1){
//                        strPreJoinTxt = "Tomorrow, "
//                    }
                    return true
                } else if (date1.compareTo(date2) == 0) {
                    Log.i("app", "Date1 is equal to Date2")
                    return true
                }

            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return false
        }

        fun getDateFromCalandar(cal: Calendar, format: String): String {
//            val cal = Calendar.getInstance()
            val dateFormat =
                SimpleDateFormat(format, Locale.US)
            println(dateFormat.format(cal.time))
            return dateFormat.format(cal.time)
        }

        fun getMinutesValue(startTime: String): Int {
            var minutes: Int = 0
            minutes = startTime.split(" ")[1].split(":")[1].toInt()
            log("getMinutesValue  " + minutes)
            return minutes
        }

        fun getHourValue(startTime: String): Int {
            var hour: Int = 0
            hour = startTime.split(" ")[1].split(":")[0].toInt()
            log("getHourValue  1 " + hour)
            /*  if (hour > 12) {
                  log("getHourValue  2 " + hour % 12)
                  return hour % 12
              }*/
            return hour
        }

        fun getHoursFromMin(t: Int): String {
            var hoursTemp: Int = t / 60 //since both are ints, you get an int
            var minutesTemp: Int = t % 60
            var hours: Int = t / 60 //since both are ints, you get an int
            var minutes: Int = t % 60

            System.out.printf("%d:%02d", hours, minutes)

            if (hours == 0) {
                if (minutes == 1) {
                    if (minutes <= 9)
                        return "0$minutes Min"// e.g 07,08
                    return "$minutes Min"
                } else {
                    if (minutes <= 9)
                        return "0$minutes Mins"// e.g 07,08
                    return "$minutes Mins"
                }
            } else {

                if (hours == 1) {
                    if (minutes <= 9)
                        return "0$hours:0$minutes Hour"// e.g 07,08
                    return "0$hours:$minutes Hour"
                } else {
                    if (minutes <= 9 && hours > 9)
                        return "$hours:0$minutes Hours"// e.g 07,08
                    else if (minutes <= 9 && hours <= 9)
                        return "0$hours:0$minutes Hours"// e.g 07,08
                    return "$hours:$minutes Hours"
                }
            }
        }
    }

}