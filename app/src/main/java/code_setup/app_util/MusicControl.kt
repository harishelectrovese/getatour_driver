package code_setup.app_util

import android.content.ContentResolver
import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import com.electrovese.setup.R


class MusicControl(context: Context?) {
    private var vibrator: Vibrator? = null
    private val mContext: Context?
    private var mMediaPlayer: MediaPlayer? = null
    fun playMusic() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mMediaPlayer = MediaPlayer()
            val uriMedis =
                Uri.parse("android.resource://" + mContext!!.packageName + "/" + R.raw.siren)
//            mMediaPlayer = MediaPlayer.create(mContext, R.raw.siren)
            mMediaPlayer!!.setDataSource(mContext!!, uriMedis!!)
        } else {
            mMediaPlayer = MediaPlayer()
            var uriMedis = Uri.parse(
                ContentResolver.SCHEME_ANDROID_RESOURCE
                        + "://" + mContext!!.packageName + "/raw/siren"
            )
            mMediaPlayer!!.setDataSource(mContext!!, uriMedis!!)
        }
        mMediaPlayer!!.setAudioStreamType(AudioManager.STREAM_ALARM)
        mMediaPlayer!!.prepare()
        mMediaPlayer!!.setVolume(100f, 100f)
        mMediaPlayer!!.start()
        mMediaPlayer!!.isLooping = true
        /*Handler().postDelayed(Runnable {
            if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) {
                stopMusic()
            }
        }, 10000)*/

        try {
            vibrator = mContext.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
            if (Build.VERSION.SDK_INT >= 26) {
                vibrator!!.vibrate(VibrationEffect.createWaveform(getPattern(), -1))
                vibrator!!.vibrate(5000)
            } else {
                vibrator!!.vibrate(getPattern(), -1); // does not repeat
                vibrator!!.vibrate(5000)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun stopMusic() {
        if (mMediaPlayer != null) {
//            mMediaPlayer!!.pause()
            mMediaPlayer!!.setVolume(0f,0f)
            mMediaPlayer!!.seekTo(0)
//            mMediaPlayer!!.stop()
            mMediaPlayer = MediaPlayer.create(mContext, R.raw.siren) //I needed this, maybe you dont hac
            mMediaPlayer!!.reset()
            mMediaPlayer!!.stop()
            try {
                var audioManager =  mContext!!.getSystemService(AUDIO_SERVICE) as AudioManager
                audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0)

            } catch (e: Exception) {
            }
//            mMediaPlayer = null
//            mMediaPlayer = MediaPlayer() //I needed this, maybe you dont hac
        }
        try {
            vibrator!!.cancel()
        } catch (e: Exception) {
        }
    }

    fun getPattern(): LongArray {
        return longArrayOf(50, 100, 1000, 200, 2000, 1000, 200)
    }

    companion object {
        private var sInstance: MusicControl? = null
        fun getInstance(context: Context?): MusicControl? {
            if (sInstance == null) {
                sInstance = MusicControl(context)
            }
            return sInstance
        }
    }

    init {
        mContext = context
    }
}