package code_setup.app_util

import android.content.Context
import android.content.Context.AUDIO_SERVICE
import android.media.AudioManager
import android.media.AudioManager.OnAudioFocusChangeListener
import android.media.AudioManager.RINGER_MODE_SILENT
import android.media.MediaPlayer
import android.media.Ringtone
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import code_setup.app_core.BaseApplication
import com.electrovese.setup.BuildConfig
import java.io.IOException


/**
 * Created by harish on 10/07/21.
 */
class MyMediaPlayer private constructor(private val context: Context) {
    private var ringMan: RingtoneManager? = null
    private var ringtone: Ringtone? = null
    private var uriMedis: Uri? = null
    var mp: MediaPlayer? = null
    private var vibrator: Vibrator? = null

    /**
     *
     * @param fileName if sound name is "sound.mp3" then pass fileName as "sound" only.
     */

    @Synchronized
    fun playSound(fileName: Uri) {
        if (instance!!.mp == null) {
            instance!!.mp = MediaPlayer()
        } else {
            instance!!.mp!!.reset()
        }
        /* try {
 //            uriMedis =
 //                Uri.parse("android.resource://" + context!!.packageName + "/" + R.raw.siren)
             !!.mp!!.setDataSource(
                 context,
                 fileName!!
             )
             instance!!.mp!!.prepare()
             instance!!.mp!!.setVolume(100f, 100f)
             instance!!.mp!!.isLooping = false
             instance!!.mp!!.start()
             instance!!.mp!!.setOnCompletionListener {
                 try {
                     Log.e("completeSound ", "completeSound: $fileName")
                     if (instance!!.mp != null) {
                         instance!!.mp!!.reset()
                         instance!!.mp = null
                     }
                 } catch (e: Exception) {
                 }
             }
             Handler().postDelayed(Runnable {
                 instance!!.mp!!.stop()
             }, 5000)
         } catch (e: IOException) {
             e.printStackTrace()
         }*/


        var alert = RingtoneManager.getDefaultType(fileName!!)
        if (alert == null) {
            // alert is null, using backup
            alert = RingtoneManager.getDefaultType(fileName!!)
            if (alert == null) {
                // alert backup is null, using 2nd backup
                alert = RingtoneManager.getDefaultType(fileName!!)
            }
        }
        ringMan = RingtoneManager(BaseApplication.instance)
        ringtone = RingtoneManager.getRingtone(BaseApplication.instance, fileName!!)
        ringtone!!.setStreamType(AudioManager.STREAM_ALARM);
        ringtone!!.play()

        try {
            vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
            if (Build.VERSION.SDK_INT >= 26) {
                vibrator!!.vibrate(VibrationEffect.createWaveform(getPattern(), -1))
                vibrator!!.vibrate(5000)
            } else {
                vibrator!!.vibrate(getPattern(), -1); // does not repeat
                vibrator!!.vibrate(5000)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getPattern(): LongArray {
        return longArrayOf(50, 100, 1000, 200, 2000, 1000, 200)
    }

    @Synchronized
    fun stopSound() {
        try {
            vibrator!!.cancel()
//          ringtone!!.volume = 0f

        } catch (e: Exception) {
        }

        try {
            ringMan!!.stopPreviousRingtone()
            ringtone!!.stop()

            try {
//                var audioManager = BaseApplication.instance.getSystemService(Context.AUDIO_SERVICE) as AudioManager
//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0)
//                audioManager.setStreamVolume(AudioManager.STREAM_ALARM, 0, 0)
                try {
//                    audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0)
//                    audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0)
                } catch (e: Exception) {
                }
            } catch (e: Exception) {
            }

            Log.e("stopSound ", "------->"+ ringtone!!.isPlaying)
        } catch (e: Exception) {
        }

        /* try {
             var audioManager = context!!.getSystemService(Context.AUDIO_SERVICE) as AudioManager
             // Change the stream to your stream of choice.
             if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                 audioManager.adjustStreamVolume(AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE, 0, 0);
                 audioManager.setStreamVolume(AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE, 0, 0)
             } else {
                 audioManager.setStreamMute(3, true);
             }

         } catch (e: Exception) {
         }


         if (instance!!.mp != null) {
             instance!!.mp!!.setVolume(0f, 0f)
             instance!!.mp!!.seekTo(0)
             instance!!.mp!!.stop()
             instance!!.mp = null
             instance!!.mp!!.release()
             try {
                 var audioManager = context!!.getSystemService(Context.AUDIO_SERVICE) as AudioManager
                 audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0)

             } catch (e: Exception) {
             }
         }*/
    }


    @Synchronized
    fun pauseSound() {
        if (instance!!.mp != null) {
            instance!!.mp!!.pause()
        }
    }

    @Synchronized
    fun restartSound() {
        if (instance!!.mp != null) {
            instance!!.mp!!.start()
        }
    }

    @Synchronized
    fun playRepeatedSound(fileName: String) {
        if (instance!!.mp == null) {
            instance!!.mp = MediaPlayer()
        } else {
            instance!!.mp!!.reset()
        }
        try {
            instance!!.mp!!.setDataSource(
                context,
                Uri.parse(APP_RAW_URI_PATH_1 + fileName)
            )
            instance!!.mp!!.prepare()
            instance!!.mp!!.setVolume(100f, 100f)
            instance!!.mp!!.isLooping = true
            instance!!.mp!!.start()
            instance!!.mp!!.setOnCompletionListener { mp ->
                var mp = mp
                if (mp != null) {
                    mp.reset()
                    mp = null
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    companion object {
        val APP_RAW_URI_PATH_1 =
            java.lang.String.format("android.resource://%s/raw/", BuildConfig.APPLICATION_ID)
        private const val TAG = "MyMediaPlayer"

        @Volatile
        private var instance: MyMediaPlayer? = null
        fun getInstance(context: Context): MyMediaPlayer? {
            if (instance == null) {
                synchronized(MyMediaPlayer::class.java) {
                    if (instance == null) {
                        instance = MyMediaPlayer(context)
                    }
                }
            }
            return instance
        }
    }

}