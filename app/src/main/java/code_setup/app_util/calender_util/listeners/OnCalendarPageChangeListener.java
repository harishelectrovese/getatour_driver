package code_setup.app_util.calender_util.listeners;

/**
 * Created by Mateusz Kornakiewicz on 12.10.2017.
 */

public interface OnCalendarPageChangeListener {
    void onChange();
}
