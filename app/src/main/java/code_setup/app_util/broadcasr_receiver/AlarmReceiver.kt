package code_setup.app_util.broadcasr_receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import code_setup.app_util.MusicControl

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        MusicControl.getInstance(context)!!.playMusic()
        // do your code here...
    }
}