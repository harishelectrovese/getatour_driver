package code_setup.app_util.broadcasr_receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import code_setup.app_util.MusicControl

class DismissBroadcast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        MusicControl.getInstance(context)!!.stopMusic()
        // do your code here...
    }
}