package code_setup.net_

class NetworkConstant {


    companion object {



        // Api request code--------------------------------------
        val LOGIN_CODE: Int = 122
        val LIST_CODE: Int = 123


        // Api url--------------------------------------
        enum class Environment private constructor(val url: String) {
            PROD("https://prod.domain.com:1088/"),
            TEST("http://91.205.173.97:6161/"),//socket
            LIVE("https://reqres.in/api/"),
            DEV("http://91.205.173.97:7575/"),//development
            LOCAL("http://192.168.1.121:7575/"),//local
            LOCAL_SAGAR("http://192.168.1.6:7575/"),//local
            LOCAL_CHARAN("http://192.168.1.111:7575/")//local
        }

        val BASE_URL: String = Environment.DEV.url
        val BASE_URL_SOCKET: String = Environment.DEV.url
        val TOKEN: String? = "token"


        // Api response code--------------------------------------
        val SUCCESS: Int = 1
        val FAIL: Int = 0


        // booking  panel links
        val BOOKING_LINK: String="http://91.205.173.97:3111/"
        val GUIDE_LINK: String="http://91.205.173.97:7577/signup"

        // Api methods names----------------------------------------------
        const val API_LOGIN: String = "api/driver/login"
        const val API_PROFILE: String = "api/driver/profile"
        const val API_CHANGE_STATUS: String = "api/driver/change/status"
        const val API_CAPURE_INFO: String = "api/driver/captureinfo"

        const val API_ACCEPT_BOOKINGS: String = "api/driver/bookings/accept"
        const val API_REJECT_BOOKINGS: String = "api/driver/bookings/reject"
        const val API_DRIVER_STATUS: String = "api/driver/current/status"
        const val API_UPCOMING_BOOKINGS: String = "api/driver/bookings/list"
        const val API_START_MOVING: String = "api/driver/bookings/start"
        const val API_TOUR_DETAIL: String = "api/driver/bookings/detail"
        const val API_ARRIVE_AT_LOCATION: String = "api/driver/bookings/arrived"
        const val API_MARK_TRIP_COMPLETE: String = "api/driver/bookings/end"
        const val API_ONBOARDING: String = "api/driver/bookings/onboard"
        const val API_GET_MESSAGES: String = "api/driver/messages"
        const val API_SEND_MESSAGES: String = "api/driver/messages/send"
        const val API_BOOKING_HISTORY: String = "api/driver/bookings/history"
        const val API_SUBMIT_RATING: String = "api/driver/bookings/review"
        const val API_GET_DATEWISE_TOUR: String = "api/driver/bookings/date"
        const val API_GET_MONTHWISE_TOUR: String = "api/driver/bookings/month"
        const val API_LOGOUT_USER: String = "api/driver/logout"
        const val API_SCHEDULE_TOUR: String = "api/driver/calendar/schedule"
        const val API_UPDATE_PROFILE: String = "api/driver/update/profile"
        const val API_SUPPORT: String = "api/driver/support/create"
        const val API_REJECTED_TOURS = "api/driver/rejected/history"
        const val API_EARNINGS_DATA = "api/driver/earnings"
        const val API_TOUR_LIST = "api/driver/tour/list"
        const val API_RIDE_SUPPORT = "api/driver/bookings/support"
        const val API_GET_SCHEDULED_TOUR: String = "api/driver/bookings/schedules/date"
        const val API_REMOVE_SCHEDULE = "api/driver/calendar/schedule/delete"
        const val API_GET_MONTHWISE_SCHEDULED_TOUR: String = "api/driver/bookings/schedules/month"
        const val API_LOCATION_UPDATES = "api/driver/bookings/update/location"
        const val API_LOCATION_UPDATES_NO_RIDE = "api/driver/update/location"
        const val API_DRIVER_REVIEWS = "api/driver/reviews"
        const val API_VERSION_CHECK = "api/driver/version/check"

        const val API_START_MOVING_TO_MEETING_POINT: String = "api/driver/bookings/start/meeting"
        const val API_REACHED_MEETING_POINT: String = "api/driver/bookings/reached/meeting"
        const val API_CANCEL_RIDE = "api/driver/bookings/cancel"
    }
}