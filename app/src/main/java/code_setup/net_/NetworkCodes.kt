package code_setup.net_

enum class NetworkCodes constructor(val nCodes: Int) {
    SUCCEES(1),
    FAIL(0),
    NETWORK_ERROR(502),
    SESSION(401)
}