package code_setup.net_

class NetworkRequest {
    companion object {


        // Api request code--------------------------------------
        val REQUEST_LOGIN_CODE: Int = 122
        val REQUEST_PROFILE_CODE: Int = 123


        val REQUEST_OTP_CODE: Int = 123
        val REQUEST_VERIFY_OTP_CODE: Int = 124
        val REQUEST_CHANGE_STATUS: Int = 125
        val REQUEST_CAPTURE_INFO: Int = 126
        val REQUEST_ACCEPT_BOOKING: Int = 127
        val REQUEST_REJECT_BOOKING: Int = 128
        val REQUEST_CURRENT_STATUS: Int = 129
        val REQUEST_UPCOMING_TOURS: Int = 130
        val REQUEST_START_MOVING_REQUEST: Int = 131
        val REQUEST_TOUR_DETAIL: Int = 132
        val REQUEST_ARRIVED_LOCATION: Int = 133
        val REQUEST_MARK_COMPLETE_TRIP: Int = 134
        val REQUEST_ONBOARDING: Int = 135
        val REQUEST_FIND_CHAT_CONVERSATION: Int = 136
        val REQUEST_SEND_MESSAGE: Int = 137
        val REQUEST_BOOKING_HISTORY: Int = 138
        val REQUEST_SUBMIT_RATTING: Int = 139
        val REQUEST_GET_DATE_WISE_TOURS: Int = 140
        val REQUEST_GET_MONYTHLY_TOURS: Int = 141
        val REQUEST_LOGOUT_USER: Int = 142
        val REQUEST_SCHEDULE_CALENDAR_BOOKING: Int = 143
        val REQUEST_UPDATE_PROFILE: Int = 144
        val REQUEST_SUPPORT: Int = 145
        val REQUEST_REJECTED_TOURS: Int = 146
        val REQUEST_EARNING_DATA: Int = 147
        val REQUEST_TOURS: Int = 148
        val REQUEST_RIDE_SUPPORT: Int = 149
        val REQUEST_GET_DATE_WISE_SCHEDULED_TOURS: Int = 150
        val REQUEST_DELETE_SCHEDULED_TOURS: Int = 151
        val REQUEST_DRIVER_REVIEWS: Int = 152

        val REQUEST_START_MOVING_TO_MEETING_REQUEST: Int = 153
        val REQUEST_REACHED_MEETING_REQUEST: Int = 154
        val REQUEST_CANCEL_RIDE: Int = 155
    }


}