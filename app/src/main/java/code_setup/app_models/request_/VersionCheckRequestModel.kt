package code_setup.app_models.request_

data class VersionCheckRequestModel(
    var app_version: String,
    var device_os: String,
    var os_version: String
)