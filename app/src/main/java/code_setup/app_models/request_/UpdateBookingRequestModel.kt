package code_setup.app_models.request_

data class UpdateBookingRequestModel(
    var booking_id: String
)