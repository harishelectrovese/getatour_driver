package code_setup.app_models.other_.event

class EVENTS {

    companion object {


        val CURRENT_LOCATION: Int = 105 * 10
        val PERMISSION_GRANTED: Int = 10002

        val START_TRIP: Int = 100
        val TRIP_DESTINATIONS: Int = 101
        val REQUEST_NEW_TOUR_JOB: Int = 102
        val REQUEST_START_MOVING_TO_MEETING_POINT_VIEW: Int = 102001
        val REQUEST_REACHED_MEETING_POINT_VIEW: Int = 102002
        val REQUEST_START_MOVING_VIEW: Int = 103
        val REQUEST_TOUR_DESTINATIONS_VIEW: Int = 104

        val REQUEST_TOUR_DETAIL: Int = 105

        val REQUEST_TOUR_PICKUP_RIDERS_VIEW: Int = 106
        val REQUEST_TOUR_PICKUP_RIDER_CODE_SCANNED: Int = 107

        val NETWORK_CONNECTION_CALLBACK: Int = 188
        val REQUEST_CAMERA_PERMISSION_CHECK: Int = 189

        val REQUEST_TOUR_CHAT_MESSAGE: Int = 190
        val CALL_PERMISSION_GRANTED: Int = 191
        val REFRESH_SCREEN: Int = 192
        /*// Functional events--------------------------------------------------*/

        val ACCEPT_TOUR: Int = 10001
        val REJECT_TOUR: Int = 10002
        val NEW_MESSAGE: Int = 10003
        val PROFILE_UPDATED: Int = 10004
        val SCHEDULE_UPDATED: Int = 10005

        /* SOCKET SUBSCRIPTION KEYS*/
        val SOCKET_REQUEST_NEW_TOUR_JOB: Int = 1002
        val NEW_TOUR_JOB: String = "NEW_TOUR_JOB"
        val MESSAGE_RECEIVED: String = "MESSAGE_RECEIVED"


    }

}
