package code_setup.app_models.other_.event

class RIDESTATUS {

    companion object {

        val RIDESTATUS_ACCEPTED: String = "ACCEPTED"

        val RIDESTATUS_IN_PROGRESS: String = "INPROGRESS"
        val RIDESTATUS_STARTED: String = "STARTED" // new status
        val RIDESTATUS_AT_MEETING_POINT: String = "AT_MEETING_POINT" // new status
        val TRIP_DESTINATIONS: Int = 101
        val REQUEST_NEW_TOUR_JOB: Int = 102
        val REQUEST_START_MOVING_VIEW: Int = 103
        val REQUEST_TOUR_DESTINATIONS_VIEW: Int = 104




    }

}
