package code_setup.app_models.other_
import java.io.Serializable

data class NotificationDataBodyModel(
    var booking_date: String,
    var created_at: String,
    var destinations: String,
    var driver_id: String,
    var drop_off: String,
    var expiration_date: Long,
    var id: String,
    var booking_id: String,
    var pickup_loc: String,
    var tour_name: String,
    var price: String,
    var currency_symbol: String,
    var rating: String,
    var tour_id: String,
    var type: String,
    var board_type : String,
    var user_id: String,
    var user_name: String,
    var user_image: String,
    var is_ride: Boolean,
    var notificatiobId: String? = null,
    var chat: Chat? = null
) : Serializable

class Chat(
    var message: String,
    var sender_name: String,
    var sender_id: String
) : Serializable

