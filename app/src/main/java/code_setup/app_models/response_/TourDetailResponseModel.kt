package code_setup.app_models.response_

import java.io.Serializable

data class TourDetailResponseModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: ResponseObj
) {

    data class ResponseObj(
        var _id: String,
        var booking_date: String,
        var status: String,
        var type: String,
        var distance: String,
        var duration: String,
        var seats: Int,
        var id: String,
        var routes: List<Route>,
        var start_location: StartLocation,
        var category_name: String,
        var tour_name: String,
        var users: List<User>
    ) : Serializable {
        data class Route(
            var _id: String,
            var bookings: List<Booking>,
            var is_arrived: Boolean,
            var lat: Double,
            var lng: Double,
            var name: String,
            var waiting: String
        ) : Serializable {
            data class Booking(
                var user_id: String,
                var contact: String,
                var email: String,
                var name: String,
                var unique_code: String,
                var is_board: Boolean,
                var user_booking_id: String,
                var seats: String,
                var driver_unread_count: String? = "0",
                var user_image: String? = null
            ) : Serializable
        }

        data class StartLocation(
            var coordinates: List<Double>,
            var name: String,
            var type: String
        ) : Serializable

        data class User(
            var user_id: String? = null,
            var contact: String? = null,
            var email: String? = null,
            var name: String? = null,
            var unique_code: String? = null,
            var location_name: String? = null,
            var is_board: Boolean = false,
            var user_booking_id: String? = null,
            var seats: String? = null,
            var isSelected: Boolean? = false,
            var _id: String? = null,
            var driver_unread_count: String? = "0",
            var user_image: String? = null
        ) : Serializable
    }
}