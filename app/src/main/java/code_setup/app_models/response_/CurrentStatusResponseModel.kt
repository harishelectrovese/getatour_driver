package code_setup.app_models.response_

import code_setup.app_models.other_.NotificationDataBodyModel

data class CurrentStatusResponseModel(
    var response_code: Int,
    var response_message: String,
    var response_obj: ResponseObj
) {
    data class ResponseObj(
        var capacity: String,
        var current_booking: String,
        var pending_job: PendingJob,
        var status: String,
        var vehicle_id: String,
        var vehicle_number: String
    ) {
        data class PendingJob(
            var action_code: String,
            var body: String,
            var channel: String,
            var data: NotificationDataBodyModel,
            var id: String,
            var time_left: Int,
            var title: String,
            var tour_id: String
        )
    }
}